package sawalha.git.command;

import org.eclipse.jgit.api.TransportConfigCallback;
import org.eclipse.jgit.transport.JschConfigSessionFactory;
import org.eclipse.jgit.transport.OpenSshConfig.Host;
import org.eclipse.jgit.transport.SshSessionFactory;
import org.eclipse.jgit.transport.SshTransport;
import org.eclipse.jgit.transport.Transport;

import com.jcraft.jsch.Session;

public class MyTransportConfigCallback implements TransportConfigCallback {

    private String password;

    public MyTransportConfigCallback(String password) {
        this.password = password;
    }

    public void configure(Transport transport) {
        SshTransport sshTransport = (SshTransport) transport;
        sshTransport.setSshSessionFactory(getSshSessionFactory());
    }

    private SshSessionFactory getSshSessionFactory() {
        SshSessionFactory sshSessionFactory = new JschConfigSessionFactory() {
            @Override
            protected void configure(Host host, Session session) {
                session.setPassword(password);
            }

            // @Override
            // protected JSch createDefaultJSch(FS fs) throws JSchException {
            // JSch defaultJSch = super.createDefaultJSch(fs);
            // defaultJSch.addIdentity("C:\\Users\\User\\.ssh\\ibrahim.sawalha.ppk");
            // return defaultJSch;
            // }
        };
        return sshSessionFactory;
    }

}
