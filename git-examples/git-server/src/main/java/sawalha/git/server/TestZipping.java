package sawalha.git.server;

import java.io.File;
import java.io.IOException;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.lib.Repository;

import sawalha.git.zip.Zipper;

public class TestZipping extends ReleaserApp {

    // private String localPath = "D:\\delete\\test-git";
    // private String remotePath = "ssh://ibrahim.sawalha@10.100.102.35:8001/Git-Testing";
    private Repository localRepo;
    private Git git;
    // private String password = "Ibrahim123!";

    private GitConfiguration config;

    public TestZipping(GitConfiguration config) throws IOException {
        super(config);
    }

    public static void main(String[] args) throws Exception {

        GitConfiguration config = new GitConfiguration();

        config.setLocalRepoPath("D:\\delete\\test-git\\release")
                .setRemoteUrl("ssh://ibrahim.sawalha@10.100.102.35:8001/Git-Testing").setSshPassword("Ibrahim123!");
        TestZipping main = new TestZipping(config);

        Zipper appZip = new Zipper();
        appZip.generateFileList(new File(config.getLocalRepoPath() + File.separator));
        appZip.zipIt(config.getLocalRepoPath() + File.separator + "output.zip");

    }
}
