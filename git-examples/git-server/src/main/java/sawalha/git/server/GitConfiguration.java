package sawalha.git.server;

import org.apache.commons.lang3.StringUtils;

public class GitConfiguration {

    private String remoteUrl;
    private String localRepoPath;
    private String sshUsername;
    private String sshPassword;

    public GitConfiguration() {

    }

    public String getRemoteUrl() {
        return remoteUrl;
    }

    public GitConfiguration setRemoteUrl(String remoteUrl) {
        sshUsername = StringUtils.substringBetween(remoteUrl, "://", "@");
        this.remoteUrl = remoteUrl;
        return this;
    }

    public String getLocalRepoPath() {
        return localRepoPath;
    }

    public GitConfiguration setLocalRepoPath(String localRepoPath) {
        this.localRepoPath = localRepoPath;
        return this;
    }

    public String getSshUsername() {
        return sshUsername;
    }

    public GitConfiguration setSshUsername(String sshUsername) {
        this.sshUsername = sshUsername;
        return this;
    }

    public String getSshPassword() {
        return sshPassword;
    }

    public GitConfiguration setSshPassword(String sshPassword) {
        this.sshPassword = sshPassword;
        return this;
    }

}
