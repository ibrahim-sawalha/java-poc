package sawalha.git.server;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.FetchCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.PullCommand;
import org.eclipse.jgit.api.PushCommand;
import org.eclipse.jgit.api.Status;
import org.eclipse.jgit.api.StatusCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.JGitInternalException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.internal.storage.file.FileRepository;
import org.eclipse.jgit.lib.ConfigConstants;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.lib.StoredConfig;
import org.eclipse.jgit.transport.PushResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sawalha.git.command.MyTransportConfigCallback;
import sawalha.git.lib.LogTextProgressMonitor;
import sawalha.git.zip.Unzipper;

public class InstallerApp {
    private Repository srcLocalRepo;
    private Git git;
    private GitConfiguration config;

    final static Logger log = LoggerFactory.getLogger(InstallerApp.class.getName());

    private void updateLocalRepoConfig() throws IOException {
        srcLocalRepo = new FileRepository(config.getLocalRepoPath() + "\\.git");
        git = new Git(srcLocalRepo);
        StoredConfig gitConfig = git.getRepository().getConfig();
        gitConfig.setString(ConfigConstants.CONFIG_BRANCH_SECTION, "master", "remote", "origin");
        gitConfig.setString(ConfigConstants.CONFIG_BRANCH_SECTION, "master", "merge", "refs/heads/master");
        gitConfig.setString(ConfigConstants.CONFIG_REMOTE_SECTION, "origin", "url", config.getRemoteUrl());
        gitConfig.setString(ConfigConstants.CONFIG_REMOTE_SECTION, "origin", "fetch",
            "+refs/heads/*:refs/remotes/origin/*");
        gitConfig.save();
    }

    public InstallerApp(GitConfiguration config) {
        this.config = config;
    }

    public void pullFromRemoteServer() throws IOException, GitAPIException {
        updateLocalRepoConfig();
        srcLocalRepo = new FileRepository(config.getLocalRepoPath() + "\\.git");
        git = new Git(srcLocalRepo);
        PullCommand pullCommand = git.pull();
        pullCommand.setTransportConfigCallback(new MyTransportConfigCallback(config.getSshPassword()));
        pullCommand.setProgressMonitor(new LogTextProgressMonitor("pull"));

        log.info(pullCommand.call().getMergeResult().toString());
    }

    public void pushToRemoteServer() throws IOException, GitAPIException {
        srcLocalRepo = new FileRepository(config.getLocalRepoPath() + "\\.git");
        git = new Git(srcLocalRepo);
        PushCommand pushCommand = git.push();
        pushCommand.setTransportConfigCallback(new MyTransportConfigCallback(config.getSshPassword()));
        pushCommand.setProgressMonitor(new LogTextProgressMonitor("push"));

        Iterable<PushResult> pushResults = pushCommand.setForce(true).call();
        for (PushResult pushResult : pushResults) {
            log.info(pushResult.getMessages());
        }

    }

    public void fetchFromRemoteServer() throws IOException, GitAPIException {
        updateLocalRepoConfig();
        srcLocalRepo = new FileRepository(config.getLocalRepoPath() + "\\.git");
        git = new Git(srcLocalRepo);
        FetchCommand fetchCommand = git.fetch();
        fetchCommand.setTransportConfigCallback(new MyTransportConfigCallback(config.getSshPassword()));
        fetchCommand.setProgressMonitor(new LogTextProgressMonitor("fetch"));
        log.info(fetchCommand.call().getMessages());
    }

    public void status() throws IOException, GitAPIException {
        updateLocalRepoConfig();
        srcLocalRepo = new FileRepository(config.getLocalRepoPath() + "\\.git");
        git = new Git(srcLocalRepo);

        StatusCommand statusCommand = git.status();
        Status status = statusCommand.call();

        log.info("Added: " + status.getAdded());
        log.info("Changed: " + status.getChanged());
        log.info("Conflicting: " + status.getConflicting());
        log.info("ConflictingStageState: " + status.getConflictingStageState());
        log.info("IgnoredNotInIndex: " + status.getIgnoredNotInIndex());
        log.info("Missing: " + status.getMissing());
        log.info("Modified: " + status.getModified());
        log.info("Removed: " + status.getRemoved());
        log.info("Untracked: " + status.getUntracked());
        log.info("UntrackedFolders: " + status.getUntrackedFolders());

        for (String untracked : status.getUntracked()) {
            log.info("Adding untracked file: " + untracked);
            git.add().addFilepattern(untracked).call();
        }
        for (String modified : status.getModified()) {
            log.info("Adding modified file: " + modified);
            git.add().addFilepattern(modified).call();
        }
        for (String missed : status.getMissing()) {
            log.info("Removing missed file: " + missed);
            git.rm().addFilepattern(missed).call();
        }

        log.info("Committing changes... " + git.commit().setMessage("Ibrahim Automation").call().getFullMessage());

    }

    public boolean cloneRemoteServer() throws IOException, InvalidRemoteException, TransportException, GitAPIException {
        File localRepo = new File(config.getLocalRepoPath());
        if (localRepo.exists()) {
            localRepo.delete();
        }

        CloneCommand cloneCommand = Git.cloneRepository();
        cloneCommand.setURI(config.getRemoteUrl());
        cloneCommand.setProgressMonitor(new LogTextProgressMonitor("clone"));

        cloneCommand.setDirectory(new File(config.getLocalRepoPath()));

        cloneCommand.setTransportConfigCallback(new MyTransportConfigCallback(config.getSshPassword()));
        try {
            cloneCommand.call();
            return true;
        } catch (JGitInternalException e) {
            e.printStackTrace();
            return false;
        }

    }

    public static void main(String[] args) throws Exception {

        if (FileUtils.getFile("D:\\delete\\test-git\\unzip").exists()) {
            FileUtils.forceDelete(FileUtils.getFile("D:\\delete\\test-git\\unzip"));
        }

        Unzipper unZip = new Unzipper();

        GitConfiguration config = new GitConfiguration();
        config.setLocalRepoPath("D:\\delete\\test-git\\unzip\\release")
                .setRemoteUrl("ssh://ibrahim@10.3.7.25:8001/Git-Testing").setSshPassword("Ibrahim123!");

        InstallerApp main = new InstallerApp(config);

        if (!main.cloneRemoteServer()) {
            // main.fetchFromRemoteServer();
            // main.pullFromRemoteServer();
        }

        listFilesForFolder(FileUtils.getFile("D:\\delete\\test-git\\unzip\\release"));

        unZip.unZipIt("D:\\delete\\test-git\\release" + File.separator + "output.zip", "D:\\delete\\test-git\\unzip");

        main.status();
        main.pushToRemoteServer();

    }

    public static void listFilesForFolder(final File folder) throws IOException {
        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {
                if (!fileEntry.getName().equals(".git")) {
                    FileUtils.forceDelete(fileEntry);
                }
            } else {
                fileEntry.delete();
            }
        }
    }
}
