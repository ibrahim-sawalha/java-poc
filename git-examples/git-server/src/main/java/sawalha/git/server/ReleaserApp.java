package sawalha.git.server;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.FetchCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.PullCommand;
import org.eclipse.jgit.api.PushCommand;
import org.eclipse.jgit.api.Status;
import org.eclipse.jgit.api.StatusCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.internal.storage.file.FileRepository;
import org.eclipse.jgit.lib.ConfigConstants;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.lib.StoredConfig;

import sawalha.git.command.MyTransportConfigCallback;
import sawalha.git.lib.LogTextProgressMonitor;
import sawalha.git.zip.Zipper;

public class ReleaserApp {

    // private String localPath = "D:\\delete\\test-git";
    // private String remotePath = "ssh://ibrahim.sawalha@10.100.102.35:8001/Git-Testing";
    private Repository localRepo;
    private Git git;
    // private String password = "Ibrahim123!";

    private GitConfiguration config;

    public ReleaserApp(GitConfiguration config) throws IOException {
        this.config = config;
        // localRepo = new FileRepository(localPath);
        // git = new Git(localRepo);
    }

    public void pullFromRemoteServer() throws IOException, GitAPIException {
        localRepo = new FileRepository(config.getLocalRepoPath() + "\\.git");
        git = new Git(localRepo);
        PullCommand pullCommand = git.pull();
        pullCommand.setProgressMonitor(new LogTextProgressMonitor("clone"));
        pullCommand.setTransportConfigCallback(new MyTransportConfigCallback(config.getSshPassword()));
        StoredConfig gitConfig = git.getRepository().getConfig();
        gitConfig.setString(ConfigConstants.CONFIG_BRANCH_SECTION, "master", "remote", "origin");
        gitConfig.setString(ConfigConstants.CONFIG_BRANCH_SECTION, "master", "merge", "refs/heads/master");
        gitConfig.setString(ConfigConstants.CONFIG_REMOTE_SECTION, "origin", "url", config.getRemoteUrl());
        gitConfig.setString(ConfigConstants.CONFIG_REMOTE_SECTION, "origin", "fetch",
            "+refs/heads/*:refs/remotes/origin/*");
        gitConfig.save();
        System.out.println(pullCommand.call());
    }

    public void pushToRemoteServer() throws IOException, GitAPIException {
        localRepo = new FileRepository(config.getLocalRepoPath() + "\\.git");
        git = new Git(localRepo);
        PushCommand pushCommand = git.push();
        pushCommand.setProgressMonitor(new LogTextProgressMonitor("clone"));
        pushCommand.setTransportConfigCallback(new MyTransportConfigCallback(config.getSshPassword()));

        StoredConfig gitConfig = git.getRepository().getConfig();
        gitConfig.setString(ConfigConstants.CONFIG_BRANCH_SECTION, "master", "remote", "origin");
        gitConfig.setString(ConfigConstants.CONFIG_BRANCH_SECTION, "master", "merge", "refs/heads/master");
        gitConfig.setString(ConfigConstants.CONFIG_REMOTE_SECTION, "origin", "url", config.getRemoteUrl());
        gitConfig.setString(ConfigConstants.CONFIG_REMOTE_SECTION, "origin", "fetch",
            "+refs/heads/*:refs/remotes/origin/*");
        gitConfig.save();

        System.out.println(pushCommand.call());
    }

    public void fetchFromRemoteServer() throws IOException, GitAPIException {
        FetchCommand fetchCommand = git.fetch();
        fetchCommand.setProgressMonitor(new LogTextProgressMonitor("clone"));
        fetchCommand.setTransportConfigCallback(new MyTransportConfigCallback(config.getSshPassword()));
        System.out.println(fetchCommand.call());
    }

    public void status() throws IOException, GitAPIException {
        StatusCommand fetchCommand = git.status();
        Status status = fetchCommand.call();

        System.out.println("Added: " + status.getAdded());
        System.out.println("Changed: " + status.getChanged());
        System.out.println("Conflicting: " + status.getConflicting());
        System.out.println("ConflictingStageState: " + status.getConflictingStageState());
        System.out.println("IgnoredNotInIndex: " + status.getIgnoredNotInIndex());
        System.out.println("Missing: " + status.getMissing());
        System.out.println("Modified: " + status.getModified());
        System.out.println("Removed: " + status.getRemoved());
        System.out.println("Untracked: " + status.getUntracked());
        System.out.println("UntrackedFolders: " + status.getUntrackedFolders());

    }

    public boolean cloneRemoteServer() throws IOException, InvalidRemoteException, TransportException, GitAPIException {
        File localRepo = new File(config.getLocalRepoPath() + "\\.git");
        if (localRepo.exists()) {
            localRepo.delete();
        }

        CloneCommand cloneCommand = Git.cloneRepository();
        cloneCommand.setProgressMonitor(new LogTextProgressMonitor("clone"));
        cloneCommand.setURI(config.getRemoteUrl());

        cloneCommand.setDirectory(new File(config.getLocalRepoPath()));

        cloneCommand.setTransportConfigCallback(new MyTransportConfigCallback(config.getSshPassword()));
        try {
            cloneCommand.call();
            return true;
        } catch (org.eclipse.jgit.api.errors.JGitInternalException e) {
            return false;
        }

    }

    public static void main(String[] args) throws Exception {

        GitConfiguration config = new GitConfiguration();

        FileUtils.deleteDirectory(FileUtils.getFile("D:\\delete\\test-git\\release"));

        config.setLocalRepoPath("D:\\delete\\test-git\\release")
                .setRemoteUrl("ssh://ibrahim.sawalha@10.100.102.35:8001/Git-Testing").setSshPassword("Ibrahim123!");
        ReleaserApp main = new ReleaserApp(config);

        // - Clone new copy from Development Server
        // - Create new temp directory
        // - Create Directories structure based on selected files
        // - Copy files to their directories
        // - Zip the temp directory

        if (!main.cloneRemoteServer()) {
            main.pullFromRemoteServer();
        }

        Zipper appZip = new Zipper();
        appZip.generateFileList(new File(config.getLocalRepoPath() + File.separator));
        appZip.zipIt(config.getLocalRepoPath() + File.separator + "output.zip");

    }
}
