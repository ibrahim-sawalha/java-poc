package sawalha.git.lib;

import org.eclipse.jgit.lib.TextProgressMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LogTextProgressMonitor extends TextProgressMonitor {
    final static Logger log = LoggerFactory.getLogger(LogTextProgressMonitor.class.getName());
    private String actionName;

    public LogTextProgressMonitor(String actionName) {
        this.actionName = actionName;
    }

    @Override
    protected void onEndTask(String taskName, int workCurr) {
        // log.info(taskName + " --- " + workCurr);
    }

    @Override
    protected void onEndTask(String taskName, int cmp, int totalWork, int pcnt) {
        if ((pcnt / 10.0 == Math.floor(pcnt / 10.0)) && !Double.isInfinite(pcnt / 10.0)) {
            log.info(actionName + ":" + taskName + ":\t" + pcnt + "% (" + cmp + "/" + totalWork + ")");
        }
    }

    @Override
    protected void onUpdate(String taskName, int workCurr) {
        // log.info(taskName + " --- " + workCurr);
    }

    @Override
    protected void onUpdate(String taskName, int cmp, int totalWork, int pcnt) {
        if (((pcnt / 10.0) == Math.floor(pcnt / 10.0)) && !Double.isInfinite(pcnt / 10.0)) {
            log.info(actionName + ":" + taskName + ":\t" + pcnt + "% (" + cmp + "/" + totalWork + ")");
        }
    }

}
