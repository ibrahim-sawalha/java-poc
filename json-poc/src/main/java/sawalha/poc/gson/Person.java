package sawalha.poc.gson;

import java.util.ArrayList;
import java.util.List;

public class Person {

    private String name;
    private String location;
    private Exam exam;
    private List<String> books;

    public Exam getExam() {
        return exam;
    }

    public void setExam(Exam exam) {
        this.exam = exam;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public List<String> getBooks() {
        return books;
    }

    public void setBooks(List<String> books) {
        this.books = books;
    }

    public void addBook(String book) {
        if (books == null) {
            books = new ArrayList();
        }
        books.add(book);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Person [name=");
        builder.append(name);
        builder.append(", location=");
        builder.append(location);
        builder.append(", exam=");
        builder.append(exam);
        builder.append(", books=");
        builder.append(books);
        builder.append("]");
        return builder.toString();
    }

}
