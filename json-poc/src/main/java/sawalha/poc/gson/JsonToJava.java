package sawalha.poc.gson;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JsonToJava {

    public static void main(String[] args) throws IOException {
        writeGSon();
        readGSon();
        writeGSon2();
        readGSon2();
    }

    public static Person getPerson() {
        Person p = new Person();
        p.setName("ibrahim");
        p.setLocation("jordan");
        Exam e = new Exam();
        e.setGrade(90.21);
        e.setSubject("accounting");
        p.setExam(e);
        p.addBook("java book");
        p.addBook("sql book");
        p.addBook("programming book");
        return p;
    }

    public static void writeGSon() throws IOException {
        try (Writer writer = new OutputStreamWriter(new FileOutputStream("Output.json"), "UTF-8")) {
            Gson gson = new GsonBuilder().create();
            gson.toJson(getPerson(), writer);
        }
    }

    public static void readGSon() throws IOException {
        try (Reader reader = new InputStreamReader(new FileInputStream("Output.json"), "UTF-8")) {
            Gson gson = new GsonBuilder().create();
            Person p = gson.fromJson(reader, Person.class);
            System.out.println(p);
        }
    }

    public static void writeGSon2() {
        Gson gson = new Gson();
        String json = gson.toJson(getPerson());

        try {
            FileWriter writer = new FileWriter("Output2.json");
            writer.write(json);
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(json);
    }

    public static void readGSon2() {
        try {
            Gson gson = new Gson();
            BufferedReader br = new BufferedReader(new FileReader("Output2.json"));

            Person personObj = gson.fromJson(br, Person.class);

            System.out.println("Person object: " + personObj);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
