package sawalha.poc.gson;

public class Exam {

    private String subject;
    private double grade;

    public double getGrade() {
        return grade;
    }

    public void setGrade(double grade) {
        this.grade = grade;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Exam [subject=");
        builder.append(subject);
        builder.append(", grade=");
        builder.append(grade);
        builder.append("]");
        return builder.toString();
    }

}
