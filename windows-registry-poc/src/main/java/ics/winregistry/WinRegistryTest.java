package ics.winregistry;

public class WinRegistryTest {

    public static void main(String[] args) {
        System.out
                .println("System.getProperty(\"sun.arch.data.model\") = " + System.getProperty("sun.arch.data.model"));
        System.out.println("System.getProperty(\"os.arch\") = " + System.getProperty("os.arch"));
        System.out.println("System.getenv(\"PROCESSOR_ARCHITECTURE\") = " + System.getenv("PROCESSOR_ARCHITECTURE"));
        System.out.println("System.getenv(\"PROCESSOR_ARCHITEW6432\") = " + System.getenv("PROCESSOR_ARCHITEW6432"));

        String jvmArch = System.getProperty("sun.arch.data.model");
        if (jvmArch.equals("32")) { // 32-bit
            System.out.println("JVM 32-bit");
            // JavaUpdateRemover.removeWin32JavaUpdate();
        } else if (jvmArch.equals("64")) { // 64 - bit
            System.out.println("JVM 64-bit");
            // JavaUpdateRemover.removeWin64JavaUpdate();
        } else {

        }

        String OS = System.getProperty("os.name").toLowerCase();
        if ((OS.indexOf("win") >= 0)) {
            System.out.println("Windows");
        }

        String osArch = System.getenv("PROCESSOR_ARCHITECTURE");
        if (osArch.contains("86")) { // 32-bit
            System.out.println("Windows 32-bit");
            // JavaUpdateRemover.removeWin32JavaUpdate();
        } else if (osArch.contains("64")) { // 64 - bit
            System.out.println("Windows 64-bit");
            // JavaUpdateRemover.removeWin64JavaUpdate();
        } else {

        }
    }
}
