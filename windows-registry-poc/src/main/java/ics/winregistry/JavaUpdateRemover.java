package ics.winregistry;

import java.lang.reflect.InvocationTargetException;

public class JavaUpdateRemover {

    public static void removeWindowsJavaUpdateSched() {
        String OS = System.getProperty("os.name").toLowerCase();
        if ((OS.indexOf("win") >= 0)) {
            String osArch = System.getenv("PROCESSOR_ARCHITECTURE");
            if (osArch.contains("86")) { // 32-bit
                System.out.println("Windows 32-bit");
                removeWin32JavaUpdateSched();
            } else if (osArch.contains("64")) { // 64 - bit
                System.out.println("Windows 64-bit");
                removeWin64JavaUpdateSched();
            } else {
                System.err.println("System.getenv(\"PROCESSOR_ARCHITECTURE\") = "
                        + System.getenv("PROCESSOR_ARCHITECTURE"));
            }

        }

    }

    private static void removeWin32JavaUpdateSched() {
        String sunJavaUpdateSchedPath = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run";
        String sunJavaUpdateSchedKey = "SunJavaUpdateSched";
        String enableJavaUpdatePath = "SOFTWARE\\JavaSoft\\Java Update\\Policy";
        String enableJavaUpdateKey = "EnableJavaUpdate";
        try {
            WinRegistry.deleteValue(WinRegistry.HKEY_LOCAL_MACHINE, // hkey
                sunJavaUpdateSchedPath, // path
                sunJavaUpdateSchedKey // key
                    );
            WinRegistry.deleteValue(WinRegistry.HKEY_LOCAL_MACHINE, // hkey
                enableJavaUpdatePath, // path
                enableJavaUpdateKey // key
                    );
        } catch (IllegalArgumentException e) {
            System.out.println("1");
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            System.out.println("2");
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            System.out.println("3");
            e.printStackTrace();
        }
    }

    private static void removeWin64JavaUpdateSched() {
        String sunJavaUpdateSchedPath = "SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Run";
        String sunJavaUpdateSchedKey = "SunJavaUpdateSched";
        String enableJavaUpdatePath = "SOFTWARE\\Wow6432Node\\JavaSoft\\Java Update\\Policy";
        String enableJavaUpdateKey = "EnableJavaUpdate";
        try {
            WinRegistry.deleteValue(WinRegistry.HKEY_LOCAL_MACHINE, // hkey
                sunJavaUpdateSchedPath, // path
                sunJavaUpdateSchedKey // key
                    );
            WinRegistry.deleteValue(WinRegistry.HKEY_LOCAL_MACHINE, // hkey
                enableJavaUpdatePath, // path
                enableJavaUpdateKey // key
                    );
        } catch (IllegalArgumentException e) {
            System.out.println("1");
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            System.out.println("2");
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            System.out.println("3");
            e.printStackTrace();
        }
    }

}
