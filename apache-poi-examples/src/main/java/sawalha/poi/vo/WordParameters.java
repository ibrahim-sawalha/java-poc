package sawalha.poi.vo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class WordParameters {

    private List<WordTable> table;
    private List<WordText> text;

    public List<WordTable> getTable() {
        if (table == null) {
            table = new ArrayList<WordTable>();
        }
        return table;
    }

    public void setTable(List<WordTable> table) {
        this.table = table;
    }

    public List<WordText> getText() {
        if (text == null) {
            text = new ArrayList<WordText>();
        }
        return text;
    }

    public void setText(List<WordText> text) {
        this.text = text;
    }

    public void addText(WordText text) {
        getText().add(text);
    }

    public void addTable(WordTable table) {
        getTable().add(table);
    }

}
