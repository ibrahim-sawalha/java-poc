package sawalha.poi.vo;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class WordText {
    private String textId;
    private String replacement;

    public WordText() {

    }

    public WordText(String textId, String replacement) {
        super();
        this.textId = textId;
        this.replacement = replacement;
    }

    public String getTextId() {
        return textId;
    }

    public void setTextId(String textId) {
        this.textId = textId;
    }

    public String getReplacement() {
        return replacement;
    }

    public void setReplacement(String replacement) {
        this.replacement = replacement;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("WordText [textId=");
        builder.append(textId);
        builder.append(", replacement=");
        builder.append(replacement);
        builder.append("]");
        return builder.toString();
    }
}
