package sawalha.poi.vo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class WordRow {

    private List<String> rowColumns;

    public List<String> getRowColumns() {
        if (rowColumns == null) {
            rowColumns = new ArrayList<String>();
        }
        return rowColumns;
    }

    public void setRowColumns(List<String> rowColumns) {
        this.rowColumns = rowColumns;
    }

    public WordRow addRowCell(String rowColumn) {
        getRowColumns().add(rowColumn);
        return this;
    }

}
