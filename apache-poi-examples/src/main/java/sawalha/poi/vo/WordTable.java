package sawalha.poi.vo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class WordTable {

    private String tableId;
    private List<WordRow> rows;

    public WordTable() {

    }

    public WordTable(String tableId) {
        this.tableId = tableId;
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public List<WordRow> getRows() {
        if (rows == null) {
            rows = new ArrayList<WordRow>();
        }
        return rows;
    }

    public void setRows(List<WordRow> rows) {
        this.rows = rows;
    }

    public WordTable addRow(WordRow row) {
        getRows().add(row);
        return this;
    }

}
