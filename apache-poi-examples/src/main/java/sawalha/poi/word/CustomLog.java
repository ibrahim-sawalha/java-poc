package sawalha.poi.word;

import java.nio.charset.Charset;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.FileAppender;
import ch.qos.logback.core.util.StatusPrinter;

public class CustomLog {

    private static Logger logbackLogger = null;

    public static void main(String[] args) {

        // log something
        getLooger("customLog.log").debug("hello");
        getLooger("customLog.log").debug("hello");
        getLooger("customLog.log").debug("hello");
        getLooger("customLog.log").debug("hello");
    }

    public static Logger getLooger(String logFile) {
        if (logbackLogger == null) {
            LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();

            FileAppender<ILoggingEvent> fileAppender = new FileAppender<ILoggingEvent>();
            fileAppender.setContext(loggerContext);
            fileAppender.setName("timestamp");

            // set the file name
            // System.out.println(new File(logFile).getAbsoluteFile());
            fileAppender.setFile(logFile);

            PatternLayoutEncoder encoder = new PatternLayoutEncoder();
            encoder.setContext(loggerContext);
            encoder.setPattern("[%-5level] %message%n");
            encoder.setCharset(Charset.forName("UTF-8"));
            encoder.start();

            fileAppender.setEncoder(encoder);
            fileAppender.start();

            // attach the rolling file appender to the logger of your choice
            logbackLogger = loggerContext.getLogger("Main");
            logbackLogger.addAppender(fileAppender);

            // OPTIONAL: print logback internal status messages
            StatusPrinter.print(loggerContext);
        }
        return logbackLogger;
    }

}
