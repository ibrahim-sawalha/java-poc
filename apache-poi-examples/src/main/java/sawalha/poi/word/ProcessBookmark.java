package sawalha.poi.word;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBookmark;
import org.w3c.dom.Node;

import sawalha.poi.vo.WordText;

public class ProcessBookmark {

    private XWPFDocument document = null;
    private List<WordText> textsToProcess;

    public ProcessBookmark(XWPFDocument document) {
        this.document = document;
    }

    public final void addBookmarkToProcess(String bookmarkName, String bookmarkValue) {
        if (textsToProcess == null) {
            textsToProcess = new ArrayList<WordText>();
        }
        textsToProcess.add(new WordText(bookmarkName, bookmarkValue));
    }

    public XWPFDocument startProcessingBookmarks() {
        if (document == null) {
            throw new RuntimeException(
                    "Input/Output Filename was not specified. Use setInputFile(String)/setOutputFile(String) methods.");
        }
        if (textsToProcess != null) {
            for (WordText wordText : textsToProcess) {
                processBookmark(wordText.getTextId(), wordText.getReplacement());
            }
        }
        return document;
    }

    private final void processBookmark(String bookmarkName, String bookmarkValue) {
        List<XWPFTable> tableList = null;
        Iterator<XWPFTable> tableIter = null;
        List<XWPFTableRow> rowList = null;
        Iterator<XWPFTableRow> rowIter = null;
        List<XWPFTableCell> cellList = null;
        Iterator<XWPFTableCell> cellIter = null;
        XWPFTable table = null;
        XWPFTableRow row = null;
        XWPFTableCell cell = null;

        // Firstly, deal with any paragraphs in the body of the document.
        this.procParaList(this.document.getParagraphs(), bookmarkName, bookmarkValue);

        // Then check to see if there are any bookmarks in table cells. To do this
        // it is necessary to get at the list of paragraphs 'stored' within the
        // individual table cell, hence this code which get the tables from the
        // document, the rows from each table, the cells from each row and the
        // paragraphs from each cell.
        tableList = this.document.getTables();
        tableIter = tableList.iterator();
        while (tableIter.hasNext()) {
            table = tableIter.next();
            rowList = table.getRows();
            rowIter = rowList.iterator();
            while (rowIter.hasNext()) {
                row = rowIter.next();
                cellList = row.getTableCells();
                cellIter = cellList.iterator();
                while (cellIter.hasNext()) {
                    cell = cellIter.next();
                    this.procParaList(cell.getParagraphs(), bookmarkName, bookmarkValue);
                }
            }
        }
    }

    private final void procParaList(List<XWPFParagraph> paraList, String bookmarkName, String bookmarkValue) {
        Iterator<XWPFParagraph> paraIter = null;
        XWPFParagraph para = null;
        List<CTBookmark> bookmarkList = null;
        Iterator<CTBookmark> bookmarkIter = null;
        CTBookmark bookmark = null;
        XWPFRun run = null;
        Node nextNode = null;

        // Get an Iterator to step through the contents of the paragraph list.
        paraIter = paraList.iterator();
        while (paraIter.hasNext()) {
            // Get the paragraph, a llist of CTBookmark objects and an Iterator
            // to step through the list of CTBookmarks.
            para = paraIter.next();
            bookmarkList = para.getCTP().getBookmarkStartList();
            bookmarkIter = bookmarkList.iterator();

            while (bookmarkIter.hasNext()) {
                // Get a Bookmark and check it's name. If the name of the
                // bookmark matches the name the user has specified...
                bookmark = bookmarkIter.next();
                if (bookmark.getName().equals(bookmarkName)) {
                    // ...create the text run to insert and set it's text
                    // content and then insert that text into the document.
                    run = para.createRun();
                    run.setText(bookmarkValue);
                    // The new Run should be inserted between the bookmarkStart
                    // and bookmarkEnd nodes, so find the bookmarkEnd node.
                    // Note that we are looking for the next sibling of the
                    // bookmarkStart node as it does not contain any child nodes
                    // as far as I am aware.
                    nextNode = bookmark.getDomNode().getNextSibling();
                    // If the next node is not the bookmarkEnd node, then step
                    // along the sibling nodes, until the bookmarkEnd node
                    // is found. As the code is here, it will remove anything
                    // it finds between the start and end nodes. This, of course
                    // comepltely sidesteps the issues surrounding boorkamrks
                    // that contain other bookmarks which I understand can happen.
                    // if (nextNode != null && nextNode.getNodeName() != null) {
                    while (nextNode != null && nextNode.getNodeName() != null
                            && !(nextNode.getNodeName().contains("bookmarkEnd"))) {
                        para.getCTP().getDomNode().removeChild(nextNode);
                        nextNode = bookmark.getDomNode().getNextSibling();
                    }
                    // }

                    // Finally, insert the new Run node into the document
                    // between the bookmarkStrat and the bookmarkEnd nodes.
                    para.getCTP().getDomNode().insertBefore(run.getCTR().getDomNode(), nextNode);
                }
            }
        }
    }

    public static void main(String[] args) {
        try {
            FileInputStream fis = new FileInputStream("d:/delete/mahmoud.docx");
            ProcessBookmark docxTest = new ProcessBookmark(new XWPFDocument(fis));
            // docxTest.openFile("d:/delete/mahmoud.docx");
            docxTest.processBookmark("V_TEST", "This should be inserted at the WHOLE_WORD bookmark.");

            // docxTest.saveAs("d:/delete/mahmoud-bookmark-out.docx");
        } catch (Exception ex) {
            System.out.println("Caught a: " + ex.getClass().getName());
            System.out.println("Message: " + ex.getMessage());
            System.out.println("Stacktrace follows:.....");
            ex.printStackTrace(System.out);
        }
    }
}
