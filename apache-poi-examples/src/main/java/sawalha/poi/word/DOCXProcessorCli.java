package sawalha.poi.word;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

import javax.xml.stream.XMLStreamException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
// import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import ch.qos.logback.classic.Logger;
import sawalha.poi.vo.WordParameters;
import sawalha.poi.vo.WordTable;
import sawalha.poi.vo.WordText;

public class DOCXProcessorCli {// extends DOCXProcessor {
    final static Logger log = (Logger) LoggerFactory.getLogger(DOCXProcessorCli.class);
    final static Logger analysisLog = (Logger) LoggerFactory.getLogger("analysis");
    private String[] args = null;

    private String srcFile;
    private String destFile;
    private DOCXProcessor docxProcessor = new DOCXProcessor();

    private ValidateXmlFile validateXmlFile = new ValidateXmlFile();

    private WordParameters params = new WordParameters();

    private Options options = new Options();
    private String varXmlFile;
    private String tabXmlFile;
    private String flixyXmlFile;

    public DOCXProcessorCli(String[] args) throws IOException {
        this.args = args;

        // withLongOpt("integer-option").withDescription("description").withType(String.class).hasArg()
        // .withArgName("argname").create());

        options.addOption("h", "help", false, "show help.");
        options.addOption("s", "src", true, "Source docx file.");
        options.addOption("d", "des", true, "Destination docx file.");

        options.addOption("b", "table", true, "Table Data xml file.");
        options.addOption("v", "var", true, "Variables Data xml file.");
        options.addOption("f", "flixy", true, "Flixy Fields Data xml file.");

        options.addOption("t", "test", false, "Test the tmplate docx file integration with data xml file.");

        File logFile = new File("logback-debug.xml");
        if (!logFile.exists()) {
            String text = IOUtils.toString(this.getClass().getClassLoader().getResourceAsStream("logback-debug.xml"));
            FileUtils.write(logFile, text, Charset.forName("UTF-8"), false);
        }
    }

    public void parse() throws SAXException, IOException {
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = null;
        try {
            cmd = parser.parse(options, args);

            if (cmd.hasOption("h")) {
                help();
            }

            if (cmd.hasOption("s") && cmd.hasOption("d")
                    && (cmd.hasOption("b") || cmd.hasOption("v") || cmd.hasOption("f"))) {
                srcFile = cmd.getOptionValue("s");
                destFile = cmd.getOptionValue("d");

                varXmlFile = cmd.getOptionValue("v");
                tabXmlFile = cmd.getOptionValue("b");
                flixyXmlFile = cmd.getOptionValue("f");

                log.info("Source file= " + cmd.getOptionValue("s"));
                log.info("Destination file= " + cmd.getOptionValue("d"));
                log.info("Variables file= " + cmd.getOptionValue("v"));
                log.info("Tables file= " + cmd.getOptionValue("b"));
                log.info("Flixy Fields file= " + cmd.getOptionValue("f"));

                try {
                    processTables(tabXmlFile);
                    processVariables(varXmlFile);
                    processFlixyFields(flixyXmlFile);

                    docxProcessor.setInputFile(srcFile);
                    docxProcessor.setOutputFile(destFile);

                    for (WordTable wordTable : params.getTable()) {
                        String id = wordTable.getTableId().toUpperCase();
                        if (id.startsWith("T_") && Character.isDigit(id.charAt(id.length() - 1))) {
                            docxProcessor.addTableToProcess(wordTable);
                        } else {
                            log.warn("Table ID [{}] was ignored. table ID should start with T_ and ends with number.",
                                id);
                        }
                    }
                    for (WordText wordText : params.getText()) {
                        String id = wordText.getTextId().toUpperCase();
                        if ((id.startsWith("V_") || (id.startsWith("F_")))
                                && Character.isDigit(id.charAt(id.length() - 1))) {
                            docxProcessor.addTextToProcess(wordText.getTextId(), wordText.getReplacement());
                        } else {
                            log.warn(
                                "Variable ID [{}] was ignored. variable ID should start with V_ or F_ and ends with number.",
                                id);
                        }
                    }

                    if (cmd.hasOption("t")) {
                        DOCXVerification ver = new DOCXVerification(docxProcessor);
                        ver.startProcessing();
                    } else {
                        docxProcessor.startProcessing();
                        log.info("Finished Processing.");
                    }
                } catch (IOException e) {
                    log.error("{} ", "Stacktrace follows:.....", e);
                } catch (InvalidFormatException e) {
                    log.error("{} ", "Stacktrace follows:.....", e);
                } catch (XMLStreamException e) {
                    log.error("{} ", "Stacktrace follows:.....", e);
                }
            } else {
                help();
            }
        } catch (ParseException e) {
            log.error("Failed to parse comand line properties", e);
            help();
        }
    }

    private void help() {
        HelpFormatter formater = new HelpFormatter();
        StringBuffer header = new StringBuffer("\nICSFS Microsoft Word Processor\n\n");
        StringBuffer footer = new StringBuffer("\nTo enable logging use -Dlogback.configurationFile=logback-debug.xml");
        footer.append("\nNotes:\n");
        footer.append(
            "- Variables should starts with V_ or FF_ and ends with at least one number or it will be ignored\n");
        footer.append(
            "- Table file name starts from character 0 until first \".\" is used as TableID, example T_TEST.something.xml -> tableID = T_TEST");
        formater.printHelp("java -jar [JARNAME]", header.toString(), options, footer.toString(), true);
        System.exit(0);
    }

    private void processTables(String xmlFile) throws IOException, XMLStreamException {
        if (xmlFile == null || xmlFile.isEmpty()) {
            return;
        }
        List<File> files = FilesParser.getFiles(new File(xmlFile));
        if (files != null && files.size() > 0) {
            StaxParser sample = new StaxParser();
            for (File f : files) {
                if (validateXmlFile.isXmlFileOracleValid(f)) {
                    log.debug("Table: Processing file " + f.getAbsolutePath());
                    params.addTable(sample.processTable(f));
                }
            }
        } else {
            log.warn("No Files Found @ [{}]", xmlFile);
        }
    }

    private void processFlixyFields(String xmlFile) throws IOException, XMLStreamException {
        if (xmlFile == null || xmlFile.isEmpty()) {
            return;
        }
        List<File> files = FilesParser.getFiles(new File(xmlFile));

        if (files != null) {
            StaxParser sample = new StaxParser();
            for (File f : files) {
                if (validateXmlFile.isXmlFileOracleValid(f)) {
                    log.debug("Flixy: Processing file " + f.getAbsolutePath());
                    List<WordText> tmp = params.getText();
                    tmp.addAll(sample.processFlixyFields(f));
                    params.setText(tmp);
                }
            }
        } else {
            log.warn("No Files Found @ [{}]", xmlFile);
        }

    }

    private void processVariables(String xmlFile) throws IOException, XMLStreamException {
        if (xmlFile == null || xmlFile.isEmpty()) {
            return;
        }
        List<File> files = FilesParser.getFiles(new File(xmlFile));

        if (files != null) {
            StaxParser sample = new StaxParser();
            for (File f : files) {
                if (validateXmlFile.isXmlFileOracleValid(f)) {
                    log.debug("Variable: Processing file " + f.getAbsolutePath());
                    List<WordText> tmp = params.getText();
                    tmp.addAll(sample.processVariables(f));
                    params.setText(tmp);
                }
            }
        } else {
            log.warn("No Files Found @ [{}]", xmlFile);
        }

    }

    public static void main(String[] args) {
        try {
            new DOCXProcessorCli(args).parse();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
