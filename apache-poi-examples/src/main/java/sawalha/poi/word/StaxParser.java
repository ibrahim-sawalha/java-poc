package sawalha.poi.word;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import sawalha.poi.vo.WordParameters;
import sawalha.poi.vo.WordRow;
import sawalha.poi.vo.WordTable;
import sawalha.poi.vo.WordText;

public class StaxParser {

    public List<WordText> processFlixyFields(File xmlFile) throws XMLStreamException {
        List<WordText> varList = new ArrayList<WordText>();
        StringBuffer tagContent = null;
        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLStreamReader reader = null;
        try {
            reader = factory.createXMLStreamReader(new FileInputStream(xmlFile));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        String textId = "";
        String replacement = "";
        while (reader.hasNext()) {
            int event = reader.next();
            switch (event) {
            case XMLStreamConstants.START_ELEMENT:
                tagContent = new StringBuffer("");
                break;

            case XMLStreamConstants.CHARACTERS:
                tagContent.append(reader.getText().trim());
                break;

            case XMLStreamConstants.END_ELEMENT:
                if (reader.getLocalName().equals("ITM_ID")) {
                    textId = tagContent.toString();
                } else if (reader.getLocalName().equals("ITM_VAL")) {
                    replacement = tagContent.toString();
                }
                if (reader.getLocalName().equals("ROW")) {
                    varList.add(new WordText(textId, replacement));
                }
                break;

            case XMLStreamConstants.START_DOCUMENT:
                break;
            }
        }
        return varList;
    }

    public List<WordText> processVariables(File xmlFile) throws XMLStreamException {
        List<WordText> varList = new ArrayList<WordText>();
        StringBuffer tagContent = null;
        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLStreamReader reader = null;
        try {
            reader = factory.createXMLStreamReader(new FileInputStream(xmlFile));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        while (reader.hasNext()) {
            int event = reader.next();
            switch (event) {
            case XMLStreamConstants.START_ELEMENT:
                tagContent = new StringBuffer("");
                break;

            case XMLStreamConstants.CHARACTERS:
                tagContent.append(reader.getText().trim());
                break;

            case XMLStreamConstants.END_ELEMENT:
                if (!reader.getLocalName().equals("ROW") && !reader.getLocalName().equals("ROWSET")) {
                    varList.add(new WordText(reader.getLocalName(), tagContent.toString()));
                }
                break;

            case XMLStreamConstants.START_DOCUMENT:
                break;
            }
        }
        return varList;
    }

    public WordTable processTable(File xmlFile) throws XMLStreamException {

        WordTable currVar = new WordTable();
        currVar.setTableId(xmlFile.getName().substring(0, xmlFile.getName().indexOf(".")));
        WordRow row = null;
        StringBuffer tagContent = null;
        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLStreamReader reader = null;
        try {
            reader = factory.createXMLStreamReader(new FileInputStream(xmlFile));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        while (reader.hasNext()) {
            int event = reader.next();
            switch (event) {
            case XMLStreamConstants.START_ELEMENT:
                tagContent = new StringBuffer("");
                if ("ROW".equals(reader.getLocalName())) {
                    row = new WordRow();
                }
                break;

            case XMLStreamConstants.CHARACTERS:
                tagContent.append(reader.getText().trim());
                break;

            case XMLStreamConstants.END_ELEMENT:
                if (!reader.getLocalName().equals("ROW") && !reader.getLocalName().equals("ROWSET")) {
                    row.addRowCell(tagContent.toString());
                } else if (reader.getLocalName().equals("ROW")) {
                    currVar.addRow(row);
                }
                break;

            case XMLStreamConstants.START_DOCUMENT:
                break;
            }

        }
        return currVar;
    }

    public static void main(String[] args) throws XMLStreamException {
        StaxParser sample = new StaxParser();
        WordParameters params = new WordParameters();
        // params.addTable(sample.processTable(new File("d:/delete/tables.xml")));
        params.setText(sample.processFlixyFields(new File("flixyfields.xml")));
        System.out.println("---------------------------------------");
        for (WordText text : params.getText()) {
            System.out.println(text);
        }
        params.setText(sample.processVariables(new File("d:/delete/vars.xml")));
    }
}
