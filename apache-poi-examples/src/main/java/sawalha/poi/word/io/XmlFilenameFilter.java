package sawalha.poi.word.io;

import java.io.File;
import java.io.FilenameFilter;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.filefilter.IOFileFilter;

public class XmlFilenameFilter implements FilenameFilter, IOFileFilter {

    private String startsWith = "";

    public XmlFilenameFilter(String startsWith) {
        this.startsWith = startsWith;
    }

    public XmlFilenameFilter(File file) {
        this.startsWith = file.getName();
    }

    @Override
    public boolean accept(File dir, String name) {
        return FilenameUtils.wildcardMatch(name, startsWith);
    }

    @Override
    public boolean accept(File file) {
        return FilenameUtils.wildcardMatch(file.getName(), startsWith);
    }

}
