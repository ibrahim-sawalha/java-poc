package sawalha.poi.word;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.profiler.Profiler;

import sawalha.poi.vo.WordParameters;
import sawalha.poi.vo.WordRow;
import sawalha.poi.vo.WordTable;
import sawalha.poi.vo.WordText;

public class DOCXProcessor {
    final static Logger log = LoggerFactory.getLogger(DOCXProcessor.class);
    private int allTableCount = 0;
    private int tableCount = 0;
    private int textCount = 0;
    private XWPFDocument document = null;
    private File outputFile = null;

    private List<WordTable> tablesToProcess;
    private List<WordText> textsToProcess;

    private Profiler profiler = new Profiler("BASIC");
    private String filename;

    public DOCXProcessor() {
        profiler.setLogger(DOCXProcessorCli.analysisLog);
    }

    public final void openFile(InputStream is) throws IOException {
        try {
            this.document = new XWPFDocument(is);
        } finally {
            try {
                if (is != null) {
                    is.close();
                    is = null;
                }
            } catch (IOException ioEx) {
                // Swallow this exception. It would have occurred only
                // when releasing the file handle and should not pose
                // problems to later processing.
            }
        }
    }

    public final void setInputFile(String filename) throws IOException {
        this.filename = filename;
        File file = null;
        FileInputStream fis = null;
        try {
            // Simply open the file and store a reference into the 'document'
            // local variable.
            file = new File(filename);
            fis = new FileInputStream(file);
            this.document = new XWPFDocument(fis);
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                    fis = null;
                }
            } catch (IOException ioEx) {
                // Swallow this exception. It would have occurred only
                // when releasing the file handle and should not pose
                // problems to later processing.
            }
        }
    }

    public final void saveAs(OutputStream os) throws IOException {
        try {
            this.document.write(os);
        } finally {
            if (os != null) {
                os.close();
                os = null;
            }
        }
    }

    public final void setOutputFile(String outputFileName) {
        this.outputFile = new File(outputFileName);
    }

    private final void saveToOutputFile() throws IOException {
        if (outputFile == null) {
            throw new RuntimeException();
        }
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(outputFile);
            this.document.write(fos);
        } finally {
            if (fos != null) {
                fos.close();
                fos = null;
            }
        }
    }

    private void addRow(int rowIndex, XWPFTable table, List<String> cells) {
        XWPFTableRow tableRow = new XWPFTableRow(table.getRow(rowIndex).getCtRow(), table);
        tableRow.setRepeatHeader(true);
        table.addRow(tableRow, rowIndex);
        int i = 0;
        for (XWPFTableCell cell : tableRow.getTableCells()) {
            // remove any text
            for (XWPFParagraph paragraph : cell.getParagraphs()) {
                for (XWPFRun run : paragraph.getRuns()) {
                    run.setText("", 0);
                    run.setBold(false);
                }
            }
            // set background color to white.
            cell.setColor("FFFFFF");
        }
        for (String data : cells) {
            // set the new text
            try {
                tableRow.getCell(i).setText(data);
            } catch (NullPointerException e) {
                // ignore
            }
            ++i;
        }
    }

    private void processSingleTable(XWPFTable table, WordTable wordTable) {
        if (table.getRow(0).getCell(0).getText().contains(wordTable.getTableId())) {
            // update column data
            for (XWPFParagraph paragraph : table.getRow(0).getCell(0).getParagraphs()) {
                String text = table.getRow(0).getCell(0).getText().replace(wordTable.getTableId(), "");
                List<XWPFRun> runs = paragraph.getRuns();
                for (int i = runs.size() - 1; i > 0; i--) {
                    paragraph.removeRun(i);
                }
                try {
                    XWPFRun run = runs.get(0);
                    run.setText(text, 0);
                } catch (Exception e) {
                    // ignore
                }
                tableCount++;
            }
            int i = 0;
            for (WordRow wordRow : wordTable.getRows()) {
                addRow(i++, table, wordRow.getRowColumns());
            }
        } else {
            for (XWPFTableRow row : table.getRows()) {
                for (XWPFTableCell cell : row.getTableCells()) {
                    if (cell.getTables() != null && cell.getTables().size() > 0) {
                        for (XWPFTable xwpfTable : cell.getTables()) {
                            ++allTableCount;
                            processSingleTable(xwpfTable, wordTable);
                        }
                    }
                }
            }
        }
    }

    private void processSingleTableText(XWPFTable table, String src, String replacement) {
        for (XWPFTableRow row : table.getRows()) {
            for (XWPFTableCell cell : row.getTableCells()) {
                if (cell.getTables() != null && cell.getTables().size() > 0) {
                    for (XWPFTable xwpfTable : cell.getTables()) {
                        processSingleTableText(xwpfTable, src, replacement);
                    }
                }

                for (XWPFParagraph p : cell.getParagraphs()) {

                    if (p.getText() != null && p.getText().contains(src)) {
                        // String text = p.getText().replace(src, replacement);
                        // textCount++;
                        List<XWPFRun> runs = p.getRuns();
                        if (runs != null) {
                            for (XWPFRun r : runs) {
                                // String text = r.getText(0);
                                String text = r.text();
                                if (text != null && text.contains(src)) {
                                    text = text.replace(src, replacement);
                                    r.setText(text, 0);
                                    textCount++;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void addTextToProcess(String src, String replacement) {
        if (textsToProcess == null) {
            textsToProcess = new ArrayList<WordText>();
        }
        textsToProcess.add(new WordText(src, replacement));
    }

    public void addTableToProcess(WordTable wordTable) {
        if (tablesToProcess == null) {
            tablesToProcess = new ArrayList<WordTable>();
        }
        tablesToProcess.add(wordTable);
    }

    public void startProcessing() throws InvalidFormatException, IOException {
        if (document == null || outputFile == null) {
            throw new RuntimeException(
                    "Input/Output Filename was not specified. Use setInputFile(String)/setOutputFile(String) methods.");
        }
        if (textsToProcess != null) {
            // ProcessBookmark processBookmark = new ProcessBookmark(document);
            for (WordText wordText : textsToProcess) {
                processText(wordText.getTextId(), wordText.getReplacement());
                // processBookmark.addBookmarkToProcess(wordText.getTextId(), wordText.getReplacement());
            }
            // document = processBookmark.startProcessingBookmarks();
        }
        if (tablesToProcess != null) {
            for (WordTable wordTable : tablesToProcess) {
                processTables(wordTable);
            }
        }
        saveToOutputFile();

        profiler.stop().log();// print();
    }

    private void processText(String src, String replacement) throws InvalidFormatException, IOException {
        // log.info("Processing variable: {}", src);
        profiler.start("Process Text [" + src + "]");
        for (XWPFParagraph p : document.getParagraphs()) {
            if (p.getText() != null && p.getText().contains(src)) {
                log.info("Found --> {}", p.getText());
                List<XWPFRun> runs = p.getRuns();
                if (runs != null) {
                    for (XWPFRun r : runs) {
                        String text = r.text();
                        if (text != null && text.equals(src)) {
                            text = text.replace(src, replacement);
                            r.setText(text, 0);
                            textCount++;
                        }
                    }
                }
            }
        }
        for (XWPFTable tbl : document.getTables()) {
            processSingleTableText(tbl, src, replacement);
        }
        log.info("Total number of variables processed for ID [{}] is: {} variable(s).", src, textCount);
        textCount = 0;
        // profiler.stop();
    }

    private void processTables(WordTable wordTable) {
        // log.info("Processing tableID:{}", wordTable.getTableId());
        profiler.start("Process Table [" + wordTable.getTableId() + "]");
        if (wordTable.getRows().size() < 1) {
            log.warn("tableID:{} does not have any defined rows. It will not be processed.", wordTable.getTableId());
            return;
        }

        for (XWPFTable tbl : document.getTables()) {
            processSingleTable(tbl, wordTable);
        }
        log.info("Total number of tables processed for tableID [{}] is: {} table(s) out of {} table(s).",
            wordTable.getTableId(), tableCount, allTableCount);
        allTableCount = 0;
        tableCount = 0;
        // profiler.stop();
    }

    public WordParameters getWrordParametersXml(File xmlFile) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(WordParameters.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        return (WordParameters) jaxbUnmarshaller.unmarshal(xmlFile);
    }

    public void generateWrordParametersXml(WordParameters param, String xmlFile) throws JAXBException {
        File file = new File(xmlFile);
        JAXBContext jaxbContext = JAXBContext.newInstance(WordParameters.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        // output pretty printed
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        jaxbMarshaller.marshal(param, file);
        // jaxbMarshaller.marshal(param, System.out);
    }

    public static void main(String[] args) {
        try {

            WordParameters param = new WordParameters();

            DOCXProcessor docxProcessor = new DOCXProcessor();
            docxProcessor.setInputFile("d:/delete/test.docx");
            docxProcessor.setOutputFile("d:/delete/Doc1 With Bookmarks Updated.docx");

            WordTable wordTable = new WordTable("#table");
            wordTable.addRow(new WordRow().addRowCell("Row1-Col1").addRowCell("Row1-Col2"));
            wordTable.addRow(new WordRow().addRowCell("").addRowCell("Row2-Col2").addRowCell("Row2-Col3"));
            wordTable.addRow(new WordRow().addRowCell("Row3-Col1").addRowCell("Row3-Col2").addRowCell("Row3-Col3"));
            docxProcessor.addTableToProcess(wordTable);
            param.addTable(wordTable);

            docxProcessor.addTableToProcess(new WordTable("#xxx"));

            docxProcessor.addTextToProcess("#total", "The-Total");
            docxProcessor.addTextToProcess("#customer", "العميل");

            param.addText(new WordText("#total", "The-Total"));
            param.addText(new WordText("#customer", "العميل"));

            docxProcessor.startProcessing();

            docxProcessor.generateWrordParametersXml(param, "d:\\delete\\word.xml");

            // profiler.stop().print();
        } catch (Exception ex) {
            log.error("{}", "Caught a: " + ex.getClass().getName());
            log.error("{}", "Message: ", ex.getMessage());
            log.error("{} ", "Stacktrace follows:.....", ex);
        }
    }

    public List<WordText> getTextsToProcess() {
        return textsToProcess;
    }

    public void setTextsToProcess(List<WordText> textsToProcess) {
        this.textsToProcess = textsToProcess;
    }

    public List<WordTable> getTablesToProcess() {
        return tablesToProcess;
    }

    public void setTablesToProcess(List<WordTable> tablesToProcess) {
        this.tablesToProcess = tablesToProcess;
    }

    public XWPFDocument getDocument() {
        return document;
    }

    public void setDocument(XWPFDocument document) {
        this.document = document;
    }

    public String getFilename() {
        return filename;
    }
}
