package sawalha.poi.word;

import java.io.IOException;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.profiler.Profiler;

import sawalha.poi.vo.WordTable;
import sawalha.poi.vo.WordText;

public class DOCXVerification {
    final static Logger log = LoggerFactory.getLogger(DOCXVerification.class);

    private DOCXProcessor parent;

    private int allTableCount = 0;
    private int tableCount = 0;
    private int textCount = 0;

    // private Logger log;

    private Profiler profiler = new Profiler("BASIC");

    public DOCXVerification(DOCXProcessor parent) {
        this.parent = parent;
        profiler.setLogger(DOCXProcessorCli.analysisLog);
    }

    public DOCXVerification(DOCXProcessor parent, String logFile) {
        this.parent = parent;
        // if (logFile == null) {
        // throw new RuntimeException("Report File location was not specified.");
        // } else {
        // log = CustomLog.getLooger(logFile);
        // }
        profiler.setLogger(DOCXProcessorCli.analysisLog);
    }

    private void processSingleTable(XWPFTable table, WordTable wordTable) {
        if (table.getRow(0).getCell(0).getText().contains(wordTable.getTableId())) {
            tableCount++;
        } else {
            for (XWPFTableRow row : table.getRows()) {
                for (XWPFTableCell cell : row.getTableCells()) {
                    if (cell.getTables() != null && cell.getTables().size() > 0) {
                        for (XWPFTable xwpfTable : cell.getTables()) {
                            ++allTableCount;
                            processSingleTable(xwpfTable, wordTable);
                        }
                    }
                }
            }
        }
    }

    public void startProcessing() throws InvalidFormatException, IOException {
        if (parent.getDocument() == null) {
            throw new RuntimeException(
                    "Input/Output Filename was not specified. Use setInputFile(String)/setOutputFile(String) methods.");
        }
        if (parent.getTextsToProcess() != null) {
            for (WordText wordText : parent.getTextsToProcess()) {
                processText(wordText.getTextId());
            }
        }
        if (parent.getTablesToProcess() != null) {
            for (WordTable wordTable : parent.getTablesToProcess()) {
                processTables(wordTable);
            }
        }
        log.info("Testing was finished.");
        profiler.stop().log();// print();
    }

    private void processText(String src) throws InvalidFormatException, IOException {
        profiler.start("Testing Variable [" + src + "]");
        log.info("Start Testing Variable [{}]", src);
        for (XWPFParagraph p : parent.getDocument().getParagraphs()) {
            if (p.getText() != null && p.getText().contains(src)) {
                // log.info("Variable {} was found in paragraph --> {}", src, p.getText());
                boolean runFound = false;
                List<XWPFRun> runs = p.getRuns();
                if (runs != null) {
                    for (XWPFRun r : runs) {
                        String text = r.text();
                        if (text != null && text.contains(src)) {
                            // log.info("Found Run--> {}", text);
                            runFound = true;
                            textCount++;
                        }
                    }
                }
                if (!runFound) {
                    log.warn("Please review the text format for variable ID [{}] in paragraph\n {} ", src, p.getText(),
                        p.getText());
                }
            }
        }
        for (XWPFTable tbl : parent.getDocument().getTables()) {
            processSingleTableText(tbl, src);
        }
        log.info("Total number of variables that can be processed for ID [{}] is: {} variable(s).", src, textCount);
        textCount = 0;
        // profiler.stop();
    }

    private void processSingleTableText(XWPFTable table, String src) {
        for (XWPFTableRow row : table.getRows()) {
            for (XWPFTableCell cell : row.getTableCells()) {
                if (cell.getTables() != null && cell.getTables().size() > 0) {
                    for (XWPFTable xwpfTable : cell.getTables()) {
                        processSingleTableText(xwpfTable, src);
                    }
                }

                for (XWPFParagraph p : cell.getParagraphs()) {
                    if (p.getText() != null && p.getText().contains(src)) {
                        // log.info("Variable {} was found in paragraph --> {}", src, p.getText());
                        List<XWPFRun> runs = p.getRuns();
                        boolean runFound = false;
                        if (runs != null) {
                            for (XWPFRun r : runs) {
                                String text = r.text();
                                if (text != null && text.contains(src)) {
                                    runFound = true;
                                    textCount++;
                                }
                            }
                        }
                        if (!runFound) {
                            log.warn("Please review the text format for variable ID [{}] in paragraph\n {} ", src,
                                p.getText(), p.getText());
                        }
                    }
                }
            }
        }
    }

    private void processTables(WordTable wordTable) {
        // log.info("Processing tableID:{}", wordTable.getTableId());
        profiler.start("Process Table [" + wordTable.getTableId() + "]");
        log.info("Start Testing Table [{}]", wordTable.getTableId());
        if (wordTable.getRows().size() < 1) {
            log.warn("tableID:{} does not have any defined rows. It will not be processed.", wordTable.getTableId());
            return;
        }

        for (XWPFTable tbl : parent.getDocument().getTables()) {
            processSingleTable(tbl, wordTable);
        }
        log.info("Total number of tables processed for tableID [{}] is: {} table(s) out of {} table(s).",
            wordTable.getTableId(), tableCount, allTableCount);
        allTableCount = 0;
        tableCount = 0;
        // profiler.stop();
    }

}
