package sawalha.poi.word;

import java.io.File;
import java.io.IOException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import ch.qos.logback.classic.Logger;

public class ValidateXmlFile {

    final static Logger log = (Logger) LoggerFactory.getLogger(ValidateXmlFile.class);

    public boolean isXmlFileOracleValid(File xmlFile) throws IOException {
        Source theXmlFile = new StreamSource(xmlFile);
        try {
            Source schemaFile = new StreamSource(
                    this.getClass().getClassLoader().getResourceAsStream("oracle-xml-schema.xsd"));
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = schemaFactory.newSchema(schemaFile);
            Validator validator = schema.newValidator();
            validator.validate(theXmlFile);
        } catch (SAXException e) {
            log.error("Error Parsing XML [{}]", theXmlFile.getSystemId(), e);
            return false;
        }
        log.debug(theXmlFile.getSystemId() + " is valid");
        return true;
    }

    public boolean isXmlFileApplicationValid(File xmlFile) throws IOException {
        Source theXmlFile = new StreamSource(xmlFile);
        try {
            Source schemaFile = new StreamSource(
                    this.getClass().getClassLoader().getResourceAsStream("app-xml-schema.xsd"));
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = schemaFactory.newSchema(schemaFile);
            Validator validator = schema.newValidator();
            validator.validate(theXmlFile);
        } catch (SAXException e) {
            log.error("Error Parsing XML [{}]", theXmlFile.getSystemId(), e);
            return false;
        }
        log.info(theXmlFile.getSystemId() + " is valid");
        return true;
    }

    public static void main(String[] args) {
        ValidateXmlFile t = new ValidateXmlFile();
        try {
            t.isXmlFileOracleValid(new File("d:\\delete\\tables.xml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
