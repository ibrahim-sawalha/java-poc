package sawalha.poi.word;

import java.io.File;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.xml.stream.XMLStreamException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;
import sawalha.poi.word.io.XmlFilenameFilter;

public class FilesParser {
    final static Logger log = (Logger) LoggerFactory.getLogger(FilesParser.class);

    public static void main(String[] args) throws XMLStreamException {
        System.out.println(getFiles(new File("d:/delete/table*.xml")).size());
    }

    public static List<File> getFiles(File file) {
        if (file.isFile()) {
            return Arrays.asList(file);
        } else if (file.isDirectory()) {
            // log.error("The specefied file [{}] is directory. Its should be file.", file.getAbsolutePath());
            throw new RuntimeException(file.getAbsolutePath() + " is directory. It should be file.");
        } else {
            IOFileFilter fileFilter = new XmlFilenameFilter(file.getName());
            try {
                LinkedList<File> fileList = (LinkedList<File>) FileUtils.listFiles(new File(file.getParent()),
                    fileFilter, null);
                // StaxParser sample = new StaxParser();
                return fileList;
                // WordParameters params = new WordParameters();
                // for (File f : fileList) {
                // System.out.println(f.getAbsolutePath());
                // params.addTable(sample.processTable(f));
                // params.setText(sample.processVariables(f));
                // }
            } catch (java.lang.IllegalArgumentException e) {
                // log.error("The directory [{}] is not valid.", file.getParent());
                throw new RuntimeException(file.getParent() + " is not valid.");
            }
        }

    }

}
