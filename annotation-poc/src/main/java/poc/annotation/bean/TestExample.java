package poc.annotation.bean;

import poc.annotation.Test;
import poc.annotation.TesterInfo;
import poc.annotation.TesterInfo.Priority;

@TesterInfo(priority = Priority.HIGH, createdBy = "mkyong.com", tags = {
		"sales", "test" })
public class TestExample {

	private String name;

	public TestExample(String name) {
		this.name = name;
	}

	@Test(name = "testA")
	void testA() {
		if (true) {
			throw new RuntimeException("This test always failed");
		}
	}

	@Test(enabled = false, name = "testB")
	void testB() {
		if (false) {
			throw new RuntimeException("This test always passed");
		}
	}

	@Test(enabled = true, name = "testC")
	void testC() {
		if (10 > 1) {
			// do nothing, this test always passed.
		}
	}

	@Test(enabled = true, name = "testD")
	void testD() {
		System.out.println("Entered name is: " + name);
	}

}
