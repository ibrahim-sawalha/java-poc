package singleton;

public class ClassicSingleton {

    private static ClassicSingleton instance = null;

    // private static ClassicSingleton instance = new ClassicSingleton();

    private ClassicSingleton() {
    }

    public static ClassicSingleton getInstance() {
        if (instance == null) {
            instance = new ClassicSingleton();
        }
        return instance;
    }

    public String getTestMsg() {
        return "test";
    }

    public int calculate(int x, int y) {
        return x + y;
    }

}
