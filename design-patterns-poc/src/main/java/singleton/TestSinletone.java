package singleton;

public class TestSinletone {

    private String test = "";

    public static void main(String[] args) {
        ClassicSingleton.getInstance().getTestMsg();
        ClassicSingleton.getInstance().calculate(145, 10);
        System.out.println(new TestSinletone().getTest());
    }

    public String getTest() {
        if (test == null) {
            test = "";
        }
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }

}
