package factory;

public class Rectangle implements Shape {

	@Override
	public void draw() {
		System.out.println("Inside Rectangle::draw() method.");
	}

	@Override
	public void init() {
		System.out.println("Inside Rectangle::init() method.");
	}

}
