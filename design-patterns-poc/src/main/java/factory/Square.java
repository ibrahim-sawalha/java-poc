package factory;

public class Square implements Shape {

	public void draw() {
		// draw("Inside Square::draw() method.");
		draw(10, 20, 30, "red");
	}

	public void draw(String color) {
		draw(10, 20, 30, null);
	}

	public void draw(int x, int y) {
		draw(x, y, 0, null);
	}

	public void draw(int x, int y, int z) {
		draw(x, y, z, null);
	}

	public void draw(int x, int y, int z, String color) {
		System.out
				.println("x:" + x + " y:" + y + " z:" + z + " color:" + color);
	}

	public void init() {
		System.out.println("Inside Square::init() method.");
	}

}
