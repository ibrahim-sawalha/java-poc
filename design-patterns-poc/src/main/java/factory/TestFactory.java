package factory;

public class TestFactory {

    public static void main(String[] args) {
        ShapeFactory f = ShapeFactory.getInstance();
        f.getShape("circle").draw();

        ShapeFactory f1 = ShapeFactory.getInstance();
        f1.getShape("circle").draw();

        ShapeFactory f2 = ShapeFactory.getInstance();
        f2.getShape("SQUARE").draw();

        ShapeFactory f3 = ShapeFactory.getInstance();
        f3.getShape("SQUARE").draw();
    }
}
