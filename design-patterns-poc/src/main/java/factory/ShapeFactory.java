package factory;

public class ShapeFactory {

    private static ShapeFactory instance = null;

    private ShapeFactory() {
    }

    public static ShapeFactory getInstance() {
        if (instance == null) {
            instance = new ShapeFactory();
        }
        return instance;
    }

    public Shape getShape(String shapeType) {
        if (shapeType == null) {
            return null;
        }
        if (shapeType.equalsIgnoreCase("circle")) {
            return new Rectangle();
        } else if (shapeType.equalsIgnoreCase("SQUARE")) {
            Square s = new Square();
            s.init();
            return s;
        }
        return null;
    }

    public Shape getCircle() {
        Rectangle r = new Rectangle();
        r.init();
        return r;
    }
}
