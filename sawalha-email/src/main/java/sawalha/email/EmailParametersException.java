package sawalha.email;

public class EmailParametersException extends Exception {
    private static final long serialVersionUID = 8400603191453267553L;

    public EmailParametersException(String exception) {
        super(exception);
    }

}
