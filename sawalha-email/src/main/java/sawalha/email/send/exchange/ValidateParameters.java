package sawalha.email.send.exchange;

import java.util.Date;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;

public class ValidateParameters {

    static Parameters validateParameters(CommandLine cmd) throws Exception {
        Parameters p = new Parameters();
        Object o = null;
        o = getParsedOption(cmd, SendEmailApp.OPT_FUNCTION);
        if (o != null) {
            String f = (String) o;
            if (f.equalsIgnoreCase(SendEmailApp.F_SEND_EMAIL) || f.equalsIgnoreCase(SendEmailApp.F_SEND_APPOINTMENT)) {
                p.setFunction(f);
            } else {
                throw new Exception("function must be specified by E or A");
            }
        }
        o = getParsedOption(cmd, SendEmailApp.OPT_USER_NAME);
        if (o != null) {
            p.setUserName((String) o);
        }
        o = getParsedOption(cmd, SendEmailApp.OPT_PASSWORD);
        if (o != null) {
            p.setPassword((String) o);
        }
        o = getParsedOption(cmd, SendEmailApp.OPT_SMTP_HOST);
        if (o != null) {
            p.setSmtpHost((String) o);
        }
        o = getParsedOption(cmd, SendEmailApp.OPT_SMTP_PORT);
        if (o != null) {
            String port = (String) o;
            p.setSmtpPort(port);
        }
        o = getParsedOption(cmd, SendEmailApp.OPT_SENDER_EMAIL);
        if (o != null) {
            p.setSenderEmail((String) o);
        }
        o = getParsedOption(cmd, SendEmailApp.OPT_RECIPIENTS_TO);
        if (o != null) {
            p.setRecipientsTO((String) o);
        }
        o = getParsedOption(cmd, SendEmailApp.OPT_RECIPIENTS_CC);
        if (o != null) {
            p.setRecipientsCC((String) o);
        }
        o = getParsedOption(cmd, SendEmailApp.OPT_RECIPIENTS_BCC);
        if (o != null) {
            p.setRecipientsBCC((String) o);
        }
        o = getParsedOption(cmd, SendEmailApp.OPT_EMAIL_SUBJECT);
        if (o != null) {
            p.setEmailSubject((String) o);
        }
        o = getParsedOption(cmd, SendEmailApp.OPT_EMAIL_CONTENT);
        if (o != null) {
            p.setEmailContent((String) o);
        }
        o = getParsedOption(cmd, SendEmailApp.OPT_ATTACHED_FILES);
        if (o != null) {
            p.setAttachedFiles((String) o);
        }
        o = getParsedOption(cmd, SendEmailApp.OPT_MEETING_SUMMARY);
        if (o != null) {
            p.setMeetingSummary((String) o);
        }
        o = getParsedOption(cmd, SendEmailApp.OPT_MEETING_LOCATION);
        if (o != null) {
            p.setMeetingLocation((String) o);
        }
        o = getParsedOption(cmd, SendEmailApp.OPT_MEETING_DESCRIPTION);
        if (o != null) {
            p.setMeetingDescription((String) o);
        }
        o = getParsedOption(cmd, SendEmailApp.OPT_MEETING_FROM);
        if (o != null) {
            try {
                Date x = SendEmailApp.appDateFormat.parse((String) o);
                p.setMeetingFrom(x);
            } catch (java.text.ParseException e) {
                throw new Exception("Error in <" + SendEmailApp.OPT_MEETING_FROM
                    + "> Date format, date should be dd-MM-yyyy,HH:mm");
            }
        }
        o = getParsedOption(cmd, SendEmailApp.OPT_MEETING_TO);
        if (o != null) {
            try {
                Date x = SendEmailApp.appDateFormat.parse((String) o);
                p.setMeetingTo(x);
            } catch (java.text.ParseException e) {
                throw new Exception("Error in <" + SendEmailApp.OPT_MEETING_FROM
                    + "> Date format, date should be dd-MM-yyyy,HH:mm");
            }
        }
        if (p.getFunction() == null) {
            throw new Exception("Function type must be specified <-F E or -F A>");
        }

        return p;
    }

    static Object getParsedOption(CommandLine cmd, String option) throws ParseException {
        if (cmd.hasOption(option)) {
            Object o = cmd.getParsedOptionValue(option);
            // System.out.println("Option " + option + " parsed as " + o + " which is instance of "
            // + o.getClass().getCanonicalName());
            return o;
        }
        return null;
    }
}
