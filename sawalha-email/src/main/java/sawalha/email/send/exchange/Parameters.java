package sawalha.email.send.exchange;

import java.util.Date;

public class Parameters {

    private String function;
    private String userName;
    private String password;
    private String smtpHost;
    private String smtpPort;
    private String senderEmail;
    private String recipientsTO;
    private String recipientsCC;
    private String recipientsBCC;
    private String emailSubject;
    private String emailContent;
    private String meetingSummary;
    private String meetingLocation;
    private String meetingDescription;
    private Date meetingFrom;
    private Date meetingTo;
    private String attachedFiles;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSmtpHost() {
        return smtpHost;
    }

    public void setSmtpHost(String smtpHost) {
        this.smtpHost = smtpHost;
    }

    public String getSmtpPort() {
        return smtpPort;
    }

    public void setSmtpPort(String smtpPort) {
        this.smtpPort = smtpPort;
    }

    public String getSenderEmail() {
        return senderEmail;
    }

    public void setSenderEmail(String senderEmail) {
        this.senderEmail = senderEmail;
    }

    public String getRecipientsTO() {
        return recipientsTO;
    }

    public void setRecipientsTO(String recipientsTO) {
        this.recipientsTO = recipientsTO;
    }

    public String getRecipientsCC() {
        return recipientsCC;
    }

    public void setRecipientsCC(String recipientsCC) {
        this.recipientsCC = recipientsCC;
    }

    public String getRecipientsBCC() {
        return recipientsBCC;
    }

    public void setRecipientsBCC(String recipientsBCC) {
        this.recipientsBCC = recipientsBCC;
    }

    public String getEmailSubject() {
        return emailSubject;
    }

    public void setEmailSubject(String emailSubject) {
        this.emailSubject = emailSubject;
    }

    public String getEmailContent() {
        return emailContent;
    }

    public void setEmailContent(String emailContent) {
        this.emailContent = emailContent;
    }

    public String getMeetingSummary() {
        return meetingSummary;
    }

    public void setMeetingSummary(String meetingSummary) {
        this.meetingSummary = meetingSummary;
    }

    public String getMeetingLocation() {
        return meetingLocation;
    }

    public void setMeetingLocation(String meetingLocation) {
        this.meetingLocation = meetingLocation;
    }

    public String getMeetingDescription() {
        return meetingDescription;
    }

    public void setMeetingDescription(String meetingDescription) {
        this.meetingDescription = meetingDescription;
    }

    public Date getMeetingFrom() {
        return meetingFrom;
    }

    public void setMeetingFrom(Date meetingFrom) {
        this.meetingFrom = meetingFrom;
    }

    public Date getMeetingTo() {
        return meetingTo;
    }

    public void setMeetingTo(Date meetingTo) {
        this.meetingTo = meetingTo;
    }

    public String getAttachedFiles() {
        return attachedFiles;
    }

    public void setAttachedFiles(String attachedFiles) {
        this.attachedFiles = attachedFiles;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Parameters [function=");
        builder.append(function);
        builder.append(", userName=");
        builder.append(userName);
        builder.append(", password=xxx");
        builder.append(", smtpHost=");
        builder.append(smtpHost);
        builder.append(", smtpPort=");
        builder.append(smtpPort);
        builder.append(", senderEmail=");
        builder.append(senderEmail);
        builder.append(", recipientsTO=");
        builder.append(recipientsTO);
        builder.append(", recipientsCC=");
        builder.append(recipientsCC);
        builder.append(", recipientsBCC=");
        builder.append(recipientsBCC);
        builder.append(", emailSubject=");
        builder.append(emailSubject);
        builder.append(", emailContent=");
        builder.append(emailContent);
        builder.append(", meetingSummary=");
        builder.append(meetingSummary);
        builder.append(", meetingLocation=");
        builder.append(meetingLocation);
        builder.append(", meetingDescription=");
        builder.append(meetingDescription);
        builder.append(", meetingFrom=");
        builder.append(meetingFrom);
        builder.append(", meetingTo=");
        builder.append(meetingTo);
        builder.append(", attachedFiles=");
        builder.append(attachedFiles);
        builder.append("]");
        return builder.toString();
    }

}
