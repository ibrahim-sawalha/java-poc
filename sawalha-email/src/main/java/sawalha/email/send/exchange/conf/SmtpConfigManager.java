package sawalha.email.send.exchange.conf;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import sawalha.email.EmailMessage;
import sawalha.email.EmailParametersException;
import sawalha.email.send.exchange.SendEmailExchange;

public class SmtpConfigManager {

    public static void main(String[] args) {
        File file = new File("smtp-config.xml");
        SmtpConfig smtpConfig = SmtpConfigManager.readSmtpConfigFile(file);
        EmailMessage email = new EmailMessage();
        email.setSmtpConfig(smtpConfig);

        email.setRecipientsTO("ibrahim.sawalha@icsfs.com");
        // email.setRecipientsCC("ibrahim.sawalha@icsfs.com");
        // email.setRecipientsBCC("ibrahim.sawalha@icsfs.com");
        email.setEmailSubject("Testing");
        email.setEmailText("Dear,</br> <p><B>Ibrahim Sawalha</B></p> <h1>is Testing</h1><hr><p>come back later</p>");
        try {
            System.out.println(SendEmailExchange.sendStandardEmail(email));
        } catch (EmailParametersException e) {
            e.printStackTrace();
        }
        System.out.println(SmtpConfigManager.readSmtpConfigFile(file));

        // SmtpConfigManager.createDefaultSmtpConfigFile(file);
        // System.out.println(SmtpConfigManager.readSmtpConfigFile(file));
    }

    public static void createDefaultSmtpConfigFile(File file) {
        try {
            SmtpConfig conf = new SmtpConfig();
            conf.setUserId("smtp-userName");
            conf.setPassword("smtp-Password");
            conf.setSmtpHost("smtp-host-ip");
            conf.setSmtpPort("smtp-port-default-25");

            JAXBContext jaxbContext = JAXBContext.newInstance(SmtpConfig.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(conf, file);
            jaxbMarshaller.marshal(conf, System.out);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public static void createSmtpConfigFile(SmtpConfig conf, File file) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(SmtpConfig.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(conf, file);
            jaxbMarshaller.marshal(conf, System.out);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public static SmtpConfig readSmtpConfigFile(File file) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(SmtpConfig.class);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            return (SmtpConfig) jaxbUnmarshaller.unmarshal(file);
        } catch (JAXBException e) {
            e.printStackTrace();
            return null;
        }
    }

}
