package sawalha.email.send.exchange.conf;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SmtpConfig {

    private String userId = "userID";
    private String password = "userPassword";
    private String smtpHost = "SMTP-server-ip";
    private String smtpPort = "25";
    private String senderEmail = "sender-email-address";
    private String RecipientsTO;
    // String RecipientsCC;
    private String emailSubject = "email-subject";
    private String emailText = "email-body";

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSmtpHost() {
        return smtpHost;
    }

    public void setSmtpHost(String smtpHost) {
        this.smtpHost = smtpHost;
    }

    public String getSmtpPort() {
        return smtpPort;
    }

    public void setSmtpPort(String smtpPort) {
        this.smtpPort = smtpPort;
    }

    public String getSenderEmail() {
        return senderEmail;
    }

    public void setSenderEmail(String senderEmail) {
        this.senderEmail = senderEmail;
    }

    public String getRecipientsTO() {
        return RecipientsTO;
    }

    public void setRecipientsTO(String recipientsTO) {
        RecipientsTO = recipientsTO;
    }

    // public String getRecipientsCC() {
    // return RecipientsCC;
    // }

    // public void setRecipientsCC(String recipientsCC) {
    // RecipientsCC = recipientsCC;
    // }

    public String getEmailSubject() {
        return emailSubject;
    }

    public void setEmailSubject(String emailSubject) {
        this.emailSubject = emailSubject;
    }

    public String getEmailText() {
        return emailText;
    }

    public void setEmailText(String emailText) {
        this.emailText = emailText;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("SmtpConfig [userId=");
        builder.append(userId);
        builder.append(", password=");
        builder.append(password);
        builder.append(", smtpHost=");
        builder.append(smtpHost);
        builder.append(", smtpPort=");
        builder.append(smtpPort);
        builder.append(", senderEmail=");
        builder.append(senderEmail);
        builder.append(", RecipientsTO=");
        builder.append(RecipientsTO);
        // builder.append(", RecipientsCC=");
        // builder.append(RecipientsCC);
        builder.append(", emailSubject=");
        builder.append(emailSubject);
        builder.append(", emailText=");
        builder.append(emailText);
        builder.append("]");
        return builder.toString();
    }

}
