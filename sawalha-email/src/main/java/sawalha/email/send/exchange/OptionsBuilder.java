package sawalha.email.send.exchange;

import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;

public class OptionsBuilder {

    @SuppressWarnings("static-access")
    static Options buildOptions() {
        Options o = new Options();
        o.addOption(OptionBuilder.withDescription("Help").create(SendEmailApp.OPT_HELP));
        o.addOption(OptionBuilder.withDescription("Debug mode").create(SendEmailApp.OPT_DEBUG));

        o.addOption(OptionBuilder.hasArg().withArgName("E or A").withType(String.class)
            .withDescription("E:Email , A:Appointment").create(SendEmailApp.OPT_FUNCTION));

        o.addOption(OptionBuilder.hasArg().withArgName("String value").withType(String.class)
            .withDescription("Mandatory - Specify SMTP user name").create(SendEmailApp.OPT_USER_NAME));
        o.addOption(OptionBuilder.hasArg().withArgName("String value").withType(String.class)
            .withDescription("Mandatory - Specify SMTP password").create(SendEmailApp.OPT_PASSWORD));
        o.addOption(OptionBuilder.hasArg().withArgName("String value").withType(String.class)
            .withDescription("Mandatory - Specify SMTP Host address/IP").create(SendEmailApp.OPT_SMTP_HOST));

        o.addOption(OptionBuilder.hasArg().withArgName("Integer value").withType(String.class)
            .withDescription("Mandatory - Specify SMTP port").create(SendEmailApp.OPT_SMTP_PORT));

        o.addOption(OptionBuilder.hasArg().withArgName("String value").withType(String.class)
            .withDescription("Mandatory - Specify Sender's email address").create(SendEmailApp.OPT_SENDER_EMAIL));
        o.addOption(OptionBuilder.hasArg().withArgName("String with , separators").withType(String.class)
            .withDescription("Mandatory - Specify the Recipients email addresses")
                .create(SendEmailApp.OPT_RECIPIENTS_TO));
        o.addOption(OptionBuilder.hasArg().withArgName("String with , separators").withType(String.class)
            .withDescription("Specify the CC Recipients email addresses").create(SendEmailApp.OPT_RECIPIENTS_CC));
        o.addOption(OptionBuilder.hasArg().withArgName("String with , separators").withType(String.class)
            .withDescription("Specify the BCC Recipients email addresses").create(SendEmailApp.OPT_RECIPIENTS_BCC));
        o.addOption(OptionBuilder.hasArg().withArgName("String value").withType(String.class)
            .withDescription("Mandatory - Specify the Email subject").create(SendEmailApp.OPT_EMAIL_SUBJECT));
        o.addOption(OptionBuilder.hasArg().withArgName("String value").withType(String.class)
            .withDescription("Mandatory - Specify the Email body content").create(SendEmailApp.OPT_EMAIL_CONTENT));
        o.addOption(OptionBuilder.hasArg().withArgName("String with ; separators").withType(String.class)
            .withDescription("Optional - Specify the Attached files if any")
                .create(SendEmailApp.OPT_ATTACHED_FILES));

        o.addOption(OptionBuilder.hasArg().withArgName("String value").withType(String.class)
            .withDescription("Mandatory (for function a)- Specify the Meeting Summary")
                .create(SendEmailApp.OPT_MEETING_SUMMARY));
        o.addOption(OptionBuilder.hasArg().withArgName("String value").withType(String.class)
            .withDescription("Mandatory (for function a)-Specify the Meeting Location")
            .create(SendEmailApp.OPT_MEETING_LOCATION));
        o.addOption(OptionBuilder.hasArg().withArgName("String value").withType(String.class)
            .withDescription("Mandatory (for function a)-Specify the Meeting Description")
            .create(SendEmailApp.OPT_MEETING_DESCRIPTION));

        o.addOption(OptionBuilder.hasArg().withArgName("String value").withType(String.class)
            .withDescription("Mandatory (for function a)-Specify the Meeting Starting Date and Time")
            .create(SendEmailApp.OPT_MEETING_FROM));
        o.addOption(OptionBuilder.hasArg().withArgName("String value").withType(String.class)
            .withDescription("Mandatory (for function a)-Specify the Meeting Ending Date and Time")
            .create(SendEmailApp.OPT_MEETING_TO));

        o.addOption(OptionBuilder.hasArg().withArgName("String with ; separators").withType(String.class)
            .withDescription("Specify the Attached files if any").create(SendEmailApp.OPT_ATTACHED_FILES));
        return o;
    }

}
