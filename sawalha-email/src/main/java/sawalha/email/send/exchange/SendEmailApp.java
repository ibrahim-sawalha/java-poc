package sawalha.email.send.exchange;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PosixParser;

import sawalha.email.EmailParametersException;
import sawalha.email.send.exchange.conf.SmtpConfig;

public class SendEmailApp {
    private static String version = "1.0.2";
    static SimpleDateFormat appDateFormat = new SimpleDateFormat("dd-MM-yyyy,HH:mm");

    private static boolean debugMode = false;

    static final String OPT_HELP = "h";
    static final String OPT_DEBUG = "debug";
    static final String F_SEND_EMAIL = "E";
    static final String F_SEND_APPOINTMENT = "A";
    static final String OPT_FUNCTION = "F";
    static final String OPT_USER_NAME = "u";
    static final String OPT_PASSWORD = "p";
    static final String OPT_SMTP_HOST = "smtp_host";
    static final String OPT_SMTP_PORT = "smtp_port";
    static final String OPT_SENDER_EMAIL = "from";
    static final String OPT_RECIPIENTS_TO = "to";
    static final String OPT_RECIPIENTS_CC = "cc";
    static final String OPT_RECIPIENTS_BCC = "bcc";
    static final String OPT_EMAIL_SUBJECT = "subject";
    static final String OPT_EMAIL_CONTENT = "content";
    static final String OPT_ATTACHED_FILES = "files";

    static final String OPT_MEETING_SUMMARY = "m_summary";
    static final String OPT_MEETING_LOCATION = "m_loc";
    static final String OPT_MEETING_DESCRIPTION = "m_desc";
    static final String OPT_MEETING_FROM = "m_from";
    static final String OPT_MEETING_TO = "m_to";

    @SuppressWarnings("resource")
    public static void main(String[] args) {
        ProgressBarTraditional pb1 = new ProgressBarTraditional();
        System.out.println("ICS Email Sender, version \"" + version + "\"");
        System.out.println("Copyright (C) 2014 by ICS Financial Systems Ltd.");
        /* Parse command line parameters first */
        CommandLineParser parser = new PosixParser();
        HelpFormatter help = new HelpFormatter();
        CommandLine cmd = null;
        Options options = OptionsBuilder.buildOptions();
        Parameters p = null;
        try {
            cmd = parser.parse(options, args);
            if (cmd.hasOption(OPT_DEBUG)) {
                debugMode = true;
            }
            if (cmd.hasOption(OPT_HELP)) {
                help.printHelp("ics-email", options, true);
                System.out.println();
                System.out.println("Examples:");
                System.out.println("-F a -u ibrahim.sawalha -p password -smtp_host 100.100.001.004 -smtp_port 25 "
                        + "-m_from 01-01-2015,01:20 -from ibrahim.sawalha@icsfs.com -to ibrahim.sawalha@icsfs.com "
                        + "-subject \"testing email ICL\" -content \"this is a testing<br><br>have a nice day\" "
                        + "-files \"c:\\test-page.pdf;c:\\test-page-s.pdf\" -m_summary \"meeting summary\" "
                        + "-m_loc \"meeting location\" -m_desc \"meeting description\" -m_from 05-05-2015,13:30 -m_to 05-05-2015,15:30");
                System.out.println();
                System.out.println("-F e -u ibrahim.sawalha -p password -smtp_host 100.100.001.004 -smtp_port 25 "
                        + "-m_from 01-01-2015,01:20 -from ibrahim.sawalha@icsfs.com -to ibrahim.sawalha@icsfs.com "
                        + "-subject \"testing email ICL\" -content \"this is a testing<br><br>have a nice day\" "
                        + "-files \"c:\\test-page.pdf;c:\\test-page-s.pdf\"");
                System.exit(1);
            } else {
                p = ValidateParameters.validateParameters(cmd);
            }
        } catch (Exception e) {
            log(e);
            System.err.println("Wrong parameters:" + e.getMessage());
            help.printHelp("ics-email", options);
            System.exit(1);
        }
        // p is definitely not null we got here
        System.out.println("Parameters parsed: " + p);
        System.out.println();
        System.out.println("Start sending emails.");
        pb1.start();
        if (p.getFunction().equalsIgnoreCase("e")) {

            SmtpConfig smtpConfig = new SmtpConfig();
            smtpConfig.setUserId(p.getUserName());
            smtpConfig.setPassword(p.getPassword());
            smtpConfig.setSmtpHost(p.getSmtpHost());
            smtpConfig.setSmtpPort(p.getSmtpPort());
            smtpConfig.setSenderEmail(p.getSenderEmail());

            try {
                System.out.println(
                    SendEmailExchange.sendStandardEmail(smtpConfig, p.getRecipientsTO(), p.getRecipientsCC(),
                        p.getRecipientsBCC(), p.getEmailSubject(), p.getEmailContent(), p.getAttachedFiles()));
            } catch (EmailParametersException e) {
                e.printStackTrace();
            }

        } else if (p.getFunction().equalsIgnoreCase("a")) {
            try {
                System.out.println(SendEmailExchange.sendAppointmentEmail(p.getUserName(), p.getPassword(),
                    p.getSmtpHost(), p.getSmtpPort(), p.getSenderEmail(), p.getRecipientsTO(), p.getRecipientsCC(),
                    p.getRecipientsBCC(), p.getEmailSubject(), p.getEmailContent(), p.getMeetingSummary(),
                    p.getMeetingLocation(), p.getMeetingDescription(), appDateFormat.format(p.getMeetingFrom()),
                    appDateFormat.format(p.getMeetingTo()), p.getAttachedFiles()));
            } catch (EmailParametersException e) {
                e.printStackTrace();
            }
        }
        pb1.showProgress = false;
        System.out.println("\nFinished...");
    }

    public static void log(Throwable e) {
        if (debugMode) {
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            System.err.println("EXCEPTION " + errors.toString());
        } else {
            System.err.println("EXCEPTION " + e.getMessage());
        }
    }

}

class ProgressBarRotating extends Thread {
    boolean showProgress = true;

    @Override
    public void run() {
        String anim = "|/-\\";
        int x = 0;
        while (showProgress) {
            System.out.print("\r Processing " + anim.charAt(x++ % anim.length()));
            try {
                Thread.sleep(100);
            } catch (Exception e) {
            }
            ;
        }
    }
}

class ProgressBarTraditional extends Thread {
    boolean showProgress = true;

    @Override
    public void run() {
        String anim = "=====================";
        int x = 0;
        while (showProgress) {
            System.out.print("\r Processing " + anim.substring(0, x++ % anim.length()) + " ");
            try {
                Thread.sleep(100);
            } catch (Exception e) {
            }
            ;
        }
    }
}
