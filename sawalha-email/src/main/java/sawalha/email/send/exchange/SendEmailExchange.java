package sawalha.email.send.exchange;

import java.io.File;
import java.io.FileInputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import sawalha.email.EmailMessage;
import sawalha.email.EmailParametersException;
import sawalha.email.send.exchange.conf.SmtpConfig;

public class SendEmailExchange {
    private static SimpleDateFormat iCalendarDateFormat = new SimpleDateFormat("yyyyMMdd'T'HHmm'00'");
    private static SimpleDateFormat appDateFormat = new SimpleDateFormat("dd-MM-yyyy,HH:mm");

    public static String sendAppointmentEmail(final String userId, final String password, String smtpHost,
            String smtpPort, String senderEmail, String RecipientsTO, String RecipientsCC, String RecipientsBCC,
            String emailSubject, String emailText, String meetingSummary, String meetingLocation,
            String meetingDescription, String meetingFromDate, String meetingToDate, String attachedFiles) //
            throws EmailParametersException {
        String test = isAppointementParametersValid(userId, password, smtpHost, smtpPort, senderEmail, RecipientsTO,
            RecipientsCC, RecipientsBCC, emailSubject, emailText, meetingSummary, meetingLocation, meetingDescription,
            meetingFromDate, meetingToDate, attachedFiles);
        if (!test.equals("ok")) {
            return test;
        }
        return sendEmail(userId, password, smtpHost, smtpPort, senderEmail, RecipientsTO, RecipientsCC, RecipientsBCC,
            emailSubject, emailText, attachedFiles, meetingSummary, meetingLocation, meetingDescription,
            meetingFromDate, meetingToDate);
    }

    public static String sendStandardEmail( //
            SmtpConfig smtpConfig, //
            String RecipientsTO, //
            String RecipientsCC, //
            String RecipientsBCC, //
            String emailSubject,//
            String emailText, //
            String attachedFiles) //
            throws EmailParametersException {
        String test = isEmailParametersValid(smtpConfig.getUserId(), smtpConfig.getPassword(), smtpConfig.getSmtpHost(),
            smtpConfig.getSmtpPort(), smtpConfig.getSenderEmail(), RecipientsTO, RecipientsCC, RecipientsBCC,
            emailSubject, emailText);
        if (!test.equals("ok")) {

            return test;
        }
        return sendEmail(smtpConfig.getUserId(), smtpConfig.getPassword(), smtpConfig.getSmtpHost(),
            smtpConfig.getSmtpPort(), smtpConfig.getSenderEmail(), RecipientsTO, RecipientsCC, RecipientsBCC,
            emailSubject, emailText, attachedFiles, null, null, null, null, null);
    }

    public static String sendStandardEmail(EmailMessage emailMessage) //
            throws EmailParametersException {
        return sendStandardEmail(emailMessage.getSmtpConfig(), //
            emailMessage.getRecipientsTO(), //
            emailMessage.getRecipientsCC(), //
            emailMessage.getRecipientsBCC(), //
            emailMessage.getEmailSubject(), //
            emailMessage.getEmailText(), //
            emailMessage.getAttachedFiles());
    }

    private static String sendEmail(final String userId, final String password, String smtpHost, String smtpPort,
            String senderEmail, String RecipientsTO, String RecipientsCC, String RecipientsBCC, String emailSubject,
            String emailText, String attachedFiles, String meetingSummary, String meetingLocation,
            String meetingDescription, String meetingFromDate, String meetingToDate) {

        Properties props = new Properties();
        Date meetingStartDate = null;
        Date meetingEndDate = null;
        props.put("mail.smtp.host", smtpHost);
        props.put("mail.smtp.socketFactory.port", smtpPort);
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", smtpPort);

        try {
            Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(userId, password);
                }
            });

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(senderEmail));
            if (RecipientsTO != null) {
                message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(RecipientsTO, false));
            }
            if (RecipientsCC != null) {
                message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(RecipientsCC, false));
            }
            if (RecipientsBCC != null) {
                message.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(RecipientsBCC, false));
            }
            message.setSubject(emailSubject);

            // Create a Multipart
            Multipart multipart = new MimeMultipart();

            // Add part one
            multipart.addBodyPart(buildHtmlTextPart(emailText));

            if (meetingFromDate != null || meetingToDate != null) {
                // Add part two
                meetingStartDate = appDateFormat.parse(meetingFromDate);
                meetingEndDate = appDateFormat.parse(meetingToDate);
                multipart.addBodyPart(buildCalendarPart(senderEmail, meetingSummary, meetingLocation,
                    meetingDescription, meetingStartDate, meetingEndDate));
            }
            if (attachedFiles != null) {
                // review https://cloud.google.com/appengine/docs/java/mail/usingjavamail
                StringTokenizer strToken = new StringTokenizer(attachedFiles, ";");
                while (strToken.hasMoreElements()) {
                    String fileName = (String) strToken.nextElement();
                    File file = new File(fileName);
                    if (file.exists()) {
                        byte[] bFile = new byte[(int) file.length()];

                        // convert file into array of bytes
                        FileInputStream fileInputStream = new FileInputStream(file);
                        fileInputStream.read(bFile);
                        fileInputStream.close();
                        MimeBodyPart attachment = new MimeBodyPart();
                        attachment.setFileName(file.getName());
                        attachment.setContent(bFile, "application/octet-stream");
                        multipart.addBodyPart(attachment);
                    }
                }
            }
            // Put parts in message
            message.setContent(multipart);

            Transport.send(message);

            return "Done";
        } catch (Exception e) {
            return exception(e);
        }
    }

    private static BodyPart buildHtmlTextPart(String message) throws MessagingException {
        MimeBodyPart descriptionPart = new MimeBodyPart();
        StringBuffer content = new StringBuffer();
        content.append("<font size=\"\"2\"\" face=\"verdana\" >");
        content.append(message);
        content.append("</font>");
        descriptionPart.setContent(content.toString(), "text/html; charset=utf-8");

        return descriptionPart;
    }

    private static BodyPart buildCalendarPart(String senderEmail, String meetingSummary, String meetingLocation,
            String meetingDescription, Date start, Date end) throws Exception {
        BodyPart calendarPart = new MimeBodyPart();

        StringBuffer calendarContent = new StringBuffer();
        calendarContent.append("BEGIN:VCALENDAR\n" + "METHOD:REQUEST\n" + "PRODID: BCP - Meeting\n" + "VERSION:2.0\n");
        calendarContent.append("BEGIN:VEVENT\n" + "DTSTAMP:");
        calendarContent.append(iCalendarDateFormat.format(start));
        calendarContent.append("\n");
        calendarContent.append("DTSTART:");
        calendarContent.append(iCalendarDateFormat.format(start));
        calendarContent.append("\n");
        calendarContent.append("DTEND:");
        calendarContent.append(iCalendarDateFormat.format(end));
        calendarContent.append("\n");
        calendarContent.append("SUMMARY:");
        calendarContent.append(meetingSummary);
        calendarContent.append("\n");
        calendarContent.append("UID:324\n");
        calendarContent
                .append("ATTENDEE;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;RSVP=TRUE:MAILTO:" + senderEmail + "\n");
        calendarContent.append("ORGANIZER:MAILTO:");
        calendarContent.append(senderEmail);
        calendarContent.append("\n");

        calendarContent.append("LOCATION:");
        calendarContent.append(meetingLocation);
        calendarContent.append("\n");

        calendarContent.append("DESCRIPTION:");
        calendarContent.append(meetingDescription);
        calendarContent.append("\n");

        calendarContent.append("SEQUENCE:0\n");
        calendarContent.append("PRIORITY:5\n");
        calendarContent.append("CLASS:PUBLIC\n");
        calendarContent.append("STATUS:CONFIRMED\n");
        calendarContent.append("TRANSP:OPAQUE\n");
        calendarContent.append("BEGIN:VALARM\n");
        calendarContent.append("ACTION:DISPLAY\n");
        calendarContent.append("DESCRIPTION:REMINDER\n");
        calendarContent
                .append("TRIGGER;RELATED=START:-PT00H15M00S\n" + "END:VALARM\n" + "END:VEVENT\n" + "END:VCALENDAR");

        calendarPart.addHeader("Content-Class", "urn:content-classes:calendarmessage");
        calendarPart.setContent(calendarContent.toString(), "text/calendar;method=CANCEL");

        return calendarPart;
    }

    private static String isAppointementParametersValid(String userId, String password, String smtpHost,
            String smtpPort, String senderEmail, String RecipientsTO, String RecipientsCC, String RecipientsBCC,
            String emailSubject, String emailText, String meetingSummary, String meetingLocation,
            String meetingDescription, String meetingFromDate, String meetingToDate, String attachedFiles) //
            throws EmailParametersException {
        String test = isEmailParametersValid(userId, password, smtpHost, smtpPort, senderEmail, RecipientsTO,
            RecipientsCC, RecipientsBCC, emailSubject, emailText);
        if (!test.equals("ok")) {
            return test;
        }
        if (meetingSummary == null || meetingSummary.isEmpty()) {
            return "ERROR: meetingSummary is not valid <" + meetingSummary + ">";
        } else if (meetingLocation == null || meetingLocation.isEmpty()) {
            return "ERROR: meetingLocation is not valid <" + meetingLocation + ">";
        } else if (meetingDescription == null || meetingDescription.isEmpty()) {
            return "ERROR: meetingDescription is not valid <" + meetingDescription + ">";
        } else if (meetingFromDate == null || meetingFromDate.isEmpty()) {
            return "ERROR: meetingFromDate is not valid <" + meetingFromDate + ">";
        } else if (meetingToDate == null || meetingToDate.isEmpty()) {
            return "ERROR: meetingToDate is not valid <" + meetingToDate + ">";
        }
        return "ok";
    }

    private static String isEmailParametersValid(String userId, String password, String smtpHost, String smtpPort,
            String senderEmail, String RecipientsTO, String RecipientsCC, String RecipientsBCC, String emailSubject,
            String emailText) throws EmailParametersException {
        if (userId == null || userId.isEmpty()) {
            throw new EmailParametersException("ERROR: userId is not valid <" + userId + ">");
        } else if (password == null) {
            throw new EmailParametersException("ERROR: password is not valid <" + password + ">");
        } else if (smtpHost == null || smtpHost.isEmpty()) {
            throw new EmailParametersException("ERROR: smtpHost is not valid <" + smtpHost + ">");
        } else if (smtpPort == null || smtpPort.isEmpty()) {
            throw new EmailParametersException("ERROR: smtpPort is not valid <" + smtpPort + ">");
        } else if (senderEmail == null || senderEmail.isEmpty()) {
            throw new EmailParametersException("ERROR: senderEmail is not valid <" + senderEmail + ">");
        } else if (emailSubject == null || emailSubject.isEmpty()) {
            throw new EmailParametersException("ERROR: emailSubject is not valid <" + emailSubject + ">");
        } else if (emailText == null || emailText.isEmpty()) {
            throw new EmailParametersException("ERROR: emailText is not valid <" + emailText + ">");
        } else if (RecipientsTO == null || RecipientsTO.isEmpty()) {
            throw new EmailParametersException("ERROR: RecipientsCC is not valid <" + RecipientsCC + ">");
        }
        try {
            Integer.parseInt(smtpPort);
        } catch (NumberFormatException nfe) {
            throw new EmailParametersException("ERROR: smtpPort is not valid NUMBER <" + smtpPort + ">");
        }
        return "ok";
    }

    public static String exception(Throwable e) {
        StringWriter errors = new StringWriter();
        e.printStackTrace(new PrintWriter(errors));
        return ("EXCEPTION " + errors.toString());
    }

}
