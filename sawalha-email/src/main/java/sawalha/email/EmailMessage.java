package sawalha.email;

import sawalha.email.send.exchange.conf.SmtpConfig;

public class EmailMessage {

    private SmtpConfig smtpConfig;

    private String RecipientsTO;
    private String RecipientsCC;
    private String RecipientsBCC;
    private String emailSubject;
    private String emailText;

    private String meetingSummary;
    private String meetingLocation;
    private String meetingDescription;
    private String meetingFromDate;
    private String meetingToDate;

    private String attachedFiles;

    public SmtpConfig getSmtpConfig() {
        return smtpConfig;
    }

    public void setSmtpConfig(SmtpConfig smtpConfig) {
        this.smtpConfig = smtpConfig;
    }

    public String getRecipientsTO() {
        return RecipientsTO;
    }

    public void setRecipientsTO(String recipientsTO) {
        RecipientsTO = recipientsTO;
    }

    public String getRecipientsCC() {
        return RecipientsCC;
    }

    public void setRecipientsCC(String recipientsCC) {
        RecipientsCC = recipientsCC;
    }

    public String getRecipientsBCC() {
        return RecipientsBCC;
    }

    public void setRecipientsBCC(String recipientsBCC) {
        RecipientsBCC = recipientsBCC;
    }

    public String getEmailSubject() {
        return emailSubject;
    }

    public void setEmailSubject(String emailSubject) {
        this.emailSubject = emailSubject;
    }

    public String getEmailText() {
        return emailText;
    }

    public void setEmailText(String emailText) {
        this.emailText = emailText;
    }

    public String getMeetingSummary() {
        return meetingSummary;
    }

    public void setMeetingSummary(String meetingSummary) {
        this.meetingSummary = meetingSummary;
    }

    public String getMeetingLocation() {
        return meetingLocation;
    }

    public void setMeetingLocation(String meetingLocation) {
        this.meetingLocation = meetingLocation;
    }

    public String getMeetingDescription() {
        return meetingDescription;
    }

    public void setMeetingDescription(String meetingDescription) {
        this.meetingDescription = meetingDescription;
    }

    public String getMeetingFromDate() {
        return meetingFromDate;
    }

    public void setMeetingFromDate(String meetingFromDate) {
        this.meetingFromDate = meetingFromDate;
    }

    public String getMeetingToDate() {
        return meetingToDate;
    }

    public void setMeetingToDate(String meetingToDate) {
        this.meetingToDate = meetingToDate;
    }

    public String getAttachedFiles() {
        return attachedFiles;
    }

    public void setAttachedFiles(String attachedFiles) {
        this.attachedFiles = attachedFiles;
    }

}
