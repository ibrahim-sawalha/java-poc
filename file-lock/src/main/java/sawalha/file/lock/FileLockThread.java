package sawalha.file.lock;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.RandomAccessFile;

import org.apache.commons.io.FileUtils;

public class FileLockThread {
    public static void main(String[] args) {
        ThreadB b = new ThreadB();
        b.start();

        synchronized (b) {
            try {
                System.out.println("Waiting for b to complete...");
                b.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println("Total is: " + b.total);
        }
    }
}

class ThreadB extends Thread {
    int total;

    @Override
    public void run() {
        synchronized (this) {
            boolean isLocked = true;
            RandomAccessFile rf = null;

            File file = FileUtils.getFile("C:/Users/User/Desktop/foxit-out/test.pdf");

            while (isLocked) {
                try {
                    rf = new RandomAccessFile(file, "rw");
                    isLocked = false;
                    System.out.println("File is NOT locked");
                } catch (FileNotFoundException e1) {
                    System.out.println("File is locked");
                    try {
                        sleep(1500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            notify();
        }
    }
}
