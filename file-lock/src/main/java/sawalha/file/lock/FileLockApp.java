package sawalha.file.lock;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;

import org.apache.commons.io.FileUtils;

public class FileLockApp {

    public static void main(String[] args) {
        File file = FileUtils.getFile("C:/Users/User/Desktop/foxit-out/test.pdf");

        System.out.println("file.canRead()..." + file.canRead());
        System.out.println("file.canExecute()..." + file.canExecute());
        System.out.println("file.canWrite()..." + file.canWrite());

        while (!file.canWrite()) {
            System.out.println("File is locked...");
        }

        System.out.println("File is NOT locked...");

        // A test file for locking has been created in FileLockExampleFile1.
        // Please run that file before running this.
        RandomAccessFile rf = null;

        boolean isLocked = true;

        while (isLocked) {
            try {
                rf = new RandomAccessFile(file, "rw");
                isLocked = false;
                System.out.println("File is NOT locked");
            } catch (FileNotFoundException e1) {
                System.out.println("File is locked");
            }
        }
        FileChannel fileChannel = rf.getChannel();

        // let us try to get a lock. FileLockExampleFile1 has an exclusive lock
        // so this call will block till FileLockExampleFile1 releases lock
        System.out.println("Trying to acquire lock");
        FileLock lockT = null;
        try {
            lockT = fileChannel.lock();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        // FileLockExampleFile1 has release lock.
        System.out.println("file lock acquired");
        try {
            lockT.release();
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

    }

}
