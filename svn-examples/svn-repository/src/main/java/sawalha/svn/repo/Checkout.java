package sawalha.svn.repo;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.io.SVNRepository;
import org.tmatesoft.svn.core.io.SVNRepositoryFactory;
import org.tmatesoft.svn.core.wc.SVNClientManager;
import org.tmatesoft.svn.core.wc.SVNRevision;
import org.tmatesoft.svn.core.wc.SVNUpdateClient;
import org.tmatesoft.svn.core.wc.SVNWCUtil;

public class Checkout {
    private static final Logger LOG = LoggerFactory.getLogger(Checkout.class);

    public static void main(String... args) {

        String userName = "ibrahim.sawalha";
        String userPassword = "Eng1neer";

        final String url = "http://10.3.2.17/svn/svnkit-test";
        final String destPath = "c:/temp/svntest111";

        SVNRepository repository = null;

        try {
            // initiate the reporitory from the url
            repository = SVNRepositoryFactory.create(SVNURL.parseURIDecoded(url));
            // create authentication data
            ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager(userName,
                userPassword);
            repository.setAuthenticationManager(authManager);
            // output some data to verify connection
            LOG.info("Repository Root: " + repository.getRepositoryRoot(true));
            LOG.info("Repository UUID: " + repository.getRepositoryUUID(true));
            // need to identify latest revision
            long latestRevision = repository.getLatestRevision();
            LOG.info("Repository Latest Revision: " + latestRevision);

            // create client manager and set authentication
            SVNClientManager ourClientManager = SVNClientManager.newInstance();
            ourClientManager.setAuthenticationManager(authManager);
            // use SVNUpdateClient to do the export
            SVNUpdateClient updateClient = ourClientManager.getUpdateClient();
            updateClient.setIgnoreExternals(false);

            updateClient.doCheckout(repository.getLocation(), new File(destPath), SVNRevision.create(latestRevision),
                SVNRevision.create(latestRevision), SVNDepth.INFINITY, true);

        } catch (SVNException e) {
            e.printStackTrace();
        } finally {
            LOG.info("Done");
        }
    }
}
