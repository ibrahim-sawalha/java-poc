package sawalha.svn.repo;

import java.io.File;

import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.internal.io.dav.DAVRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.fs.FSRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.svn.SVNRepositoryFactoryImpl;
import org.tmatesoft.svn.core.io.SVNRepository;
import org.tmatesoft.svn.core.io.SVNRepositoryFactory;
import org.tmatesoft.svn.core.wc.ISVNOptions;
import org.tmatesoft.svn.core.wc.SVNClientManager;
import org.tmatesoft.svn.core.wc.SVNWCUtil;

public class LockFile {
    private static String userName = "ibrahim.sawalha";
    private static String userPassword = "Eng1neer";

    private static final String url = "http://10.3.2.17/svn/svnkit-test";
    private static final String destPath = "c:/temp/svntest111";

    public static void lockFileOnSystem() {
        try {

            DAVRepositoryFactory.setup();
            SVNRepositoryFactoryImpl.setup();
            FSRepositoryFactory.setup();

            File fileToLock = new File("C:/temp/svntest111/vars.xml");
            File[] filesToLock = new File[] { fileToLock };

            ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager("ibrahim.sawalha",
                "Eng1neer");
            ISVNOptions options = SVNWCUtil.createDefaultOptions(true);

            SVNClientManager clientManager = SVNClientManager.newInstance(options, authManager);

            clientManager.getWCClient().doLock(filesToLock, false, "locking");

        } catch (SVNException e) {

            System.out.println(e.toString());
        }

        System.out.println("Done");
    }

    public static void lockFileOnURL() {
        SVNRepository repository = null;
        try {

            DAVRepositoryFactory.setup();
            SVNRepositoryFactoryImpl.setup();
            FSRepositoryFactory.setup();

            // initiate the reporitory from the url
            repository = SVNRepositoryFactory.create(SVNURL.parseURIDecoded(url));
            // create authentication data
            ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager(userName,
                userPassword);
            repository.setAuthenticationManager(authManager);

            SVNURL svnUrl = SVNURL.parseURIDecoded("http://10.3.2.17/svn/svnkit-test/test/file.txt");
            SVNURL[] svnUrls = new SVNURL[] { svnUrl };

            ISVNOptions options = SVNWCUtil.createDefaultOptions(true);

            SVNClientManager clientManager = SVNClientManager.newInstance(options, authManager);

            clientManager.getWCClient().doLock(svnUrls, false, "This file is locked, please review with Mr. XXX");

            // clientManager.getWCClient().doUnlock(svnUrls, true);

        } catch (SVNException e) {

            System.out.println(e.toString());
        }

        System.out.println("Done");
    }

    public static void main(String[] args) {
        lockFileOnURL();
    }

}
