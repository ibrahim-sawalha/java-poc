package sawalha.svn.repo;

import java.util.Collection;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tmatesoft.svn.core.SVNDirEntry;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNNodeKind;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.io.SVNRepository;
import org.tmatesoft.svn.core.io.SVNRepositoryFactory;
import org.tmatesoft.svn.core.wc.SVNWCUtil;

public class DisplayRepositoryTree {
    private static final Logger LOG = LoggerFactory.getLogger(DisplayRepositoryTree.class);
    private static long revisionNumber = -1;

    public static void main(String[] args) {

        try {

            SVNURL svnUrl;
            svnUrl = SVNURL.parseURIDecoded("http://10.3.2.17/svn/svnkit-test");
            SVNRepository repository = SVNRepositoryFactory.create(svnUrl, null);

            // set an auth manager which will provide user credentials
            // ISVNAuthenticationManager basicAuthManager = new BasicAuthenticationManager("ibrahim.sawalha",
            // "123456789");

            ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager("ibrahim.sawalha",
                "Eng1neer");

            repository.setAuthenticationManager(authManager);

            LOG.info("Repository Root: " + repository.getRepositoryRoot(true));
            LOG.info("Repository UUID: " + repository.getRepositoryUUID(true));

            // SVNNodeKind nodeKind = repository.checkPath("", -1); // -1 latest revision
            SVNNodeKind nodeKind = repository.checkPath("", revisionNumber);
            if (nodeKind == SVNNodeKind.NONE) {
                LOG.error("There is no entry at '" + svnUrl + "'.");
                System.exit(1);
            } else if (nodeKind == SVNNodeKind.FILE) {
                LOG.error("The entry at '" + svnUrl + "' is a file while a directory was expected.");
                System.exit(1);
            }

            // listEntries(repository,-1, ""); // -1 last revision
            listEntries(repository, revisionNumber, "");

            Long latestRevision = repository.getLatestRevision();
            LOG.info("Repository latest revision: " + latestRevision);
        } catch (SVNException e) {
            e.printStackTrace();
        }
    }

    public static void listEntries(SVNRepository repository, long revision, String path) throws SVNException {
        Collection<?> entries = repository.getDir(path, revision, null, (Collection<?>) null);
        Iterator<?> iterator = entries.iterator();
        while (iterator.hasNext()) {
            SVNDirEntry entry = (SVNDirEntry) iterator.next();
            LOG.info("/" + (path.equals("") ? "" : path + "/") + entry.getName() + " ( author: '" + entry.getAuthor()
                    + "'; revision: " + entry.getRevision() + "; date: " + entry.getDate() + "; commit: "
                    + entry.getCommitMessage() + ")");
            if (entry.getKind() == SVNNodeKind.DIR) {
                listEntries(repository, revision, (path.equals("")) ? entry.getName() : path + "/" + entry.getName());
            }
        }
    }
}
