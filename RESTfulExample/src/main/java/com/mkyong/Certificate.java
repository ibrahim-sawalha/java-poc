package com.mkyong;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Certificate {

    private byte[] certificate;
    private byte[] signature;

    public byte[] getCertificate() {
        return certificate;
    }

    public void setCertificate(byte[] certificate) {
        this.certificate = certificate;
    }

    public byte[] getSignature() {
        return signature;
    }

    public void setSignature(byte[] signature) {
        this.signature = signature;
    }

}
