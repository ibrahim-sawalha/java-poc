package com.mkyong.client;

import java.awt.Transparency;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.ComponentColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.Raster;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;

import com.mkyong.Certificate;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class CertificateClient {

    public static void main(String[] args) {
        try {

            Client client = Client.create();

            // WebResource webResource =
            // client.resource("http://localhost:8787/RESTfulExample/rest/xml/certificate/get");
            WebResource webResource = client.resource("http://localhost:8787/RESTfulExample/rest/json/certificate/get");

            ClientResponse response = webResource.accept("application/json").get(ClientResponse.class);

            if (response.getStatus() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
            }

            Certificate output = response.getEntity(Certificate.class);

            FileUtils.writeByteArrayToFile(new File("c:/test-2.p12"), output.getCertificate());

            OutputStream os = new FileOutputStream("c:/test.jpg");
            BufferedImage image = createRGBImage(output.getSignature(), 1920, 1080);
            ImageIO.write(image, "jpg", os);

            System.out.println("Output from Server .... \n");
            System.out.println(output);

        } catch (Exception e) {

            e.printStackTrace();

        }
    }

    private static BufferedImage createRGBImage(byte[] bytes, int width, int height) {
        DataBufferByte buffer = new DataBufferByte(bytes, bytes.length);
        ColorModel cm = new ComponentColorModel(ColorSpace.getInstance(ColorSpace.CS_sRGB), new int[] { 8, 8, 8 },
                false, false, Transparency.OPAQUE, DataBuffer.TYPE_BYTE);
        return new BufferedImage(cm, Raster.createInterleavedRaster(buffer, width, height, width * 3, 3, new int[] { 0,
                1, 2 }, null), false, null);
    }

}
