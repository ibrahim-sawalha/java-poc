package com.mkyong.rest;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.io.FileUtils;

import com.mkyong.Certificate;
import com.mkyong.Track;

@Path("/json/certificate")
public class JsonCertificateService {

    @GET
    @Path("/get")
    @Produces(MediaType.APPLICATION_JSON)
    public Certificate getTrackInJSON() {
        Certificate c = new Certificate();
        c.setCertificate(getCertificate());
        c.setSignature(getImage());
        return c;
    }

    public byte[] getImage() {
        // open image
        File imgPath = new File("test.jpg");
        System.out.println("Path:" + imgPath.getAbsolutePath());
        BufferedImage bufferedImage;
        try {
            bufferedImage = ImageIO.read(imgPath);

            // get DataBufferBytes from Raster
            WritableRaster raster = bufferedImage.getRaster();
            DataBufferByte data = (DataBufferByte) raster.getDataBuffer();
            return (data.getData());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public byte[] getCertificate() {
        // open image
        File file = new File("test.p12");

        try {
            return FileUtils.readFileToByteArray(file);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    @POST
    @Path("/post")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createTrackInJSON(Track track) {

        String result = "Track saved : " + track;
        return Response.status(201).entity(result).build();

    }

}
