package poc.restful.service;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import poc.restful.model.User;
import poc.restful.model.Users;

@Path("/user-management")
public class UserManagementModule {
    @GET
    @Path("/users/{id}")
    @Produces("application/json")
    public Response getUserById(@PathParam("id") Integer id) {
        User user = new User();
        user.setId(id);
        user.setFirstName("Lokesh");
        user.setLastName("Gupta");
        return Response.status(200).entity(user).build();
    }

    @GET
    @Path("/users")
    @Produces("application/vnd.com.demo.user-management.users+xml;charset=UTF-8;version=1")
    public Users getAllUsers() {
        User user1 = new User();
        user1.setId(1);
        user1.setFirstName("demo");
        user1.setLastName("user");
        user1.setUri("/user-management/users/1");

        User user2 = new User();
        user2.setId(2);
        user2.setFirstName("Mark");
        user2.setLastName("Dwain");
        user2.setUri("/user-management/users/2");

        Users users = new Users();
        users.setUsers(new ArrayList<User>());
        users.getUsers().add(user1);
        users.getUsers().add(user2);

        return users;
    }

    @POST
    @Path("/users")
    @Consumes("application/vnd.com.demo.user-management.user+xml;charset=UTF-8;version=1")
    public Response createUser(User user, @DefaultValue("false") @QueryParam("allow-admin") boolean allowAdmin)
            throws URISyntaxException {
        System.out.println(user.getFirstName());
        System.out.println(user.getLastName());
        return Response.status(201).contentLocation(new URI("/user-management/users/123")).build();
    }
}
