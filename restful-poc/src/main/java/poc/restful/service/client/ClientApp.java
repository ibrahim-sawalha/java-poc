package poc.restful.service.client;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

import poc.restful.model.User;
import poc.restful.model.Users;

public class ClientApp {

    public static String SERVER_URL = "http://localhost:8787/restful-poc";

    public static void main(String[] args) {
        getExample_one();
        getExample_two();
        getExample_three();
        postExample();
    }

    private static void getExample_one() {
        ResteasyClient client = new ResteasyClientBuilder().build();
        ResteasyWebTarget target = client.target(SERVER_URL + "/user-management/users");
        Response response = target.request().get();
        // Read output in string format
        String value = response.readEntity(String.class);
        System.out.println(value);
        response.close();
    }

    private static void getExample_three() {
        ResteasyClient client = new ResteasyClientBuilder().build();
        ResteasyWebTarget target = client.target(SERVER_URL + "/user-management/users/9");
        Response response = target.request().get();
        // Read output in string format
        User value = response.readEntity(User.class);
        System.out.println(value);
        // User userValue = response.readEntity(User.class);
        // System.out.println(userValue);
        response.close();
    }

    private static void getExample_two() {
        ResteasyClient client = new ResteasyClientBuilder().build();
        ResteasyWebTarget target = client.target(SERVER_URL + "/user-management/users");
        Response response = target.request().get();
        // Read the entity
        Users users = response.readEntity(Users.class);
        for (User user : users.getUsers()) {
            System.out.println(user.getId());
            System.out.println(user.getLastName());
        }
        response.close();
    }

    private static void postExample() {
        User user = new User();
        user.setFirstName("john");
        user.setLastName("Maclane");

        ResteasyClient client = new ResteasyClientBuilder().build();
        ResteasyWebTarget target = client.target(SERVER_URL + "/user-management/users");
        Response response = target.request().post(
            Entity.entity(user, "application/vnd.com.demo.user-management.user+xml;charset=UTF-8;version=1"));
        // Read output in string format
        System.out.println(response.getStatus());
        response.close();
    }
}
