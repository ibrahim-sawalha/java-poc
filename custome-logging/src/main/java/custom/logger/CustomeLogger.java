package custom.logger;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;

public class CustomeLogger {

    public static String NONE = "NONE";
    private static File logFile = null;
    private static String logFilePath = "log-file.log";

    private static boolean enableLog = true;

    public static void setDebugEnable(boolean value) {
        enableLog = value;
    }

    public static void info(Object... message) {
        log("INFO", getCallingClass().getClassName(), getCallingClass().getLineNumber(), getString(message));
    }

    public static void debug(Object... message) {
        if (enableLog) {
            log("DEBUG", getCallingClass().getClassName(), getCallingClass().getLineNumber(), getString(message));
        }
    }

    public static void exception(Exception e) {
        StringWriter errors = new StringWriter();
        e.printStackTrace(new PrintWriter(errors));
        log("EXCEPTION", getCallingClass().getClassName(), getCallingClass().getLineNumber(), errors.toString());
    }

    public static void exception(Throwable e) {
        StringWriter errors = new StringWriter();
        e.printStackTrace(new PrintWriter(errors));
        log("EXCEPTION", getCallingClass().getClassName(), getCallingClass().getLineNumber(), errors.toString());
    }

    public static void fatal(Object... message) {
        log("FATAL", getCallingClass().getClassName(), getCallingClass().getLineNumber(), getString(message));
    }

    private static void log(String errorType, String className, int lineNumber, String message) {
        try {
            DateFormat dateFormat = new SimpleDateFormat("h:mm:ssaa dd-MM-yy");
            Calendar cal = Calendar.getInstance();
            System.out.println(dateFormat.format(cal.getTime()));
            StringBuffer strBuffer = new StringBuffer();
            strBuffer.append("[");
            strBuffer.append(errorType);
            strBuffer.append("] [");
            strBuffer.append(dateFormat.format(cal.getTime()));
            strBuffer.append("] <");
            strBuffer.append(className);
            strBuffer.append(":");
            strBuffer.append(lineNumber);
            strBuffer.append("> ");
            strBuffer.append(message);
            strBuffer.append("\n");
            logSystemConsole(errorType, className, lineNumber, message);
            FileUtils.writeStringToFile(getLogFile(), strBuffer.toString(), true);
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    private static void logSystemConsole(String errorType, String className, int lineNumber, String message) {
        StringBuffer strBuffer = new StringBuffer();
        strBuffer.append("[");
        strBuffer.append(errorType);
        strBuffer.append("] <");
        strBuffer.append(className);
        strBuffer.append(":");
        strBuffer.append(lineNumber);
        strBuffer.append("> ");
        strBuffer.append(message);
        System.out.println(strBuffer.toString());

    }

    private static StackTraceElement getCallingClass() {
        return Thread.currentThread().getStackTrace()[3];
    }

    private static String getString(Object... message) {
        StringBuffer buffer = new StringBuffer();
        for (Object s : message) {
            buffer.append(s);
            buffer.append("");
        }
        return buffer.toString();
    }

    private static File getLogFile() {
        if (logFile == null) {
            try {
                logFile = new File(logFilePath);
                if (!logFile.exists()) {
                    logFile.createNewFile();
                } else {
                    long fileSizeMB = logFile.length() / 1024;
                    if (fileSizeMB > 25600) { // 10 MB
                        logFile.delete();
                        logFile.createNewFile();
                    }
                }
            } catch (IOException e) {
                System.out.println(e);
            }
        }
        return logFile;
    }

}
