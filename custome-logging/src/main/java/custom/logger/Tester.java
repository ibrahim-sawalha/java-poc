package custom.logger;

public class Tester {

    public static void main(String args[]) {
        CustomeLogger.setDebugEnable(true);
        CustomeLogger.info("info");
        CustomeLogger.debug("debug");
        CustomeLogger.fatal("fatal");

        try {
            int i = 9 / 0;
        } catch (Exception e) {
            CustomeLogger.exception(e);
        }
    }
}
