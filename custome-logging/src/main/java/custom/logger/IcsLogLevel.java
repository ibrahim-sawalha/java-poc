package custom.logger;

public enum IcsLogLevel {

    DEBUG("DEBUG"), INFO("INFO"), FATAL("FATAL"), OFF("OFF");

    private String statusCode;

    private IcsLogLevel(String s) {
        statusCode = s;
    }

    public String getStatusCode() {
        return statusCode;
    }

}
