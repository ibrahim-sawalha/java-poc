package ics.installer.generator;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;

public class WeblogicScriptPath {

    final static Logger log = (Logger) LoggerFactory.getLogger(WeblogicScriptPath.class);

    private WeblogicScriptPath() {
    }

    public static String weblogic_scripts_OS_path = "/home/bankthin/installer-scripts";
    public static String weblogic_scripts_local_path = "/shell-scripts/weblogic-scripts";
    //
    public static String updateJvmcontrollers_Py_OSPATH = weblogic_scripts_OS_path + "/update-jvmcontrollers.py";
    public static String updateJvmcontrollers_Py_LocalPATH = weblogic_scripts_local_path + "/update-jvmcontrollers.py";
    //
    public static String stopBanksJVM_Sh_OSPATH = weblogic_scripts_OS_path + "/stop_banksJVM.sh";
    public static String stopBanksJVM_Sh_LocalPATH = weblogic_scripts_local_path + "/stop_banksJVM.sh";

    public static String startBanksJVM_Sh_OSPATH = weblogic_scripts_OS_path + "/start_banksJVM.sh";
    public static String startBanksJVM_Sh_LocalPATH = weblogic_scripts_local_path + "/start_banksJVM.sh";
    //
    public static String CreateWeblogicUser_py_OSPATH = weblogic_scripts_OS_path
            + "/create-wl-user/create-weblogic-user.py";
    public static String CreateWeblogicUser_py_LocalPATH = weblogic_scripts_local_path
            + "/create-wl-user/create-weblogic-user.py";

    public static String CreateWeblogicUser_sh_OSPATH = weblogic_scripts_OS_path
            + "/create-wl-user/create-weblogic-user.sh";
    public static String CreateWeblogicUser_sh_LocalPATH = weblogic_scripts_local_path
            + "/create-wl-user/create-weblogic-user.sh";
    //
    public static String UpdateFormsUrl_py_OSPATH = weblogic_scripts_OS_path + "/update-forms-url.py";
    public static String UpdateFormsUrl_py_LocalPATH = weblogic_scripts_local_path + "/update-forms-url.py";
}
