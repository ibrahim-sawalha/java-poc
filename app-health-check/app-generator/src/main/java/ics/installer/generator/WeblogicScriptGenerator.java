package ics.installer.generator;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;
// import static ics.installer.generator.WeblogicScriptPath.weblogic_scripts_OS_path;
// import static ics.installer.generator.WeblogicScriptPath.weblogic_scripts_local_path;;

public class WeblogicScriptGenerator {

    final static Logger log = (Logger) LoggerFactory.getLogger(WeblogicScriptGenerator.class);
    private final static String generateMessage = "Generating script: ";

    private WeblogicScriptGenerator() {
    }

    public static void generateBanksJVMScripts(boolean force) throws IOException {
        File startBanksJVMFile = FileUtils.getFile(WeblogicScriptPath.startBanksJVM_Sh_OSPATH);
        File stopBanksJVMFile = FileUtils.getFile(WeblogicScriptPath.stopBanksJVM_Sh_OSPATH);
        log.info(generateMessage + startBanksJVMFile.getAbsolutePath());
        if (!startBanksJVMFile.exists() || (startBanksJVMFile.exists() && force)) {
            FileUtils.copyInputStreamToFile(
                WeblogicScriptGenerator.class.getResourceAsStream(WeblogicScriptPath.startBanksJVM_Sh_LocalPATH),
                startBanksJVMFile);
            startBanksJVMFile.setExecutable(true);
        }
        log.info(generateMessage + stopBanksJVMFile.getAbsolutePath());
        if (!stopBanksJVMFile.exists() || (stopBanksJVMFile.exists() && force)) {
            FileUtils.copyInputStreamToFile(
                WeblogicScriptGenerator.class.getResourceAsStream(WeblogicScriptPath.stopBanksJVM_Sh_LocalPATH),
                stopBanksJVMFile);
            stopBanksJVMFile.setExecutable(true);
        }
    }

    public static void generateUpdateJvmControllers(boolean force) throws IOException {
        log.info(generateMessage + WeblogicScriptPath.updateJvmcontrollers_Py_OSPATH);
        File file = FileUtils.getFile(WeblogicScriptPath.updateJvmcontrollers_Py_OSPATH);
        if (!file.exists() || (file.exists() && force)) {
            FileUtils.copyInputStreamToFile(
                WeblogicScriptGenerator.class.getResourceAsStream(WeblogicScriptPath.updateJvmcontrollers_Py_LocalPATH),
                FileUtils.getFile(WeblogicScriptPath.updateJvmcontrollers_Py_OSPATH));
        }
    }

    public static void generateCreateWeblogicUser(boolean force) throws IOException {
        log.info(generateMessage + WeblogicScriptPath.CreateWeblogicUser_py_OSPATH);
        log.info(generateMessage + WeblogicScriptPath.CreateWeblogicUser_sh_OSPATH);

        File shFile = FileUtils.getFile(WeblogicScriptPath.CreateWeblogicUser_sh_OSPATH);
        File pyFile = FileUtils.getFile(WeblogicScriptPath.CreateWeblogicUser_py_OSPATH);
        if (!shFile.exists() || (shFile.exists() && force)) {
            FileUtils.copyInputStreamToFile(
                WeblogicScriptGenerator.class.getResourceAsStream(WeblogicScriptPath.CreateWeblogicUser_sh_LocalPATH),
                FileUtils.getFile(WeblogicScriptPath.CreateWeblogicUser_sh_OSPATH));
            FileUtils.getFile(WeblogicScriptPath.CreateWeblogicUser_sh_OSPATH).setExecutable(true);
        }
        if (!pyFile.exists() || (pyFile.exists() && force)) {
            FileUtils.copyInputStreamToFile(
                WeblogicScriptGenerator.class.getResourceAsStream(WeblogicScriptPath.CreateWeblogicUser_py_LocalPATH),
                FileUtils.getFile(WeblogicScriptPath.CreateWeblogicUser_py_OSPATH));
        }
    }

    public static void generateUpdateFormsUrl(boolean force) throws IOException {
        log.info(generateMessage + WeblogicScriptPath.UpdateFormsUrl_py_OSPATH);
        File file = FileUtils.getFile(WeblogicScriptPath.UpdateFormsUrl_py_OSPATH);
        if (!file.exists() || (file.exists() && force)) {
            FileUtils.copyInputStreamToFile(
                WeblogicScriptGenerator.class.getResourceAsStream(WeblogicScriptPath.UpdateFormsUrl_py_LocalPATH),
                FileUtils.getFile(WeblogicScriptPath.UpdateFormsUrl_py_OSPATH));
        }
    }
}
