package ics.installer.generator;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;

public class JBossScriptPath {

    final static Logger log = (Logger) LoggerFactory.getLogger(JBossScriptPath.class);

    private JBossScriptPath() {
    }

    public static String jboss_scripts_OS_path = "/home/brmsthin/installer-scripts";
    public static String jboss_scripts_local_path = "/shell-scripts/jboss-scripts";
    //
    public static String updateBanksDS_Sh_OSPATH = jboss_scripts_OS_path + "/updateBanksDS.sh";
    public static String updateBanksDS_Sh_LocalPATH = jboss_scripts_local_path + "/scripts/updateBanksDS.sh";
    public static String updateBanksDS_Cli_OSPATH = jboss_scripts_OS_path + "/updateBanksDS.cli";
    public static String updateBanksDS_Cli_LocalPATH = jboss_scripts_local_path + "/scripts/updateBanksDS.cli";
    //
    public static String updateBpmDS_Sh_OSPATH = jboss_scripts_OS_path + "/updateBpmDS.sh";
    public static String updateBpmDS_Sh_LocalPATH = jboss_scripts_local_path + "/scripts/updateBpmDS.sh";
    public static String updateBpmDS_Cli_OSPATH = jboss_scripts_OS_path + "/updateBpmDS.cli";
    public static String updateBpmDS_Cli_LocalPATH = jboss_scripts_local_path + "/scripts/updateBpmDS.cli";
    //
    public static String UpdateLimitsConf_py_OSPATH = jboss_scripts_OS_path + "/UpdateLimitsConf.py";
    public static String UpdateLimitsConf_py_LocalPATH = jboss_scripts_local_path + "/scripts/UpdateLimitsConf.py";
    //
    public static String UpdateEmailAddress_py_OSPATH = jboss_scripts_OS_path + "/update-email.py";
    public static String UpdateEmailAddress_py_LocalPATH = jboss_scripts_local_path + "/scripts/update-email.py";
    //
    public static final String InstallJBossServer_py_LocalPATH = jboss_scripts_local_path
            + "/scripts/install-jboss-server.py";
    public static final String InstallJBossServer_py_OSPATH = jboss_scripts_OS_path + "/install-jboss-server.py";
    //
    public static final String PrepareJBossServer_py_LocalPATH = jboss_scripts_local_path
            + "/scripts/prepare-jboss-server.py";
    public static final String PrepareJBossServer_py_OSPATH = jboss_scripts_OS_path + "/prepare-jboss-server.py";
}
