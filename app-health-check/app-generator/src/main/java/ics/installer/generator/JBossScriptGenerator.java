package ics.installer.generator;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;
import ics.installer.common.zip.DateTimeUtil;

public class JBossScriptGenerator {

    final static Logger log = (Logger) LoggerFactory.getLogger(JBossScriptGenerator.class);

    private JBossScriptGenerator() {
    }

    public static void generateJBossMenu(boolean force) throws IOException {

        String brmsScriptsPath = "/home/brmsthin/brms-scripts";

        FileUtils.copyFile(FileUtils.getFile("/home/brmsthin/.profile"),
            FileUtils.getFile("/home/brmsthin/.profile-" + DateTimeUtil.getCurrentDateTiem()));
        // if (!FileUtils.getFile("/home/brmsthin/.profile_backup").exists()) {
        // log.info("Creating backup file /home/brmsthin/.profile_backup");
        // } else {
        // log.warn("Backup file /home/brmsthin/.profile_backup already exist.");
        // }

        log.info("Creating file /home/brmsthin/.profile");
        FileUtils.copyInputStreamToFile(
            JBossScriptGenerator.class.getResourceAsStream(JBossScriptPath.jboss_scripts_local_path + "/menu/.profile"),
            FileUtils.getFile("/home/brmsthin/.profile"));

        log.info("Creating file " + brmsScriptsPath + "/brms_clean_log.sh");
        FileUtils.copyInputStreamToFile(
            JBossScriptGenerator.class
                    .getResourceAsStream(JBossScriptPath.jboss_scripts_local_path + "/menu/brms_clean_log.sh"),
            FileUtils.getFile(brmsScriptsPath + "/brms_clean_log.sh"));
        FileUtils.getFile(brmsScriptsPath + "/brms_clean_log.sh").setExecutable(true);

        log.info("Creating file " + brmsScriptsPath + "/brms_kill.sh");
        FileUtils.copyInputStreamToFile(
            JBossScriptGenerator.class
                    .getResourceAsStream(JBossScriptPath.jboss_scripts_local_path + "/menu/brms_kill.sh"),
            FileUtils.getFile(brmsScriptsPath + "/brms_kill.sh"));
        FileUtils.getFile(brmsScriptsPath + "/brms_kill.sh").setExecutable(true);

        log.info("Creating file " + brmsScriptsPath + "/brms_start.sh");
        FileUtils.copyInputStreamToFile(
            JBossScriptGenerator.class
                    .getResourceAsStream(JBossScriptPath.jboss_scripts_local_path + "/menu/brms_start.sh"),
            FileUtils.getFile(brmsScriptsPath + "/brms_start.sh"));
        FileUtils.getFile(brmsScriptsPath + "/brms_start.sh").setExecutable(true);

        log.info("Creating file " + brmsScriptsPath + "/brms_status.sh");
        FileUtils.copyInputStreamToFile(
            JBossScriptGenerator.class
                    .getResourceAsStream(JBossScriptPath.jboss_scripts_local_path + "/menu/brms_status.sh"),
            FileUtils.getFile(brmsScriptsPath + "/brms_status.sh"));
        FileUtils.getFile(brmsScriptsPath + "/brms_status.sh").setExecutable(true);

        log.info("Creating file " + brmsScriptsPath + "/brms_stop.sh");
        FileUtils.copyInputStreamToFile(
            JBossScriptGenerator.class
                    .getResourceAsStream(JBossScriptPath.jboss_scripts_local_path + "/menu/brms_stop.sh"),
            FileUtils.getFile(brmsScriptsPath + "/brms_stop.sh"));
        FileUtils.getFile(brmsScriptsPath + "/brms_stop.sh").setExecutable(true);
    }

    public static void generateBpmDSJBoss(String dbAddress, String brms6Password, String bankwsPassword)
            throws IOException {
        File updateDSshell = FileUtils.getFile(JBossScriptPath.updateBpmDS_Sh_OSPATH);

        List<String> lines = IOUtils
                .readLines(JBossScriptGenerator.class.getResourceAsStream(JBossScriptPath.updateBpmDS_Cli_LocalPATH));
        List<String> newLines = new ArrayList<String>();
        for (String line : lines) {

            if (line.contains("localhost")) {
                line = line.replaceAll("localhost", dbAddress);
            }
            if (line.contains("--password=brms6")) {
                line = line.replaceAll("brms6", brms6Password);
            }
            if (line.contains("Password:add(value=bankws)")) {
                line = line.replaceAll("bankws", bankwsPassword);
            }
            newLines.add(line);
        }
        FileUtils.writeLines(FileUtils.getFile(JBossScriptPath.updateBpmDS_Cli_OSPATH), newLines);

        FileUtils.copyInputStreamToFile(
            JBossScriptGenerator.class.getResourceAsStream(JBossScriptPath.updateBpmDS_Sh_LocalPATH), updateDSshell);
        updateDSshell.setExecutable(true);
    }

    public static void generateBanksDSJBoss(String dbAddress, String brms6Password, String bankwsPassword)
            throws IOException {
        File updateDSshell = FileUtils.getFile(JBossScriptPath.updateBanksDS_Sh_OSPATH);
        List<String> lines = IOUtils
                .readLines(JBossScriptGenerator.class.getResourceAsStream(JBossScriptPath.updateBanksDS_Cli_LocalPATH));
        List<String> newLines = new ArrayList<String>();
        for (String line : lines) {

            if (line.contains("localhost")) {
                line = line.replaceAll("localhost", dbAddress);
            }
            if (line.contains("--password=brms6")) {
                line = line.replaceAll("brms6", brms6Password);
            }
            if (line.contains("Password:add(value=bankws)")) {
                line = line.replaceAll("bankws", bankwsPassword);
            }
            newLines.add(line);
        }
        FileUtils.writeLines(FileUtils.getFile(JBossScriptPath.updateBanksDS_Cli_OSPATH), newLines);
        FileUtils.copyInputStreamToFile(
            JBossScriptGenerator.class.getResourceAsStream(JBossScriptPath.updateBanksDS_Sh_LocalPATH), updateDSshell);
        updateDSshell.setExecutable(true);
    }

    public static void generateJBossStandaloneXML(File standaloneXmlFile, String jbossAddress, String dbAddress,
            String dbSID, String brms6Password, String bankwsPassword, String emailAddress, String emailPassword)
            throws IOException {
        if (!standaloneXmlFile.exists()) {
            throw new RuntimeException("standalone.xml file was not found at " + standaloneXmlFile.getAbsolutePath());
        }

        File standaloneBackupFile = new File(standaloneXmlFile.getParent() + File.separator + "standalone.xml.backup");
        FileUtils.moveFile(standaloneXmlFile, standaloneBackupFile);

        List<String> lines = IOUtils.readLines(JBossScriptGenerator.class
                .getResourceAsStream(JBossScriptPath.jboss_scripts_local_path + "/standalone.cli"));
        List<String> newLines = new ArrayList<String>();
        for (String line : lines) {

            if (line.contains("bpms-ipAddress")) {
                line = line.replaceAll("bpms-ipAddress", jbossAddress);
            }
            if (line.contains("db-ipAddress")) {
                line = line.replaceAll("db-ipAddress", dbAddress);
            }
            if (line.contains("db-SID")) {
                line = line.replaceAll("db-SID", dbSID);
            }
            if (line.contains("<password>brms6-password</password>")) {
                line = line.replaceAll("brms6-password", brms6Password);
            }
            if (line.contains("<xa-datasource-property name=\"Password\">bankws-password</xa-datasource-property>")) {
                line = line.replaceAll("bankws-password", bankwsPassword);
            }
            if (line.contains("<login name=\"notification-email\" password=\"email-password\"/>")) {
                line = line.replaceAll("notification-email", emailAddress);
                line = line.replaceAll("email-password", emailPassword);
            }
            newLines.add(line);
        }
        FileUtils.writeLines(standaloneXmlFile, newLines);
    }

    public static void generateUpdateLimitsConf(boolean force) throws IOException {
        log.info("Creating Script file " + JBossScriptPath.UpdateLimitsConf_py_OSPATH);
        File file = FileUtils.getFile(JBossScriptPath.UpdateLimitsConf_py_OSPATH);
        if (!file.exists() || (file.exists() && force)) {
            FileUtils.copyInputStreamToFile(
                JBossScriptGenerator.class.getResourceAsStream(JBossScriptPath.UpdateLimitsConf_py_LocalPATH),
                FileUtils.getFile(JBossScriptPath.UpdateLimitsConf_py_OSPATH));
        }
    }

    public static void generateUpdateEmailAddress(boolean force) throws IOException {
        log.info("Creating Script file " + JBossScriptPath.UpdateEmailAddress_py_OSPATH);
        File file = FileUtils.getFile(JBossScriptPath.UpdateEmailAddress_py_OSPATH);
        if (!file.exists() || (file.exists() && force)) {
            FileUtils.copyInputStreamToFile(
                JBossScriptGenerator.class.getResourceAsStream(JBossScriptPath.UpdateEmailAddress_py_LocalPATH),
                FileUtils.getFile(JBossScriptPath.UpdateEmailAddress_py_OSPATH));
        }
    }

    public static void generateInstallJBossServer(boolean force) throws IOException {
        log.info("Creating Script file " + JBossScriptPath.InstallJBossServer_py_OSPATH);
        File file = FileUtils.getFile(JBossScriptPath.InstallJBossServer_py_OSPATH);
        if (!file.exists() || (file.exists() && force)) {
            FileUtils.copyInputStreamToFile(
                JBossScriptGenerator.class.getResourceAsStream(JBossScriptPath.InstallJBossServer_py_LocalPATH),
                FileUtils.getFile(JBossScriptPath.InstallJBossServer_py_OSPATH));
        }
    }

    public static void generatePrepareJBossServer(boolean force) throws IOException {
        log.info("Creating Script file " + JBossScriptPath.PrepareJBossServer_py_OSPATH);
        File file = FileUtils.getFile(JBossScriptPath.PrepareJBossServer_py_OSPATH);
        if (!file.exists() || (file.exists() && force)) {
            FileUtils.copyInputStreamToFile(
                JBossScriptGenerator.class.getResourceAsStream(JBossScriptPath.PrepareJBossServer_py_LocalPATH),
                FileUtils.getFile(JBossScriptPath.PrepareJBossServer_py_OSPATH));
        }
    }

}
