package ics.installer.generator;

public class InstallerPath {

    private InstallerPath() {
    }

    public static String weblogicInstallation_xml_OSPATH = WeblogicScriptPath.weblogic_scripts_OS_path
            + "/weblogic-installation.xml";

    public static String jbossInstallation_xml_OSPATH = JBossScriptPath.jboss_scripts_OS_path
            + "/jboss-installation.xml";

}
