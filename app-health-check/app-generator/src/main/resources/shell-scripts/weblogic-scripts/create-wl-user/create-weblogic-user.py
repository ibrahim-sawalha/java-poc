from java.io import FileInputStream
import sys, getopt

adminUserName="weblogic"
adminPassword="weblogic123"
adminURL="t3://localhost:7001"

group=''
user=''
password = 'weblogic1'

def usage():
  print("Usage: %s -p <weblogic admin password> -s <new username> -w <new username's password> -g <new username's group>" % sys.argv[0])
  sys.exit(2)

try:
    opts, args = getopt.getopt(sys.argv[1:], "hu:p:r:s:g:w:", ["help", "wl_user=", "wl_password=" , "wl_url=" , "user=" , "password=" ,"group="])
except getopt.GetoptError, e:
  print (str(e))
  usage()

for opt, arg in opts:
        if opt in ("-h", "--help"):
          usage()
        if opt in ("-u" , "--wl_user"):
          adminUserName=arg
        if opt in ("-p" , "--wl_password"):
          adminPassword=arg
        if opt in ("-r" , "--wl_url"):
          adminURL=arg
        if opt in ("-s" , "--user"):
          user=arg
        if opt in ("-g" , "--group"):
          group=arg
        if opt in ("-w" , "--password"):
          password=arg

if not adminUserName or not adminPassword or not adminURL or not group or not user:
  usage()

print adminUserName + " " + adminPassword + " " +adminURL
try:
  connect(adminUserName, adminPassword, adminURL)
  serverConfig()

  print '[INFO] lookup DefaultAuthenticator' 

  atnr=cmo.getSecurityConfiguration().getDefaultRealm().lookupAuthenticationProvider('DefaultAuthenticator')

  errorFound=None

  if atnr.groupExists(group):
    if not atnr.userExists(user):
      atnr.createUser(user,password,user)
      atnr.addMemberToGroup(group,user)
      print '[INFO] New username ' + user + ' was created.' 
    else:
      print "[WARN] Username " + user + " already exist."
  else:
    print "[ERROR] Group " + group + " does not exist."
    errorFound=True
except Exception, e:
  errorFound=True
  print "[ERROR] " + str(e)

disconnect()
edit()
if errorFound:
  sys.exit(1)
else:
  sys.exit(0)
