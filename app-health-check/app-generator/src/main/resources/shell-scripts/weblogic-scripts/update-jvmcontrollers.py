import os

classPath=""

if not os.environ.get('FORMS_CONF'):
  raise Exception('$FORMS_CONF was not defined in the OS environment variables.')
if not os.environ.get('ORACLE_INSTANCE'):
  raise Exception('$ORACLE_INSTANCE was not defined in the OS environment variables.')

formsConfDir = open(os.environ.get('FORMS_CONF')+'/default.env', 'r')
jvmcontrollersFile = open(os.environ.get('ORACLE_INSTANCE')+'/config/FRComponent/frcommon/tools/jvm/jvmcontrollers.cfg','r')
outFile = open (os.environ.get('ORACLE_INSTANCE')+'/config/FRComponent/frcommon/tools/jvm/jvmcontrollers.cfg','w')

if not os.path.isfile(os.environ.get('ORACLE_INSTANCE')+'/config/FRComponent/frcommon/tools/jvm/jvmcontrollers.cfg') :
  raise Exception('File not found: ' + os.environ.get('ORACLE_INSTANCE')+'/config/FRComponent/frcommon/tools/jvm/jvmcontrollers.cfg')
if not os.path.isfile(os.environ.get('FORMS_CONF')+'/default.env'):
  raise Exception('File not found: ' + os.path.isfile(os.environ.get('FORMS_CONF')+'/default.env'))

#print formsConfDir
for line in formsConfDir:
 if "CLASSPATH=" in line:
   classPath=line[10:]
   print classPath
   if "brms_proxy.jar" not in classPath or "worklistjboss.jar" not in classPath:
     raise Exception('brms_proxy.jar & worklistjboss.jar was not found in the CLASSPATH at default.env file.')

banksJVMFound=None
newLine=[]
for line in jvmcontrollersFile:
  if line.startswith("[") and not line.startswith("[banksJVM]"):
    banksJVMFound=False
  if line.startswith("[banksJVM]"):
    print "Old configurations for banksJVM was found. It will be deleted."
    banksJVMFound=True
  if (not banksJVMFound):
    newLine.append(line)

newLine.append("[banksJVM]")
newLine.append("jvmoptions=-Xms512m -Xmx1024m -javaagent:/home/thin/banksAgent.jar")
newLine.append("maxsessions=50")
newLine.append("logdir=/myapps/anapp/log")
newLine.append("logging=off")
newLine.append("classpath="+classPath)

print "Adding new banksJVM configuration as following:"
print newLine
#for x in newLine:
#  print x

for line in newLine:
  print>>outFile, line
