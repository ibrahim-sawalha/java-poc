cd $JBOSS_HOME/bin
echo '---------------------------------------------------------------'

if [ -f start.out ]; then
    rm start.out
fi

ps -u brms |grep standalone.sh >start.out 2>&1

if [ -f start.out ]; then
  while read p; do
    if [[ $p == *"standalone.sh"* ]]
    then
      echo 'WARNING: Server is running, server.log file will not be deleted.'
      echo 'Deleting the following files:'
      ls -l $JBOSS_HOME/standalone/log/server.log.*
      rm $JBOSS_HOME/standalone/log/server.log.*
      exit 0
    fi
  done <start.out
fi

echo 'Deleting the following files:'
ls -l $JBOSS_HOME/standalone/log/server.*
rm $JBOSS_HOME/standalone/log/server.*

rm start.out
echo "INFO: Cleaning log files was finished."

