cd $JBOSS_HOME/bin
echo '---------------------------------------------------------------'

set status="idle"

if [ -f stop.out ]; then
    rm stop.out
fi
ps -u brms |grep standalone.sh >stop.out 2>&1

while read p; do
  if [[ $p == *"standalone.sh"* ]]
  then
    echo "Stopping JBoss BPM Suite"
    echo -n 'INFO: '
    ./jboss-cli.sh --connect --command=:shutdown
    echo '---------------------------------------------------------------'
    status="shutdown"
  fi
done <stop.out

if [[ $status != "shutdown" ]]
then
  echo "WARNING: Server is already down."
  echo "Operation is canceled"
  rm stop.out
  exit 1
fi

# wait 5 seconds until server finish shutting down
sleep 5

ps -u brms |grep standalone.sh >stop.out 2>&1
while read p; do
  if [[ $p == *"standalone.sh"* ]]
  then
    echo 'WARNING: Found process with name standalone.sh'
    echo "It maybe the server is starting up."
    echo "Please retry after few seconds"
  fi
done <stop.out

echo "Shutdown was finished."
rm stop.out
