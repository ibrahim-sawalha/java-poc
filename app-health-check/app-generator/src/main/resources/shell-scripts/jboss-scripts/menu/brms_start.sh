# trap ctrl-c and call ctrl_c()
trap ctrl_c INT

function ctrl_c() {
  echo " "
  echo "CTRL-C"
  echo "Exit by user."
  echo "Continue tracing log file using command:"
  echo "tail -f $JBOSS_HOME/standalone/log/server.log"
  exit 0;
}

cd $JBOSS_HOME/bin
echo '---------------------------------------------------------------'

if [ -f start.out ]; then
    rm start.out
fi

ps -u brms |grep standalone.sh >start.out 2>&1

if [ -f start.out ]; then
  while read p; do
    if [[ $p == *"standalone.sh"* ]]
    then
      echo 'ERROR: Server is running, we found process with name "standalone.sh".'
      echo 'Server cannot have multiple instances.'
      echo ''
      echo 'Current Server Status:'
      ./jboss-cli.sh -c --command="read-attribute server-state"
      echo ''
      rm start.out
      exit 0
    fi
  done <start.out
fi

echo 'Starting Server...'

# jbpm
nohup ./standalone.sh -Dorg.kie.demo=false -c standalone-full.xml -b 0.0.0.0 -bmanagement 0.0.0.0>/dev/null &

# jboss
#nohup ./standalone.sh -b 0.0.0.0 -bmanagement 0.0.0.0>/dev/null 2>&1 &



sleep 3 # wait 3 seconds then start reading the log file
tail -f $JBOSS_HOME/standalone/log/server.log | while read p
do 
  trap ctrl_c INT
  if [[ $p == *"JBAS015874"* ]] # the server is in run status
  then
    echo ' done.'
    echo '---------------------------------------------------------------'
    echo 'INFO: Server is running.'
    echo '---------------------------------------------------------------'
    rm start.out
    exit 0
  elif [[ $p == *"JBAS015950"* ]] # the server is in stop status JBAS015950
  then
    echo ' .'
    echo '---------------------------------------------------------------'
    echo 'FATAL ERROR: Cannot start the server, there  was exceptions.'
    echo 'Review /u01/EAP6/jboss-eap-6.4/standalone/log/server.log for errors'
    echo '---------------------------------------------------------------'
    exit 0
  else
    echo -n "."
  fi
done

echo "Starting was finished."
