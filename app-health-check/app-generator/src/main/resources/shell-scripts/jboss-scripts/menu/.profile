umask 000
# ibrahim: admin should provide the correct Java Home path
# The following JAVA_HOME is found in Serve 35
JAVA_HOME=/usr/java/jdk1.7.0_79; export JAVA_HOME
PATH=.:$JAVA_HOME/bin:$PATH; export PATH

#JBOSS_HOME=/u01/EAP6/jboss-eap-6.4 export JBOSS_HOME
JBOSS_HOME=/u01/EAP6/jboss-eap-6.4/wildfly-8.1.0.Final/ export JBOSS_HOME
PATH=.:$JBOSS_HOME:$PATH; export PATH

INSTALLER_HOME=/home/brmsthin/; export INSTALLER_HOME
PATH=.:$INSTALLER_HOME:$PATH; export PATH

#------------------------------------------------------------------------
start_brms()  {
    /home/brmsthin/brms-scripts/brms_start.sh
    echo 'operation finished, press any key to continue.'
    read dumy
}
#------------------------------------------------
stop_brms()  {
    /home/brmsthin/brms-scripts/brms_stop.sh
    echo 'operation finished, press any key to continue.'
    read dumy
}
#------------------------------------------------
kill_brms() {
    /home/brmsthin/brms-scripts/brms_kill.sh
    echo 'operation finished, press any key to continue.'
    read dumy
}
#------------------------------------------------
clean_log_brms() {
    /home/brmsthin/brms-scripts/brms_clean_log.sh
    echo 'operation finished, press any key to continue.'
    read dumy
}
#------------------------------------------------
thin_status()  {
    /home/brmsthin/brms-scripts/brms_status.sh
    echo 'operation finished, press any key to continue.'
    read dumy
}
#------------------------------------------------
update_BPM_DS_password() {
  cd /home/brmsthin/
  java -jar /home/brmsthin/ics-installer-verifier.jar --updateBpmDS
  echo 'operation finished, press any key to continue.'
  read dumy
}
#------------------------------------------------
update_BANKS_DS_password() {
  cd /home/brmsthin/
  java -jar /home/brmsthin/ics-installer-verifier.jar --updateBanksDS
  echo 'operation finished, press any key to continue.'
  read dumy
}
#------------------------------------------------
# start the menu
trap '' 2 3 9 15
trap 'exit' 1
clear=`tput clear`
rev=`tput rev`
bold=`tput bold`
normal=`tput rmso`
###
while [ 1 -eq 1 ]
do
  clear
  touch pmonchecker
  ps -ef >pmonchecker
   echo -e "\f"
   set color -r yellow black
   echo -e "\t\t\c"
   echo -e "$rev  JBOSS BPM SUITE ADMIN MENU  $normal"
   echo -e 
   set color green black
   echo -e "$bold"
   echo -e "\t1- START BPM SERVER"
   echo -e "\t2- STOP BPM SERVER"
   echo -e "\t3- BPM SERVER STATUS"
   echo -e "\t4- FORCE SHUTDOWN BPM SERVER"
   #echo -e "\t5- CLEAN BPM SERVER LOG FILES"
   echo -e "\t5- CHANGE BRMS6 PASSWORD"
   echo -e "\t6- CHANGE BANKWS PASSWORD"
   echo -e "\ts- Exit to command prompt"
   echo -e "\tq- Quit"
   echo -e "\n"
   echo -e "\tEnter your choice ? $normal \c"
   read choice
   case $choice in
     "1")
         start_brms
         continue;;
     "2")
         stop_brms
         continue;;
     "3")
         thin_status
         continue;;
     "4")
         kill_brms
         continue;; 
     #"5")
     #    clean_log_brms
     #    continue;;
     "5")
         update_BPM_DS_password
         continue;;
     "6")
         update_BANKS_DS_password
         continue;;
     "+")
         exit;;
     "s")
         sh
  esac
done
###
echo -e $normal $clear
echo -e "The brmsthin was killed by another proccess ???"
Read answer
exit
