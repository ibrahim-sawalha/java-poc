cd $JBOSS_HOME/bin
echo '---------------------------------------------------------------'

if [ -f status.out ]; then
    rm status.out
fi

./jboss-cli.sh -c --command="read-attribute server-state" >status.out 2>&1

if [ -f status.out ]; then
  while read p; do
    if [[ $p == *"JBAS012174"* ]]
    then
      echo 'WARNING: Server instance "standalone.sh" was not found.'
      echo 'The Server maybe down.'
      ps -u brms |grep standalone.sh
      exit 0
    fi
  done <status.out
fi

echo 'Server Status:'
./jboss-cli.sh -c --command="read-attribute server-state"


