echo '---------------------------------------------------------------'
echo ' '
read -r -p "Are you sure you want to force shutdown? [y/n] " response
  if [[ $response =~ ^(yes|y)$ ]]
  then
    pkill -9 standalone.sh
    echo "standalone.sh processes was destroyed"
  else
    echo "Operation was canceled."
  fi

