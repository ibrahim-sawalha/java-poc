import sys, os, subprocess, getopt

if os.path.exists("/u01/EAP6"):
    raise Exception('/u01/EAP6/ Direcotry was found, it should be deleted.')
    sys.exit(1)

if not os.path.exists("/u01/BPM/ics-bpm6.zip"):
    raise Exception('/u01/BPM/ics-bpm6.zip file was not found.')
    sys.exit(1)

print "Start Extracting /u01/BPM/ics-bpm6.zip file"
subprocess.call("cd /u01/; cp BPM/ics-bpm6.zip .;unzip ics-bpm6.zip;rm ics-bpm6.zip;", shell=True)
print "Finished extracting /u01/BPM/ics-bpm6.zip file"

