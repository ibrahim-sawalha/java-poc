import os
from shutil import copyfile

__author__ = 'Ibrahim.Sawalha'


limitsFile = open('/etc/security/limits.conf', 'r')
updateFile = open('/etc/security/limits.conf', 'w')

copyfile('/etc/security/limits.conf', '/etc/security/limits.conf.backup')


newLines = []
softFound = None
hardFound = None
lineNumber = 0

allLines = limitsFile.read().splitlines()
totalLines = len(allLines)

for line in allLines:
    lineNumber += 1
    if lineNumber == totalLines:
        if not softFound:
            newLines.append("#Auto added by ICSFS");
            newLines.append("brms             soft    nofile          2048")
        if not hardFound:
            newLines.append("#Auto added by ICSFS");
            newLines.append("brms             hard    nofile          63536")
    if line.startswith("brms") and "soft" in line and "nofile" in line:
        softFound = True;
    if line.startswith("brms") and "hard" in line and "nofile" in line:
        hardFound = True;
    newLines.append(line);

print "Adding new banksJVM configuration as following:"

for line in newLines:
    print>> updateFile, line
    print line

limitsFile.close()
updateFile.close()
