import sys, os, subprocess, getopt

serverIp=''
serverPort=''
emailAddress=''
emailPassword=''
SCRIPT_HOME="/home/brmsthin/installer-scripts/"

def usage():
  print("Usage: %s -i <email server address> -p <email server port defualt = 25> -a <user name> -w <password>" % sys.argv[0])
  print("Usage: %s --serverIp <email server address> --serverPort <email server port defualt = 25> --emailAddress <user name> --emailPassword <password>" % sys.argv[0])
  sys.exit(2)

try:
    opts, args = getopt.getopt(sys.argv[1:], "hi:p:a:w:", ["help", "serverIp=", "serverPort=" , "emailAddress=" , "emailPassword="])
except getopt.GetoptError, e:
    print (str(e))
    usage()

for opt, arg in opts:
        if opt in ("-h", "--help"):
          usage()
        if opt in ("-i" , "--serverIp"):
          serverIp=arg
        if opt in ("-p" , "--serverPort"):
          serverPort=arg
        if opt in ("-a" , "--emailAddress"):
          emailAddress=arg
        if opt in ("-w" , "--emailPassword"):
          emailPassword=arg

if not serverIp or not serverPort or not emailAddress or not emailPassword:
  usage()        

print "Create new directory /home/brmsthin/installer-scripts/jbpm-console"
subprocess.call("mkdir /home/brmsthin/installer-scripts/jbpm-console;", shell=True)
print "Copy $JBOSS_HOME/standalone/deployments/jbpm-console.war TO " +SCRIPT_HOME+"jbpm-console.war;"
subprocess.call("cp $JBOSS_HOME/standalone/deployments/jbpm-console.war "+SCRIPT_HOME+"jbpm-console.war;", shell=True)
print "Copy "+SCRIPT_HOME+"jbpm-console.war TO "+SCRIPT_HOME+"jbpm-console;"
subprocess.call("cp "+SCRIPT_HOME+"jbpm-console.war "+SCRIPT_HOME+"jbpm-console;", shell=True)
print "Extracting jbpm-console.war  file into " +SCRIPT_HOME+"jbpm-console"
subprocess.call("cd "+SCRIPT_HOME+"jbpm-console;jar xf jbpm-console.war;", shell=True)
print "Delete "+SCRIPT_HOME+"jbpm-console/jbpm-console.war"
subprocess.call("rm "+SCRIPT_HOME+"jbpm-console/jbpm-console.war;", shell=True)

print "Updating file "+SCRIPT_HOME+"jbpm-console/WEB-INF/classes/META-INF/kie-wb-deployment-descriptor.xml"
configFile = open (SCRIPT_HOME+"jbpm-console/WEB-INF/classes/META-INF/kie-wb-deployment-descriptor.xml" , "r")

allLines = configFile.read().splitlines()
newLines = []

for line in allLines:
  if 'org.jbpm.process.workitem.email.EmailWorkItemHandler' in line:
    #newLine = line.replace("server-ip",serverIp)
    #newLine = newLine.replace("server-port",serverPort)
    #newLine = newLine.replace("email-address",emailAddress)
    #newLine = newLine.replace("email-password",emailPassword)
    #newLines.append(newLine)
    newLine="        <identifier>new org.jbpm.process.workitem.email.EmailWorkItemHandler(\""+serverIp+"\", \""+serverPort+"\", \""+emailAddress+"\", \""+emailPassword+"\")</identifier>"
    newLines.append(newLine)
  else:
    newLines.append(line)

configFile.close()


print "Saving changes into "+SCRIPT_HOME+"jbpm-console/WEB-INF/classes/META-INF/kie-wb-deployment-descriptor.xml"
configFile = open (SCRIPT_HOME+"jbpm-console/WEB-INF/classes/META-INF/kie-wb-deployment-descriptor.xml" , "w")

for line in newLines:
  print>> configFile, line

configFile.close()

print "Wrapping new jbpm-console.war file. "
subprocess.call("cd "+SCRIPT_HOME+"jbpm-console;jar cfM ../jbpm-console.war.new .;", shell=True)
print "Copying "+SCRIPT_HOME+"jbpm-console.war.new $JBOSS_HOME/standalone/deployments/jbpm-console.war"
subprocess.call("cp "+SCRIPT_HOME+"jbpm-console.war.new $JBOSS_HOME/standalone/deployments/jbpm-console.war", shell=True)
