import sys, os, subprocess, getopt, shutil

if not os.path.exists("/u01/EAP6"):
    raise Exception('/u01/EAP6/ Direcotry was not found.')
    sys.exit(1)

if os.path.exists("/u01/BPM/ics-bpm6.zip"):
    print "Deleting /u01/BPM/ics-bpm6.zip file"
    os.remove('/u01/BPM/ics-bpm6.zip')

print "Start Zipping /u01/EAP6/ directory to /u01/BPM/ics-bpm6.zip file"
subprocess.call("cd /u01/;zip -r BPM/ics-bpm6.zip EAP6/;", shell=True)
print "Finished Zipping /u01/EAP6/ directory to /u01/BPM/ics-bpm6.zip file"
