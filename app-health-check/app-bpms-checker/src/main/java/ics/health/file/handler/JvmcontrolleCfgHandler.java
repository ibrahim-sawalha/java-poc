package ics.health.file.handler;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;

public class JvmcontrolleCfgHandler {
    final static Logger log = (Logger) LoggerFactory.getLogger(JvmcontrolleCfgHandler.class);

    public static List<String> DEFAULT_CONFIG = new ArrayList<String>();
    {
        DEFAULT_CONFIG.add("[banksJVM]");
        DEFAULT_CONFIG.add("jvmoptions=-Xms512m -Xmx1024m  -javaagent:/home/thin/banksAgent.jar");
        DEFAULT_CONFIG.add("# Classpath settings given here is an example only. This should be");
        DEFAULT_CONFIG.add("# modified to include the required jar files and should be set in");
        DEFAULT_CONFIG.add("# platform specific manner.");
        DEFAULT_CONFIG.add("classpath=/u01/BEA/as_1/forms/j2ee/frmsrv.jar:/u01/BEA/as_1/jlib/ldapjclnt11.jar:"
                + "/u01/BEA/as_1/jlib/debugger.jar:/u01/BEA/as_1/jlib/ewt3.jar:/u01/BEA/as_1/jlib/share.jar:"
                + "/u01/BEA/as_1/jlib/utj.jar:/u01/BEA/as_1/jlib/zrclient.jar:/u01/BEA/as_1/reports/jlib/rwrun.jar:"
                + "/u01/BEA/as_1/forms/java/frmwebutil.jar:/u01/BEA/as_1/jlib/start_dejvm.jar:"
                + "/u01/BEA/as_1/opmn/lib/optic.jar:/u/oracle/bank/java-obj/brms_proxy.jar:"
                + "/u/oracle/bank/java-obj/worklistjboss_6.jar:/u/oracle/bank/java-obj/formsMail.jar");
        DEFAULT_CONFIG.add("maxsessions=50");
        DEFAULT_CONFIG.add("logdir=/myapps/anapp/log");
        DEFAULT_CONFIG.add("logging=off");
    }

    public JvmcontrolleCfgHandler() {
    }

    public void process(File jvmcontrollerFile, List<String> config) throws IOException {
        log.info("Start processing: " + jvmcontrollerFile.getAbsolutePath());
        if (!jvmcontrollerFile.exists()) {
            throw new IOException("File " + jvmcontrollerFile.getAbsolutePath() + " was not found.");
        }
        log.info("Create backup file" + jvmcontrollerFile.getAbsolutePath() + ".backup");
        FileUtils.copyFile(jvmcontrollerFile, FileUtils.getFile(jvmcontrollerFile.getAbsolutePath() + ".backup"));
        List<String> lines = FileUtils.readLines(jvmcontrollerFile);
        List<String> newLines = new ArrayList<String>();
        boolean banksJVMFound = false;
        for (String line : lines) {
            if (line.trim().startsWith("[") && !line.trim().equals("[banksJVM]")) {
                banksJVMFound = false;
            }
            if (line.startsWith("[banksJVM]")) {
                banksJVMFound = true;
            }
            if (!banksJVMFound) {
                newLines.add(line);
            }
        }
        if (config != null) {
            log.info("Adding configuration lines:\n " + config);
            newLines.addAll(config);
        }
        FileUtils.writeLines(jvmcontrollerFile, newLines, false);
        log.info("Finished processing: " + jvmcontrollerFile.getAbsolutePath());
    }

}
