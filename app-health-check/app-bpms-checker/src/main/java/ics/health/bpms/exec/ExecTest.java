package ics.health.bpms.exec;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.exec.Executor;
import org.apache.commons.exec.PumpStreamHandler;
import org.slf4j.LoggerFactory;
import org.slf4j.event.Level;

import ch.qos.logback.classic.Logger;

public class ExecTest {

    final static Logger log = (Logger) LoggerFactory.getLogger(ExecTest.class);

    public static void main(String[] args) {
        try {
            CommandLine cmdLine = new CommandLine("dir");
            // cmdLine.addArgument("/p");
            // cmdLine.addArgument("/h");
            // cmdLine.addArgument("${file}");
            // HashMap map = new HashMap();
            // map.put("file", new File("invoice.pdf"));
            // cmdLine.setSubstitutionMap(map);

            DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();

            ExecuteWatchdog watchdog = new ExecuteWatchdog(60 * 1000);
            Executor executor = new DefaultExecutor();
            executor.setExitValue(1);
            executor.setWatchdog(watchdog);
            executor.execute(cmdLine, resultHandler);

            // some time later the result handler callback was invoked so we
            // can safely request the exit value
            resultHandler.waitFor();
            // example 2
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            CommandLine commandline = CommandLine.parse("dir");
            DefaultExecutor exec = new DefaultExecutor();
            PumpStreamHandler streamHandler = new PumpStreamHandler(outputStream);
            exec.setStreamHandler(streamHandler);
            exec.execute(commandline);
            System.out.println(outputStream.toString());
            // example 3
            DefaultExecutor executor2 = new DefaultExecutor();
            executor2.setExitValue(0);
            PumpStreamHandler psh = new PumpStreamHandler(new ExecLogHangler(log, Level.DEBUG),
                    new ExecLogHangler(log, Level.ERROR));
            executor2.setStreamHandler(psh);
            CommandLine commandlineDir = CommandLine.parse("test.bat");
            // commandlineDir.addArgument(" /s");
            System.out.println(executor2.execute(commandlineDir));

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecuteException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
