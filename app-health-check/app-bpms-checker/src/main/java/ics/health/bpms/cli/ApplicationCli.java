package ics.health.bpms.cli;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import ch.qos.logback.classic.Logger;
import ics.health.bpms.exec.UpdateLimitsConf;
import ics.health.bpms.validate.DatabaseConnection;
import ics.installer.common.entity.DataBaseEntity;
import ics.installer.common.entity.EntityFactory;
import ics.installer.common.zip.Unzipper;

public class ApplicationCli {

    final static Logger log = (Logger) LoggerFactory.getLogger(ApplicationCli.class);
    private static final Pattern IP_PATTERN = Pattern
            .compile("^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])$");

    private Options options = new Options();

    private String[] args;

    public ApplicationCli(String[] args) throws IOException {
        this.args = args;

        options.addOption("h", "help", false, "show help.");
        options.addOption("u", "db-user", true, "Database user name");
        options.addOption("p", "db-pass", true, "Database password");
        options.addOption("d", "db-url", true, "Database connection URl <Server IP>:<Port>:<SID>");
        options.addOption("j", "jboss-file", true, "JBoss BPM Suite zipped file.");

        options.addOption("l", "limit-conf", false, "Update the /etc/security/limits.conf file");

        File logFile = new File("logback-debug.xml");
        if (!logFile.exists()) {
            String text = IOUtils.toString(this.getClass().getClassLoader().getResourceAsStream("logback-debug.xml"));
            FileUtils.write(logFile, text, Charset.forName("UTF-8"), false);
        }
    }

    public void parse() throws SAXException, IOException {
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = null;
        try {
            cmd = parser.parse(options, args);

            if (cmd.hasOption("h")) {
                help();
            }
            if (cmd.hasOption("l")) {
                log.info("Start Updating /etc/security/limits.conf file");
                UpdateLimitsConf.updateLimitsConf();
                log.info("Finished Updating /etc/security/limits.conf file");
            }
            if (cmd.hasOption("u") && cmd.hasOption("p") && cmd.hasOption("d")) {
                log.info("Start Testing database connection");
                StringTokenizer tokenizer = new StringTokenizer(cmd.getOptionValue("d"), ":");
                String dbUsername = cmd.getOptionValue("u");
                String dbPassword = cmd.getOptionValue("p");
                String dbIpAddress = null;
                String dbPort = null;
                String dbSid = null;

                // if (dbUsername == null || dbUsername.isEmpty() || dbPassword == null ||
                // dbPassword.isEmpty()) {
                // log.error("Database Username/Password are mandatory.");
                // System.exit(1);
                // }

                if (tokenizer.countTokens() == 3) {
                    dbIpAddress = tokenizer.nextToken();
                    if (!IP_PATTERN.matcher(dbIpAddress).matches()) {
                        log.error("Server IP Address is not valid format.");
                        System.exit(1);
                    }
                    dbPort = tokenizer.nextToken();
                    try {
                        Integer.parseInt(dbPort);
                    } catch (NumberFormatException e) {
                        log.error("Database Port is not a number.");
                        System.exit(1);
                    }
                    dbSid = tokenizer.nextToken();
                } else {
                    log.error("Database connection URL format is wrong, it should be <Server-IP>:<Port>:<SID>");
                    System.exit(1);
                }

                DataBaseEntity dbEntity = EntityFactory.buildDataBaseEntity(dbUsername, dbPassword, dbIpAddress, null,
                    null);
                DatabaseConnection dbTest = new DatabaseConnection(dbEntity);
                System.out.println(dbTest.isConnectionValid());
                log.info("Finished Testing database connection");
            }
            if (cmd.hasOption("j")) {
                log.info("Start Unzipping JBoss BPM Suite file");
                if (!FileUtils.getFile("/u01/EAP6/jboss-eap-6.4/").exists()) {
                    Unzipper unZip = new Unzipper();
                    // unZip.unZipIt(cmd.getOptionValue("j"),
                    // "D:/delete/jboss-server-backup/jboss-eap-6.4_6.2_unzip");
                    unZip.unZipIt(cmd.getOptionValue("j"), "/u01/EAP6/jboss-eap-6.4/");
                } else {
                    log.error(
                        "Directory /u01/EAP6/jboss-eap-6.4/ already exists, please make sure this directory does not exist before installation.");
                }
                log.info("Finished Unzipping JBoss BPM Suite file");
            } else {
                help();
            }
        } catch (ParseException e) {
            log.error("Failed to parse comand line properties", e);
            help();
        }
    }

    private void help() {
        HelpFormatter formater = new HelpFormatter();
        StringBuffer header = new StringBuffer("\nICSFS Microsoft Word Processor\n\n");
        StringBuffer footer = new StringBuffer("\nTo enable logging use -Dlogback.configurationFile=logback-debug.xml");
        footer.append("\nNotes:\n");
        footer.append(
            "- Variables should starts with V_ or FF_ and ends with at least one number or it will be ignored\n");
        footer.append(
            "- Table file name starts from character 0 until first \".\" is used as TableID, example T_TEST.something.xml -> tableID = T_TEST");
        formater.printHelp("java -jar [JARNAME]", header.toString(), options, footer.toString(), true);
        System.exit(0);
    }

    public static void main(String[] args) {
        try {
            new ApplicationCli(args).parse();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
