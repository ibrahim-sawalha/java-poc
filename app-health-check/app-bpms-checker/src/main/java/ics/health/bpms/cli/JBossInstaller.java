package ics.health.bpms.cli;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import org.apache.commons.dbutils.ResultSetHandler;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;

public class JBossInstaller {
    final static Logger log = (Logger) LoggerFactory.getLogger(JBossInstaller.class);

    private ResultSetHandler<Object[]> setHandler;

    public JBossInstaller() {
        setHandler = new ResultSetHandler<Object[]>() {
            @Override
            public Object[] handle(ResultSet rs) throws SQLException {
                if (!rs.next()) {
                    return null;
                }

                ResultSetMetaData meta = rs.getMetaData();
                int cols = meta.getColumnCount();
                Object[] result = new Object[cols];

                for (int i = 0; i < cols; i++) {
                    result[i] = rs.getObject(i + 1);
                }
                return result;
            }
        };
    }

}
