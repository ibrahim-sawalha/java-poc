package ics.health.bpms.exec;

import org.apache.commons.exec.LogOutputStream;
import org.slf4j.event.Level;

import ch.qos.logback.classic.Logger;

public class ExecLogHangler extends LogOutputStream {
    private Logger log;

    public ExecLogHangler(Logger log, Level level) {
        super(level.toInt());
        this.log = log;
    }

    @Override
    protected void processLine(String line, int level) {

        log.info(line);
    }
}
