package ics.health.bpms.validate;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;
import ics.installer.common.entity.DataBaseEntity;

public class DatabaseConnection {

    final static Logger log = (Logger) LoggerFactory.getLogger(DatabaseConnection.class);

    public static final String DB_DRIVER = "oracle.jdbc.driver.OracleDriver";
    public static final DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

    private String userName;
    private String password;
    public String dbConnectionUrl = null;

    public DatabaseConnection(DataBaseEntity dbEntity) {
        this(dbEntity.getDbUserName(), dbEntity.getDbPassword(), dbEntity.getDbAddress(), dbEntity.getDbPort(),
                dbEntity.getDbSid());
    }

    private DatabaseConnection(String userName, String password, String ipAddress, String port, String sid) {
        super();
        if (userName == null || userName.isEmpty()) {
            throw new IllegalArgumentException("DB Username was NULL");
        }
        if (password == null || password.isEmpty()) {
            throw new IllegalArgumentException("DB password was NULL");
        }
        if (ipAddress == null || ipAddress.isEmpty()) {
            throw new IllegalArgumentException("DB Address was NULL");
        }
        if (port == null || port.isEmpty()) {
            throw new IllegalArgumentException("DB port was NULL");
        }
        if (sid == null || sid.isEmpty()) {
            throw new IllegalArgumentException("DB SID was NULL");
        }
        this.userName = userName;
        this.password = password;
        dbConnectionUrl = buildDBConnectionURL(ipAddress, port, sid);
    }

    public boolean isConnectionValid() {
        log.info("-------- Starting Oracle JDBC Connection Testing ------");
        try {
            Class.forName(DB_DRIVER);
        } catch (ClassNotFoundException e) {
            log.error("Where is your Oracle JDBC Driver?", e);
            return false;
        }

        log.info("Oracle JDBC Driver Registered!");
        log.info("Connection {} , Password: {}", dbConnectionUrl, password.replaceAll("(?s).", "*"));
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(dbConnectionUrl, userName, password);
        } catch (SQLException e) {
            log.error("Connection Failed! Check output console", e);
            return false;
        }

        if (connection != null) {
            log.info("Database Connection was Successfull.");
            try {
                connection.close();
            } catch (SQLException e) {
                log.error("", e);
            }
        } else {
            log.info("Failed to make connection!");
        }
        log.info("-------- Finish Oracle JDBC Connection Testing ------");
        return true;
    }

    public Connection getDBConnection() {
        Connection dbConnection = null;
        try {
            Class.forName(DB_DRIVER);
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }
        try {
            dbConnection = DriverManager.getConnection(dbConnectionUrl, userName, password);
            return dbConnection;

        } catch (SQLException e) {
            log.error("", e);
        }
        return dbConnection;
    }

    private static String buildDBConnectionURL(String address, String port, String sid) {
        return "jdbc:oracle:thin:@" + address + ":" + port + ":" + sid;
    }

    private static String getCurrentTimeStamp() {
        java.util.Date today = new java.util.Date();
        return dateFormat.format(today.getTime());
    }

}
