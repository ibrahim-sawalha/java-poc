package ics.health.bpms.exec;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;

public class UpdateLimitsConf {

    final static Logger log = (Logger) LoggerFactory.getLogger(UpdateLimitsConf.class);
    private static String brmsSoft = "brms             soft    nofile          2048";
    private static String brmsHard = "brms             hard    nofile          63536";

    public static void main(String[] args) {
        // try {
        // updateLimitsConf("D:\\Developer\\POC\\java-poc\\app-health-check\\app-bpms-checker\\limits.conf");
        // } catch (IOException e) {
        // e.printStackTrace();
        // }
        updateLimitsConf();
    }

    public static void updateLimitsConf() {
        log.info("Process updating limits.conf");
        if (SystemUtils.IS_OS_LINUX) {
            try {
                updateLimitsConf("/etc/security/limits.conf");
            } catch (IOException e) {
                if (e.getMessage().contains("Permission denied")) {
                    log.error("Permission denied, cannot access file [/etc/security/limits.conf], "
                            + "try to login using root user.");
                } else {
                    log.error("", e);
                }
            }

        } else {
            log.error("The operating system is not LINUX!");
        }
        log.info("Finished updating limits.conf");
    }

    private static void updateLimitsConf(String fileName) throws IOException {
        File file = FileUtils.getFile(fileName);
        File backupFile = FileUtils.getFile(file.getParentFile() + File.separator + file.getName()
                + Calendar.getInstance().getTimeInMillis() + ".back");
        // try {
        if (!backupFile.exists()) {
            backupFile.createNewFile();
        }
        FileUtils.copyFile(file, backupFile, false);
        List<String> lines = FileUtils.readLines(file);
        List<String> newLines = new ArrayList<String>();
        boolean softFound = false;
        boolean hardFound = false;
        int lineNumber = 0;
        for (String line : lines) {
            ++lineNumber;
            String out = line;
            // if (out.trim().equals("# End of file")) {
            if (lineNumber == lines.size()) { // reach last line
                if (!softFound) {
                    log.info("[brms soft nofile ...] was not found and will be added.");
                    newLines.add("#Auto added by ICSFS");
                    newLines.add(brmsSoft);
                }
                if (!hardFound) {
                    log.info("[brms hard nofile ...] was not found and will be added.");
                    newLines.add("#Auto added by ICSFS");
                    newLines.add(brmsHard);
                }
            }

            if (out.trim().startsWith("brms") && out.contains("soft") && out.contains("nofile")) {
                out = StringUtils.replace(out, "\t", " "); // replace tabs with space
                out = out.trim().replaceAll("\\s+", " "); // remove all duplicated white spaces
                log.info("[{}] was found and will be replaced by [brms hard nofile 2048]", out);
                out = brmsSoft;
                softFound = true;
            } else if (out.trim().startsWith("brms") && out.contains("hard") && out.contains("nofile")) {
                out = StringUtils.replace(out, "\t", " ");// replace tabs with space
                out = line.trim().replaceAll("\\s+", " ");// remove all duplicated white spaces
                log.info("[{}] was found and will be replaced by [brms hard nofile 63536]", out);
                out = brmsHard;
                hardFound = true;
            }
            newLines.add(out);
        }

        FileUtils.writeLines(file, "UTF-8", newLines);
        // } catch (IOException e) {
        // log.error("", e);
        // }
    }

}
