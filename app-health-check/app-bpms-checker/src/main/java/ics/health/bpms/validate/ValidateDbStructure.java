package ics.health.bpms.validate;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Scanner;

public class ValidateDbStructure {

    private DatabaseConnection dbSchema;

    public ValidateDbStructure(DatabaseConnection dbSchema) {
        this.dbSchema = dbSchema;
    }

    public void runScript() throws IOException, SQLException {
        String content = new Scanner(new File("d:\\delete\\BPM_INIT.sql")).next();
        System.out.println(content);

        String sql = readFile("d:\\delete\\BPM_INIT.sql", StandardCharsets.UTF_8);

        Connection conn = dbSchema.getDBConnection();
        CallableStatement cs = conn.prepareCall(sql);
        cs.execute();
        cs.close();
        conn.close();
    }

    static String readFile(String path, Charset encoding) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }

}
