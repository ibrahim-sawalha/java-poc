package ics.health.bpms.cli;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;
import ics.health.bpms.validate.DatabaseConnection;

public class WeblogicInstaller {
    final static Logger log = (Logger) LoggerFactory.getLogger(WeblogicInstaller.class);

    private ResultSetHandler<Object[]> setHandler;
    private DatabaseConnection db;

    public WeblogicInstaller(DatabaseConnection db) {
        if (db == null) {
            log.error("Database connection was null.");
            throw new IllegalArgumentException("Database connection should be valid");
        }
        this.db = db;
        setHandler = new ResultSetHandler<Object[]>() {
            @Override
            public Object[] handle(ResultSet rs) throws SQLException {
                if (!rs.next()) {
                    return null;
                }

                ResultSetMetaData meta = rs.getMetaData();
                int cols = meta.getColumnCount();
                Object[] result = new Object[cols];

                for (int i = 0; i < cols; i++) {
                    result[i] = rs.getObject(i + 1);
                }
                return result;
            }
        };
    }

    public boolean isBRMSSchemaValid(String dbAddress, String dbUserName, String dbPassword, String dbSid) {
        if (isBRMS6UserExist()) {
            log.info("BRMS6 User was found. It will not be created.");
            return true;
        } else {
            log.warn("BRMS6 User was not found. It will be created.");
            return false;
        }
    }

    public boolean isBRMS6UserExist() {
        Connection conn = db.getDBConnection();
        QueryRunner run = new QueryRunner();
        try {
            Object[] result = run.query(conn, "SELECT \'BRMS6 USER EXISTS\' FROM all_users WHERE username = \'BRMS6\'",
                setHandler);
            if (result != null && result.length > 0) {
                return true;
            } else {
                log.warn("BRMS6 User was not found. It will be created.");
                return false;
            }
        } catch (SQLException e) {
            log.error("{}", e);
        } finally {
            try {
                DbUtils.close(conn);
            } catch (SQLException e) {
                log.error("{}", e);
            }
        }
        return false;
    }

    public boolean isDmsDirectoryExist() {
        Connection conn = db.getDBConnection();
        QueryRunner run = new QueryRunner();
        try {
            Object[] result = run.query(conn, "select 1 from all_directories where DIRECTORY_NAME = 'DMS'", setHandler);
            if (result != null && result.length > 0) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            log.error("{}", e);
        } finally {
            try {
                DbUtils.close(conn);
            } catch (SQLException e) {
                log.error("{}", e);
            }
        }
        return false;
    }

}
