package app.installer.exec.command;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.slf4j.LoggerFactory;

import app.installer.exec.argument.PythonCommandArgumentImpl;
import app.installer.exec.argument.ScriptArgument;
import app.installer.exec.command.common.AbstractCommand;
import ch.qos.logback.classic.Logger;

public class PythonCommand extends AbstractCommand {

    final static Logger log = (Logger) LoggerFactory.getLogger(PythonCommand.class);
    private AbstractCommand command;

    public PythonCommand() {
        super();
    }

    public void runPythonScriptCommand(PythonCommandArgumentImpl argument) {
        File shellFile = FileUtils.getFile(argument.getExecutable());
        if (shellFile.exists()) {
            if (!shellFile.canExecute()) {
                shellFile.setExecutable(true);
            }
            if (argument.getScriptArguments() != null) {
                runCommand(log, "python " + argument.getExecutable() + " " + argument.getScriptArguments());
            } else {
                runCommand(log, "python " + argument.getExecutable());
            }
        } else {
            log.error("pythonScriptFile does not exist in [" + argument.getExecutable() + "]");
            if (argument.isExitOnFailure()) {
                throw new RuntimeException("pythonScriptFile does not exist in [" + argument.getExecutable() + "]");
            }
        }
    }

    @Override
    public void process(ScriptArgument argument) {
        log.info("Start processing " + argument.getExecutable());
        if (argument instanceof PythonCommandArgumentImpl) {
            runPythonScriptCommand((PythonCommandArgumentImpl) argument);
            log.info("Finished processing " + argument.getExecutable());
        } else if (command != null) {
            command.process(argument);
        }
    }

    @Override
    public void setNextCommand(AbstractCommand command) {
        this.command = command;
    }
}
