package app.installer.exec.command;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.slf4j.LoggerFactory;

import app.installer.exec.argument.ScriptArgument;
import app.installer.exec.argument.ShellCommandArgumentImpl;
import app.installer.exec.command.common.AbstractCommand;
import ch.qos.logback.classic.Logger;

public class ShellCommand extends AbstractCommand {
    final static Logger log = (Logger) LoggerFactory.getLogger(ShellCommand.class);
    private AbstractCommand nextCommand;

    public ShellCommand() {
        super();
    }

    public void runShellScriptCommand(ShellCommandArgumentImpl argument) {
        if (!argument.isValid()) {
            log.error("Cannot execute shell nextCommand [" + argument.getExecutable() + "]");
            if (argument.isExitOnFailure()) {
                throw new RuntimeException("Cannot execute shell nextCommand [" + argument.getExecutable() + "]");
            }
        }
        File shellFile = FileUtils.getFile(argument.getExecutable());
        if (shellFile.exists()) {
            if (!shellFile.canExecute()) {
                shellFile.setExecutable(true);
            }
            if (argument.getScriptArguments() != null) {
                runCommand(log, argument.getExecutable() + " " + argument.getScriptArguments());
            } else {
                runCommand(log, argument.getExecutable());
            }
        } else {
            log.error("shellScriptFile does not exist in [" + argument.getExecutable() + "]");
            if (argument.isExitOnFailure()) {
                throw new RuntimeException("shellScriptFile does not exist in [" + argument.getExecutable() + "]");
            }
        }
    }

    @Override
    public void process(ScriptArgument argument) {
        log.info("Start processing " + argument.getExecutable());
        if (argument instanceof ShellCommandArgumentImpl) {
            runShellScriptCommand((ShellCommandArgumentImpl) argument);
            log.info("Finished processing " + argument.getExecutable());
        } else if (nextCommand != null) {
            nextCommand.process(argument);
        }
    }

    @Override
    public void setNextCommand(AbstractCommand command) {
        this.nextCommand = command;
    }

}
