package app.installer.exec.argument;

import java.io.Serializable;

public abstract class AbstractScriptArgument implements ScriptArgument, Serializable {
    private static final long serialVersionUID = -3501977524447611075L;

    protected String executableCommand;
    private boolean exitOnFailure = false;
    private boolean backupAndRestoreOnFailure = false;
    protected String commandType;

    public AbstractScriptArgument() {
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("AbstractScriptArgument [executableCommand=");
        builder.append(executableCommand);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public boolean isExitOnFailure() {
        return exitOnFailure;
    }

    @Override
    public void setExitOnFailure(boolean exitOnFailure) {
        this.exitOnFailure = exitOnFailure;
    }

    @Override
    public boolean isBackupAndRestoreOnFailure() {
        return backupAndRestoreOnFailure;
    }

    @Override
    public void setBackupAndRestoreOnFailure(boolean backupAndRestoreOnFailure) {
        this.backupAndRestoreOnFailure = backupAndRestoreOnFailure;
    }

    @Override
    public String getCommandType() {
        return commandType;
    }

    @Override
    public void setCommandType(String commandType) {
        this.commandType = commandType;
    }

    @Override
    public String getExecutable() {
        return executableCommand;
    }

    @Override
    public void setExecutable(String scriptCommand) {
        this.executableCommand = scriptCommand;
    }

}
