package app.installer.exec.argument;

public class JBossCommandArgumentImpl extends AbstractScriptArgument implements ScriptArgument {

    private String scriptArguments;

    protected JBossCommandArgumentImpl() {
    }

    public JBossCommandArgumentImpl(String scriptCommand) {
        setExecutable(scriptCommand);
    }

    @Override
    public boolean isValid() {
        if (getExecutable() == null || getExecutable().isEmpty()) {
            return false;
        }
        return true;
    }

    @Override
    public String getScriptArguments() {
        return scriptArguments;
    }

    @Override
    public void setScriptArguments(String scriptArguments) {
        this.scriptArguments = scriptArguments;
    }

    @Override
    public String getExecutable() {
        return executableCommand;
    }

    @Override
    public void setExecutable(String scriptCommand) {
        this.executableCommand = scriptCommand;
    }

    @Override
    public ScriptArgument getBackupCommand() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ScriptArgument getRestoreCommand() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setBackupCommand(ScriptArgument x) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setRestoreCommand(ScriptArgument x) {
        // TODO Auto-generated method stub

    }

    @Override
    public String getCommandType() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setCommandType(String commandType) {
        // TODO Auto-generated method stub

    }

}
