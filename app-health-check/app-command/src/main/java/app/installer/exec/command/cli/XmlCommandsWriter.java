package app.installer.exec.command.cli;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.commons.io.FileUtils;

import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.Serializer;
import net.sf.saxon.s9api.Serializer.Property;

public class XmlCommandsWriter {

    private List<Object> commands = new ArrayList<Object>();

    public static void main(String[] args) {

        try {
            StringWriter stringWriter = new StringWriter();

            net.sf.saxon.s9api.Processor p = new net.sf.saxon.s9api.Processor(true);
            Serializer s = p.newSerializer();
            s.setOutputProperty(Property.METHOD, "xml");
            s.setOutputProperty(Property.INDENT, "yes");
            s.setOutputStream(System.out);
            s.setOutputWriter(stringWriter);
            // s.setOutputFile(FileUtils.getFile("c:\\out.xml"));
            XMLStreamWriter xMLStreamWriter = s.getXMLStreamWriter();

            xMLStreamWriter.writeStartDocument();

            xMLStreamWriter.writeStartElement("commands");

            // command 1
            xMLStreamWriter.writeStartElement("command");
            xMLStreamWriter.writeAttribute("type", "wlst");
            xMLStreamWriter.writeStartElement("executable");
            xMLStreamWriter.writeCharacters("/home/thin/ibrahim-scripts/weblogic_scripts/serverAdminState.py");
            xMLStreamWriter.writeEndElement();// end executable
            xMLStreamWriter.writeStartElement("logFile");
            xMLStreamWriter.writeCharacters("log-file.log");
            xMLStreamWriter.writeEndElement();// end executable
            xMLStreamWriter.writeStartElement("arguments");
            xMLStreamWriter.writeCharacters("-f file -t test -x xmlfile -y yes");
            xMLStreamWriter.writeEndElement();// end executable
            xMLStreamWriter.writeEndElement();// end command

            xMLStreamWriter.writeEndElement();
            xMLStreamWriter.writeEndDocument(); // end document

            xMLStreamWriter.flush();
            xMLStreamWriter.close();

            String xmlString = stringWriter.getBuffer().toString();

            System.out.println(xmlString);
            FileUtils.writeStringToFile(FileUtils.getFile("c:\\out.xml"), stringWriter.getBuffer().toString(), false);

            stringWriter.close();

        } catch (XMLStreamException e) {
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SaxonApiException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
}
