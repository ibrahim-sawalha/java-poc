package app.installer.exec.argument;

import java.io.Serializable;

import ics.installer.common.entity.DataBaseEntity;

public class SQLScriptCommandArgumentImpl extends AbstractScriptArgument implements Serializable {
    private static final long serialVersionUID = 1L;

    // private String dbUserName;
    // private String dbPassword;

    private DataBaseEntity dataBaseEntity;

    @Override
    public String getExecutable() {
        return executableCommand;
    }

    @Override
    public void setExecutable(String scriptCommand) {
        this.executableCommand = scriptCommand;
    }

    public SQLScriptCommandArgumentImpl(String scriptCommand) {
        setExecutable(scriptCommand);
    }

    // public String getDbUserName() {
    // return dbUserName;
    // }
    //
    // public SQLScriptCommandArgumentImpl setDbUserName(String dbUserName) {
    // this.dbUserName = dbUserName;
    // return this;
    // }
    //
    // public String getDbPassword() {
    // return dbPassword;
    // }
    //
    // public SQLScriptCommandArgumentImpl setDbPassword(String dbPassword) {
    // this.dbPassword = dbPassword;
    // return this;
    // }

    @Override
    public boolean isValid() {
        if (/*
             * dbUserName == null || dbPassword == null || dbUserName.isEmpty() || dbPassword.isEmpty()
             * ||
             */ getExecutable() == null || getExecutable().isEmpty()) {
            return false;
        }
        return true;
    }

    @Override
    public ScriptArgument getBackupCommand() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ScriptArgument getRestoreCommand() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setBackupCommand(ScriptArgument x) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setRestoreCommand(ScriptArgument x) {
        // TODO Auto-generated method stub

    }

    @Override
    public String getScriptArguments() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setScriptArguments(String scriptArguments) {
        // TODO Auto-generated method stub

    }

    @Override
    public String getCommandType() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setCommandType(String commandType) {
        // TODO Auto-generated method stub

    }

    public DataBaseEntity getDataBaseEntity() {
        return dataBaseEntity;
    }

    public void setDataBaseEntity(DataBaseEntity dataBaseEntity) {
        this.dataBaseEntity = dataBaseEntity;
    }

}
