package app.installer.exec.argument;

public class WeblogicCommandArgumentImpl extends ShellCommandArgumentImpl {

    protected WeblogicCommandArgumentImpl() {
        super();
    }

    public WeblogicCommandArgumentImpl(String scriptCommand) {
        super(scriptCommand);
    }

}
