package app.installer.exec.command.common;

import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.ExecuteResultHandler;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;

public class IbrahimExecuteResultHandler extends DefaultExecuteResultHandler implements ExecuteResultHandler {

    final static Logger log = (Logger) LoggerFactory.getLogger(IbrahimExecuteResultHandler.class);
    private IbrahimCommandLine cmdLine;
    private boolean processComplete;

    public IbrahimExecuteResultHandler(IbrahimCommandLine cmdLine) {
        this.cmdLine = cmdLine;
    }

    @Override
    public void onProcessComplete(int exitValue) {
        super.onProcessComplete(exitValue);
        processComplete = true;
        log.debug("ProcessComplete [" + cmdLine.toString() + "] result [" + exitValue + "]");
    }

    @Override
    public void onProcessFailed(ExecuteException e) {
        super.onProcessFailed(e);
        processComplete = false;
        log.error("ProcessFailed [" + cmdLine.toString() + "]", e);
    }

    public boolean isProcessComplete() {
        return processComplete;
    }

}
