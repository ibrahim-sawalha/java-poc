package app.installer.exec.command.common;

import java.util.ArrayList;
import java.util.List;

public class ExecResultsHandler {
    private boolean execSuccess = true;
    List<String> outLines;

    public ExecResultsHandler() {
        outLines = new ArrayList<String>();
    }

    public void addLine(String line) {
        if (line != null && line.toUpperCase().contains("ERROR")) {
            execSuccess = false;
        }
        outLines.add(line);
    }

    public boolean isExecutionSuccess() {
        return execSuccess;
    }

    public void setExecutionSuccess(boolean execSuccess) {
        this.execSuccess = execSuccess;
    }

    public List<String> getExecutionOutput() {
        return outLines;
    }
}
