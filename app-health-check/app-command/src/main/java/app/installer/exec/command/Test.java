package app.installer.exec.command;

import java.util.List;

import org.apache.commons.io.FileUtils;
import org.slf4j.LoggerFactory;

import app.installer.exec.argument.ScriptArgument;
import app.installer.exec.command.cli.XmlCommandsParser;
import ch.qos.logback.classic.Logger;

public class Test {
    final static Logger log = (Logger) LoggerFactory.getLogger(Test.class);

    public static void main(String[] args) {
        try {
            List<ScriptArgument> argumentList = new XmlCommandsParser().run(FileUtils.getFile(args[0]));

            SQLScriptCommand sql = new SQLScriptCommand();
            ShellCommand shell = new ShellCommand();
            WeblogicCommand weblogic = new WeblogicCommand();

            sql.setWLSTCommand("/u01/BEA/oracle_common/common/bin/wlst.sh");
            sql.setJBossCommand("/u01/EAP6/jboss-eap-6.4/bin/jboss-cli.sh");
            weblogic.setWLSTCommand("/u01/BEA/oracle_common/common/bin/wlst.sh");
            weblogic.setJBossCommand("/u01/EAP6/jboss-eap-6.4/bin/jboss-cli.sh");

            sql.setNextCommand(weblogic);
            weblogic.setNextCommand(shell);
            shell.setNextCommand(null);

            for (ScriptArgument arg : argumentList) {
                sql.process(arg);
            }

        } catch (RuntimeException e) {
            log.error("", e);
        }

    }

}
