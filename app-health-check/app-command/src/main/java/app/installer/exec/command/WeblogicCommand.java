package app.installer.exec.command;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.slf4j.LoggerFactory;

import app.installer.exec.argument.ScriptArgument;
import app.installer.exec.argument.WeblogicCommandArgumentImpl;
import app.installer.exec.command.common.AbstractCommand;
import ch.qos.logback.classic.Logger;

public class WeblogicCommand extends AbstractCommand {

    final static Logger log = (Logger) LoggerFactory.getLogger(WeblogicCommand.class);
    private AbstractCommand command;

    public WeblogicCommand() {
        super();
    }

    public void runWeblogicScriptCommand(WeblogicCommandArgumentImpl argument) {
        File wlstFile = FileUtils.getFile("/u01/BEA/oracle_common/common/bin/wlst.sh");
        if (!wlstFile.exists()) {
            log.error("wlst.sh file does not exist on the following path:" + wlstFile.getAbsolutePath());
            if (argument.isExitOnFailure()) {
                throw new RuntimeException(
                        "wlst.sh file does not exist on the following path:" + wlstFile.getAbsolutePath());
            }
            return;
        }

        setWLSTCommand("/u01/BEA/oracle_common/common/bin/wlst.sh");

        File shellFile = FileUtils.getFile(argument.getExecutable());
        if (shellFile.exists()) {
            if (argument.getScriptArguments() != null) {
                runWeblogicScript(log, argument.getExecutable() + " " + argument.getScriptArguments());
            } else {
                runWeblogicScript(log, argument.getExecutable());
            }
        } else {
            log.error("weblogicScriptFile does not exist in [" + argument.getExecutable() + "]");
            if (argument.isExitOnFailure()) {
                throw new RuntimeException("weblogicScriptFile does not exist in [" + argument.getExecutable() + "]");
            }
            return;
        }
    }

    @Override
    public void process(ScriptArgument argument) {
        if (argument instanceof WeblogicCommandArgumentImpl) {
            runWeblogicScriptCommand((WeblogicCommandArgumentImpl) argument);
        } else if (command != null) {
            command.process(argument);
        }
    }

    @Override
    public void setNextCommand(AbstractCommand command) {
        this.command = command;
    }
}
