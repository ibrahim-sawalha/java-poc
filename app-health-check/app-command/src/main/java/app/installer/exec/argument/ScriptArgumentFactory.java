package app.installer.exec.argument;

public class ScriptArgumentFactory {

    private ScriptArgumentFactory() {
    }

    private static ScriptArgumentFactory instance;

    public static ScriptArgumentFactory getFactory() {
        if (instance == null) {
            instance = new ScriptArgumentFactory();
        }
        return instance;
    }

    public SQLScriptCommandArgumentImpl makeSqlScriptCommand(String scriptCommand) {
        return new SQLScriptCommandArgumentImpl(scriptCommand);
    }

    public ShellCommandArgumentImpl makeShellScriptCommand(String scriptCommand) {
        return new ShellCommandArgumentImpl(scriptCommand);
    }

    public WeblogicCommandArgumentImpl makeWeblogicScriptCommand(String scriptCommand) {
        return new WeblogicCommandArgumentImpl(scriptCommand);
    }

}
