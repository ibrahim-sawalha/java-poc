package app.installer.exec.command.common;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.exec.PumpStreamHandler;
import org.slf4j.LoggerFactory;
import org.slf4j.event.Level;

import app.installer.exec.argument.ScriptArgument;
import ch.qos.logback.classic.Logger;

public abstract class AbstractCommand {

    final static Logger log = (Logger) LoggerFactory.getLogger(AbstractCommand.class);
    private String wlstCommand = null;
    private String jbossCommand = null;

    public AbstractCommand() {
    }

    abstract public void process(ScriptArgument argument);

    abstract public void setNextCommand(AbstractCommand command);

    public void setWLSTCommand(String wlstCommand) {
        this.wlstCommand = wlstCommand;
    }

    public void setJBossCommand(String jbossCommand) {
        this.jbossCommand = jbossCommand;
    }

    protected void runPython(Logger log, String command) {
        if (command == null || command.isEmpty()) {
            throw new RuntimeException("Command Cannot be null");
        }
        IbrahimCommandLine cmdLine = new IbrahimCommandLine("python");

        StringTokenizer token = new StringTokenizer(command, " ");
        if (token.countTokens() > 1) {
            while (token.hasMoreElements()) {
                String val = (String) token.nextElement();
                cmdLine.addArgument(val);
            }
        }
        execute(cmdLine);
    }

    protected void runWeblogicScript(Logger log, String command) {
        if (command == null || command.isEmpty()) {
            log.error("Command Cannot be null");
            throw new RuntimeException("Command Cannot be null");
        }
        if (wlstCommand == null || wlstCommand.isEmpty()) {
            log.error("wlst.sh Command Path was not defined");
            throw new RuntimeException("Command Cannot be null");
        }

        IbrahimCommandLine cmdLine = new IbrahimCommandLine(wlstCommand);

        StringTokenizer token = new StringTokenizer(command, " ");
        if (token.countTokens() > 1) {
            while (token.hasMoreElements()) {
                String val = (String) token.nextElement();
                cmdLine.addArgument(val);
            }
        } else {
            cmdLine.addArgument(command);
        }
        execute(cmdLine);
    }

    protected void runJBossScript(Logger log, String command) {
        if (command == null || command.isEmpty()) {
            log.error("Command Cannot be null");
            throw new RuntimeException("Command Cannot be null");
        }
        if (jbossCommand == null || jbossCommand.isEmpty()) {
            log.error("jboss-cli.sh Command Path was not defined");
            throw new RuntimeException("Command Cannot be null");
        }

        IbrahimCommandLine cmdLine = new IbrahimCommandLine(jbossCommand);
        cmdLine.addArgument("--file=" + command);

        execute(cmdLine);
    }

    protected ExecResultsHandler runCommand(Logger log, String command) {
        if (command == null || command.isEmpty()) {
            throw new RuntimeException("Command Cannot be null");
        }
        IbrahimCommandLine cmdLine = new IbrahimCommandLine(command);

        StringTokenizer token = new StringTokenizer(command, " ");
        if (token.countTokens() > 1) {
            int i = 0;
            while (token.hasMoreElements()) {
                String val = (String) token.nextElement();
                if (i > 0) {
                    cmdLine.addArgument(val);
                } else {
                    cmdLine = new IbrahimCommandLine(val);
                }
                ++i;
            }
        }
        return execute(cmdLine);
    }

    private ExecResultsHandler execute(IbrahimCommandLine cmdLine) {
        log.info("Executing Command: {}", cmdLine);
        IbrahimExecuteResultHandler resultHandler = new IbrahimExecuteResultHandler(cmdLine);

        ExecuteWatchdog watchDog = new ExecuteWatchdog(60 * 1000);
        DefaultExecutor executor = new DefaultExecutor();
        executor.setWatchdog(watchDog);
        executor.setExitValue(0);
        ExecInfoLogHandler infoLogHandler = new ExecInfoLogHandler(log, Level.ERROR);
        ExecErrorLogHandler errorLogHandler = new ExecErrorLogHandler(log, Level.ERROR);
        PumpStreamHandler psh = new PumpStreamHandler(infoLogHandler, errorLogHandler);
        executor.setStreamHandler(psh);

        if (watchDog.killedProcess()) {
            log.info("{}", "Process Killed");
        }
        try {
            ExecResultsHandler results = new ExecResultsHandler();

            executor.execute(cmdLine, resultHandler);
            while (!resultHandler.hasResult()) {
                try {
                    resultHandler.waitFor();
                } catch (InterruptedException e) {
                }
            }
            if (!resultHandler.isProcessComplete()) {
                results.setExecutionSuccess(false);
            }
            return results;
        } catch (ExecuteException e) {
            log.error("{}", e);
        } catch (IOException e) {
            log.error("{}", e);
        }
        return null;
    }

}
