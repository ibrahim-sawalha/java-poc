package app.installer.exec.command.cli;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.apache.commons.io.FileUtils;
import org.slf4j.LoggerFactory;

import app.installer.exec.argument.SQLScriptCommandArgumentImpl;
import app.installer.exec.argument.ScriptArgument;
import app.installer.exec.argument.ShellCommandArgumentImpl;
import app.installer.exec.argument.WeblogicCommandArgumentImpl;
import ch.qos.logback.classic.Logger;
import ics.installer.common.entity.DataBaseEntity;
import ics.installer.common.entity.EntityFactory;

public class XmlCommandsParser {
    final static Logger log = (Logger) LoggerFactory.getLogger(XmlCommandsParser.class);
    private List<ScriptArgument> commands;
    private String sCommandType;
    private String data;
    private String sExecutable = null;
    private String sArguments = null;
    private String sLogFile = null;
    private String sJdbc;

    public static void main(String[] args) {
        new XmlCommandsParser().run(FileUtils.getFile("c:\\out.xml"));
    }

    public List<ScriptArgument> run(File xmlFile) {
        try {

            XMLInputFactory factory = XMLInputFactory.newInstance();
            XMLEventReader eventReader = factory.createXMLEventReader(new FileReader(xmlFile));

            while (eventReader.hasNext()) {
                XMLEvent event = eventReader.nextEvent();
                switch (event.getEventType()) {

                case XMLStreamConstants.START_ELEMENT:
                    StartElement startElement = event.asStartElement();
                    String qName = startElement.getName().getLocalPart();
                    Iterator<Attribute> attributes = startElement.getAttributes();
                    if (qName.equalsIgnoreCase("commands")) {
                        commands = new ArrayList<ScriptArgument>();
                    } else if (qName.equalsIgnoreCase("command")) {
                        sCommandType = attributes.next().getValue();
                    }
                    break;

                case XMLStreamConstants.CHARACTERS:
                    data = event.asCharacters().getData();
                    break;

                case XMLStreamConstants.END_ELEMENT:
                    EndElement endElement = event.asEndElement();
                    if (endElement.getName().getLocalPart().equals("executable")) {
                        sExecutable = data;
                    } else if (endElement.getName().getLocalPart().equals("arguments")) {
                        sArguments = data;
                    } else if (endElement.getName().getLocalPart().equals("logFile")) {
                        sLogFile = data;
                    } else if (endElement.getName().getLocalPart().equals("jdbc")) {
                        sJdbc = data;
                    }

                    if (endElement.getName().getLocalPart().equalsIgnoreCase("command")) {
                        if (sCommandType.equals("wlst")) {
                            ScriptArgument sCommand = new WeblogicCommandArgumentImpl(sExecutable);
                            sCommand.setScriptArguments(sArguments);
                            log.info("Weblogic Script Found " + sArguments + " " + sExecutable + " " + sLogFile);
                            commands.add(sCommand);
                        } else if (sCommandType.equals("shell")) {
                            ScriptArgument sCommand = new ShellCommandArgumentImpl(sExecutable);
                            sCommand.setScriptArguments(sArguments);
                            log.info("SHELL Script Found " + sArguments + " " + sExecutable + " " + sLogFile);
                            commands.add(sCommand);
                        } else if (sCommandType.equals("sql")) {
                            SQLScriptCommandArgumentImpl sCommand = new SQLScriptCommandArgumentImpl(sExecutable);
                            StringTokenizer str = new StringTokenizer(sJdbc, "/");
                            String dbUserName = str.nextToken();
                            String dbPassword = str.nextToken();
                            DataBaseEntity e = EntityFactory.buildDataBaseEntity(dbUserName, dbPassword, "localhost",
                                "HOBANK", "1521");
                            // sCommand.setDbUserName(str.nextToken());
                            // sCommand.setDbPassword(str.nextToken());
                            sCommand.setScriptArguments(sArguments);
                            sCommand.setDataBaseEntity(e);
                            ;
                            log.info("SQL Script Found " + sArguments + " " + sExecutable + " " + sLogFile);
                            commands.add(sCommand);
                        }
                    }
                    break;
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
        return commands;
    }
}
