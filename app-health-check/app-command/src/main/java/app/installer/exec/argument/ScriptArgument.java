package app.installer.exec.argument;

import java.io.Serializable;

public interface ScriptArgument extends Serializable {

    abstract public boolean isValid();

    abstract public String getExecutable();

    public void setExecutable(String scriptCommand);

    abstract public ScriptArgument getBackupCommand();

    abstract public ScriptArgument getRestoreCommand();

    abstract public void setBackupCommand(ScriptArgument x);

    abstract public void setRestoreCommand(ScriptArgument x);

    abstract public String getScriptArguments();

    abstract public void setScriptArguments(String scriptArguments);

    public boolean isBackupAndRestoreOnFailure();

    abstract public void setBackupAndRestoreOnFailure(boolean backupAndRestoreOnFailure);

    public String getCommandType();

    public void setCommandType(String commandType);

    public boolean isExitOnFailure();

    public void setExitOnFailure(boolean exitOnFailure);

}
