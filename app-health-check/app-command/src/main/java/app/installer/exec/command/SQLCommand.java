package app.installer.exec.command;

import org.slf4j.LoggerFactory;

import app.installer.exec.argument.SQLCommandArgumentImpl;
import app.installer.exec.argument.ScriptArgument;
import app.installer.exec.command.common.AbstractCommand;
import ch.qos.logback.classic.Logger;

public class SQLCommand extends AbstractCommand {

    final static Logger log = (Logger) LoggerFactory.getLogger(SQLCommand.class);
    private AbstractCommand command;

    public SQLCommand() {
        super();
    }

    public void runSqlCommand(SQLCommandArgumentImpl command) {
        throw new UnsupportedOperationException();
        // if (!command.isValid()) {
        // log.error("Cannot connect to database using Native SQL command. userName=[" +
        // command.getDbUserName()
        // + "] password=[" + command.getDbPassword() + "], SQL Command=[" + command.getExecutable() + "]");
        // if (command.isExitOnFailure()) {
        // throw new RuntimeException("sqlFile have errors, please review log files.");
        // }
        // }

        // StringBuffer sql = new StringBuffer("sqlplus");
        // sql.append(" ");
        // sql.append(command.getDbUserName());
        // sql.append("/");
        // sql.append(command.getDbPassword());
        // sql.append(" @ ");
        // if (FileUtils.getFile(command.getExecutable()).exists()) {
        // ExecResultsHandler execResult = runCommand(log, sql.toString() + command.getExecutable());
        // if (!execResult.isExecutionSuccess() && command.isExitOnFailure()) {
        // throw new RuntimeException("sqlFile have errors, please review log files.");
        // }
        // } else {
        // log.error("sqlFile does not exist in [" + command.getExecutable() + "]");
        // if (command.isExitOnFailure()) {
        // throw new RuntimeException("sqlFile does not exist in [" + command.getExecutable() + "]");
        // }
        // }
    }

    @Override
    public void process(ScriptArgument argument) {
        if (argument instanceof SQLCommandArgumentImpl) {
            runSqlCommand((SQLCommandArgumentImpl) argument);
        } else if (command != null) {
            command.process(argument);
        }
    }

    @Override
    public void setNextCommand(AbstractCommand command) {
        this.command = command;
    }
}
