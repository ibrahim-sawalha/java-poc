package app.installer.exec.argument;

import java.io.Serializable;

public class SQLCommandArgumentImpl extends AbstractScriptArgument implements Serializable {
    private static final long serialVersionUID = 1L;

    private String dbUserName;
    private String dbPassword;
    private String dbSid;
    private String dbAddress;
    private String dbPort = "1521";

    @Override
    public String getExecutable() {
        return executableCommand;
    }

    @Override
    public void setExecutable(String scriptCommand) {
        this.executableCommand = scriptCommand;
    }

    public SQLCommandArgumentImpl(String scriptCommand) {
        setExecutable(scriptCommand);
    }

    public String getDbUserName() {
        return dbUserName;
    }

    public SQLCommandArgumentImpl setDbUserName(String dbUserName) {
        this.dbUserName = dbUserName;
        return this;
    }

    public String getDbPassword() {
        return dbPassword;
    }

    public SQLCommandArgumentImpl setDbPassword(String dbPassword) {
        this.dbPassword = dbPassword;
        return this;
    }

    @Override
    public boolean isValid() {
        if (dbUserName == null || dbPassword == null || dbUserName.isEmpty() || dbPassword.isEmpty()
                || dbAddress == null || dbAddress.isEmpty() || dbSid == null || dbSid.isEmpty() || dbPort == null
                || dbPort.isEmpty() || getExecutable() == null || getExecutable().isEmpty()) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("SQLScriptCommandArgumentImpl [dbUserName=");
        builder.append(dbUserName);
        builder.append(", dbPassword=");
        builder.append(dbPassword);
        builder.append(", getScriptCommand()=");
        builder.append(getExecutable());
        builder.append("]");
        return builder.toString();
    }

    @Override
    public ScriptArgument getBackupCommand() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ScriptArgument getRestoreCommand() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setBackupCommand(ScriptArgument x) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setRestoreCommand(ScriptArgument x) {
        // TODO Auto-generated method stub

    }

    @Override
    public String getScriptArguments() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setScriptArguments(String scriptArguments) {
        // TODO Auto-generated method stub

    }

    @Override
    public String getCommandType() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setCommandType(String commandType) {
        // TODO Auto-generated method stub

    }

    public String getDbSid() {
        return dbSid;
    }

    public void setDbSid(String dbSid) {
        this.dbSid = dbSid;
    }

    public String getDbAddress() {
        return dbAddress;
    }

    public void setDbAddress(String dbAddress) {
        this.dbAddress = dbAddress;
    }

    public String getDbPort() {
        return dbPort;
    }

    public void setDbPort(String dbPort) {
        this.dbPort = dbPort;
    }

}
