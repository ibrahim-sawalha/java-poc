package app.installer.exec.command;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.slf4j.LoggerFactory;

import app.installer.exec.argument.JBossCommandArgumentImpl;
import app.installer.exec.argument.ScriptArgument;
import app.installer.exec.command.common.AbstractCommand;
import ch.qos.logback.classic.Logger;

public class JBossCommand extends AbstractCommand {

    final static Logger log = (Logger) LoggerFactory.getLogger(JBossCommand.class);
    private AbstractCommand command;
    private String jbossCliShellFilePath = "/u01/EAP6/jboss-eap-6.4/wildfly-8.1.0.Final/bin/jboss-cli.sh";

    public JBossCommand() {
        super();
    }

    public JBossCommand(String s) {
        super();
    }

    public void runJBossScriptCommand(JBossCommandArgumentImpl argument) {
        File jbossCliShellFile = FileUtils.getFile(jbossCliShellFilePath);
        if (!jbossCliShellFile.exists()) {
            log.error(jbossCliShellFilePath + " file does not exist.");
            if (argument.isExitOnFailure()) {
                throw new RuntimeException(jbossCliShellFilePath + " file does not exist.");
            }
            return;
        }

        File shellFile = FileUtils.getFile(argument.getExecutable());
        if (shellFile.exists()) {
            if (argument.getScriptArguments() != null) {
                runJBossScript(log, argument.getExecutable() + " " + argument.getScriptArguments());
            } else {
                runJBossScript(log, argument.getExecutable());
            }
        } else {
            log.error("shellFile does not exist in [" + argument.getExecutable() + "]");
            if (argument.isExitOnFailure()) {
                throw new RuntimeException("shellFile does not exist in [" + shellFile + "]");
            }
            return;
        }
    }

    @Override
    public void process(ScriptArgument argument) {
        if (argument instanceof JBossCommandArgumentImpl) {
            runJBossScriptCommand((JBossCommandArgumentImpl) argument);
        } else if (command != null) {
            command.process(argument);
        }
    }

    @Override
    public void setNextCommand(AbstractCommand command) {
        this.command = command;
    }
}
