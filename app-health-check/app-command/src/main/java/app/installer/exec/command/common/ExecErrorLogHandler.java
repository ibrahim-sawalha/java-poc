package app.installer.exec.command.common;

import org.apache.commons.exec.LogOutputStream;
import org.slf4j.event.Level;

import ch.qos.logback.classic.Logger;

public class ExecErrorLogHandler extends LogOutputStream {
    private Logger log;
    private ExecResultsHandler result;

    public ExecErrorLogHandler(Logger log, Level level) {
        super(level.toInt());
        this.log = log;
        result = new ExecResultsHandler();
    }

    @Override
    protected void processLine(String line, int level) {
        // if (line != null && line.toUpperCase().contains("ERROR")) {
        // } else {
        // log.info(line);
        // }
        log.error(line);
        // result.addLine(line);
    }

    public ExecResultsHandler getExecutionResults() {
        return result;
    }
}
