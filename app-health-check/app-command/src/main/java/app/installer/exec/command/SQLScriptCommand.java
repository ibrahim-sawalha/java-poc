package app.installer.exec.command;

import org.apache.commons.io.FileUtils;
import org.slf4j.LoggerFactory;

import app.installer.exec.argument.SQLScriptCommandArgumentImpl;
import app.installer.exec.argument.ScriptArgument;
import app.installer.exec.command.common.AbstractCommand;
import app.installer.exec.command.common.ExecResultsHandler;
import ch.qos.logback.classic.Logger;

public class SQLScriptCommand extends AbstractCommand {

    final static Logger log = (Logger) LoggerFactory.getLogger(SQLScriptCommand.class);
    private AbstractCommand command;

    public SQLScriptCommand() {
        super();
    }

    public void runSqlScriptCommand(SQLScriptCommandArgumentImpl argument) {
        if (!argument.isValid()) {
            log.error("Cannot connect to database using sqlplus command. userName=["
                    + argument.getDataBaseEntity().getDbUserName() + "] password=["
                    + argument.getDataBaseEntity().getDbPassword() + "] sqlScritp file=[" + argument.getExecutable()
                    + "]");
            if (argument.isExitOnFailure()) {
                throw new RuntimeException("sqlFile have errors, please review log files.");
            }
        }

        StringBuffer sql = new StringBuffer("sqlplus");
        sql.append(" ");
        sql.append(argument.getDataBaseEntity().getDbUserName());
        sql.append("/");
        sql.append(argument.getDataBaseEntity().getDbPassword());
        sql.append(" @ ");
        if (FileUtils.getFile(argument.getExecutable()).exists()) {
            ExecResultsHandler execResult = runCommand(log, sql.toString() + argument.getExecutable());
            if (!execResult.isExecutionSuccess() && argument.isExitOnFailure()) {
                throw new RuntimeException("sqlFile have errors, please review log files.");
            }
        } else {
            log.error("sqlFile does not exist in [" + argument.getExecutable() + "]");
            if (argument.isExitOnFailure()) {
                throw new RuntimeException("sqlFile does not exist in [" + argument.getExecutable() + "]");
            }
        }
    }

    @Override
    public void process(ScriptArgument argument) {
        if (argument instanceof SQLScriptCommandArgumentImpl) {
            runSqlScriptCommand((SQLScriptCommandArgumentImpl) argument);
        } else if (command != null) {
            command.process(argument);
        }
    }

    @Override
    public void setNextCommand(AbstractCommand command) {
        this.command = command;
    }
}
