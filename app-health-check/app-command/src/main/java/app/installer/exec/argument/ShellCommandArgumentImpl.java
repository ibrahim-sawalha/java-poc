package app.installer.exec.argument;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;

public class ShellCommandArgumentImpl extends AbstractScriptArgument {// implements ScriptArgument {

    final static Logger log = (Logger) LoggerFactory.getLogger(ShellCommandArgumentImpl.class);
    private String scriptArguments;

    protected ShellCommandArgumentImpl() {
    }

    public ShellCommandArgumentImpl(String scriptCommand) {
        setExecutable(scriptCommand);
    }

    @Override
    public boolean isValid() {
        if (getExecutable() == null || getExecutable().isEmpty()) {
            return false;
        }
        return true;
    }

    @Override
    public String getScriptArguments() {
        return scriptArguments;
    }

    @Override
    public void setScriptArguments(String scriptArguments) {
        this.scriptArguments = scriptArguments;
    }

    @Override
    public ScriptArgument getBackupCommand() {
        return null;
    }

    @Override
    public ScriptArgument getRestoreCommand() {
        return null;
    }

    @Override
    public void setBackupCommand(ScriptArgument x) {
    }

    @Override
    public void setRestoreCommand(ScriptArgument x) {
    }

    @Override
    public String getCommandType() {
        return null;
    }

    @Override
    public void setCommandType(String commandType) {
    }

}
