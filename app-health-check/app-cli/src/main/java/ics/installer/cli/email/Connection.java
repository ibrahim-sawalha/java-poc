package ics.installer.cli.email;

import java.lang.reflect.Field;

public class Connection {
    public Connection() {
    }

    public Connection(String host, String port, String userName, String password) {
        this.host = host;
        this.port = port;
        this.userName = userName;
        this.password = password;
    }

    public Connection(String host, String port) {
        this.host = host;
        this.port = port;
    }

    private String host;
    private String port;
    private String userName;
    private String password;
    private Boolean startTls;

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((host == null) ? 0 : host.hashCode());
        result = prime * result + ((password == null) ? 0 : password.hashCode());
        result = prime * result + ((port == null) ? 0 : port.hashCode());
        result = prime * result + ((userName == null) ? 0 : userName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        Field[] fields = Connection.class.getDeclaredFields();
        for (Field field : fields) {
            try {
                Object thisVal = field.get(this);
                Object thatVal = field.get(obj);
                if (thisVal != null && thatVal != null) {
                    if (!thisVal.equals(thatVal)) {
                        return false;
                    }
                } else if (thisVal != thatVal) {
                    return false;
                }
            } catch (Exception e) {
                return false;
            }
        }

        return true;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getStartTls() {
        return startTls;
    }

    public void setStartTls(boolean startTls) {
        this.startTls = startTls;
    }
}
