package ics.installer.cli.rmi;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;

public class ApplicationServer {
    final static Logger log = (Logger) LoggerFactory.getLogger(ApplicationServer.class);

    public static void main(String args[]) throws Exception {
        ApplicationServer obj = new ApplicationServer();
        obj.startRmiServer("10.100.102.37", "1099");
        // obj.startRmiServer("10.20.20.108", "1099");
        // log.get
    }

    public void startRmiServer(String host, String port) {
        try {
            System.setProperty("java.rmi.server.hostname", host);

            java.rmi.registry.LocateRegistry.createRegistry(1099);
            log.info("RMI Registry ready...");
        } catch (Exception e) {
            log.error("", e);
        }

        try {
            CommandService serverRemote = new CommandServiceImpl();
            CommandService stub = (CommandService) UnicastRemoteObject.exportObject(serverRemote,
                Integer.valueOf(port));

            // Bind the remote object's stub in the registry
            Registry registry = LocateRegistry.getRegistry();
            registry.bind("ICS-Installer", stub);

            log.info("RMI Server ready... " + host + ":" + port);

            Thread thread = new Thread("ics-print") {
                @Override
                public synchronized void run() {
                    while (true) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            };
            thread.start();

        } catch (Exception e) {
            log.error("ERROR: Exception starting Server:", e);
        }

    }

}
