package ics.installer.cli.rmi;

import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.List;

import app.installer.exec.argument.SQLScriptCommandArgumentImpl;
import app.installer.exec.argument.ScriptArgument;
import app.installer.exec.command.SQLScriptCommand;
import ics.installer.common.log.IcsLogger;

public class CommandServiceImpl implements CommandService {

    @Override
    public String execCommand(String commandName) throws RemoteException {
        return "exe " + commandName;
    }

    @Override
    public List<String> execCommandArgument(ScriptArgument sqlArgument) throws RemoteException, RuntimeException {
        IcsLogger.startLogging();
        try {
            if (sqlArgument instanceof SQLScriptCommandArgumentImpl) {
                System.out.println(((SQLScriptCommandArgumentImpl) sqlArgument).getExecutable());
                SQLScriptCommand sql = new SQLScriptCommand();

                sql.process(sqlArgument);
                return Arrays.asList("Success");
            }
        } catch (RuntimeException e) {
            return IcsLogger.getLog();
        }
        return null;

    }

}
