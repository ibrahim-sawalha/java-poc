package ics.installer.cli.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import app.installer.exec.argument.ScriptArgument;

public interface CommandService extends Remote {

    public String execCommand(String commandName) throws RemoteException;

    public List<String> execCommandArgument(ScriptArgument script) throws RemoteException;

}
