package ics.installer.cli.email;

public class TestEmail {

    public static void main(String[] args) {
        try {

            // try {
            // JBossScriptGenerator.generateJBossMenu(true);
            // } catch (IOException e) {
            // e.printStackTrace();
            // }

            Connection connection = new Connection();
            connection.setHost("100.100.20.1");
            connection.setPort("25");
            connection.setPassword("noreply");
            // connection.setUserName("noreply@icslondon.com");// working
            connection.setUserName("noreply");

            Recipients recipients = new Recipients();
            recipients.addRecipient(new Recipient("To", "ibrahim", "ibrahim.sawalha@icsfs.com", "079"));
            recipients.addRecipient(new Recipient("To", null, "ibrahim.sawalha@gmail.com", null));

            Message message = new Message();
            message.setFrom("noreply");
            message.setBody("test");
            message.setSubject("test-Rasheed");
            message.setRecipients(recipients);

            Email email = new Email();
            email.setMessage(message);

            email.setConnection(connection);
            SendEmail.sendHtml(email, connection, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
