package ics.installer.cli;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.io.FileUtils;
import org.slf4j.LoggerFactory;

import app.installer.exec.argument.PythonCommandArgumentImpl;
import app.installer.exec.argument.SQLScriptCommandArgumentImpl;
import app.installer.exec.argument.ShellCommandArgumentImpl;
import app.installer.exec.command.PythonCommand;
import app.installer.exec.command.SQLScriptCommand;
import app.installer.exec.command.ShellCommand;
import ch.qos.logback.classic.Logger;
import ics.health.bpms.validate.DatabaseConnection;
import ics.installer.cli.input.InstallerConfiguration;
import ics.installer.common.entity.DataBaseEntity;
import ics.installer.common.entity.EntityFactory;
import ics.installer.common.entity.WeblogicConnEntity;
import ics.installer.generator.InstallerPath;
import ics.installer.generator.WeblogicScriptGenerator;
import ics.installer.generator.WeblogicScriptPath;

public class WeblogicInstallerCli {

    final static Logger log = (Logger) LoggerFactory.getLogger(WeblogicInstallerCli.class);

    private static File formsConfigPath;
    private File sqlScriptFile;
    private File jvmcontrollersFile;

    public WeblogicInstallerCli() {
        if (System.getenv().get("FORMS_CONF") != null) {
            formsConfigPath = FileUtils.getFile(System.getenv().get("FORMS_CONF"));
        } else {
            log.error("Could not find $FORMS_CONF environment variable.");
            throw new RuntimeException();
        }
        sqlScriptFile = FileUtils.getFile("/u/oracle/banksql/BPM_INIT.sql");
        if (!sqlScriptFile.exists()) {
            log.error("Could not find file /u/oracle/banksql/BPM_INIT.sql");
            throw new RuntimeException();
        }
        jvmcontrollersFile = FileUtils.getFile(formsConfigPath.getAbsolutePath() + File.separator + "default.env");
        if (!jvmcontrollersFile.exists()) {
            log.error("Could not find file " + formsConfigPath.getAbsolutePath() + File.separator + "default.env");
            throw new RuntimeException();
        }
        try {
            WeblogicScriptGenerator.generateUpdateJvmControllers(false);
            WeblogicScriptGenerator.generateUpdateFormsUrl(false);
            WeblogicScriptGenerator.generateCreateWeblogicUser(false);
            WeblogicScriptGenerator.generateBanksJVMScripts(false);
        } catch (IOException e) {
            log.error("Error in Generating Script file.", e);
        }
    }

    public void installAndVerifyBPMCli(Scanner scanner) {
        WeblogicInstallerCli instance = new WeblogicInstallerCli();
        InstallerConfiguration installation = instance.readInstallationFile();

        String dbAddress = null;
        String dbUserName = "banksys";
        String dbPassword = null;
        String dbSid = "hobank";
        String weblogicAdminPassword = null;
        if (installation != null) {
            dbAddress = installation.getDbAddress();
            // dbUserName = installation.getDbUserName();
            // dbSid = installation.getDbSid();
        }

        while (weblogicAdminPassword == null || weblogicAdminPassword.isEmpty()) {
            log.info("Enter Weblogic Admin Password: ");
            weblogicAdminPassword = scanner.nextLine();
        }
        while (dbAddress == null || dbAddress.isEmpty()) {
            log.info("Enter Database Address/IP: ");
            dbAddress = scanner.nextLine();
        }
        while (dbSid == null || dbSid.isEmpty()) {
            log.info("Enter Database SID: ");
            dbSid = scanner.nextLine();
        }
        while (dbUserName == null || dbUserName.isEmpty()) {
            log.info("Enter Database Username: ");
            dbUserName = scanner.nextLine();
        }
        while (dbPassword == null || dbPassword.isEmpty()) {
            log.info("Enter Database Username <" + dbUserName + "> Password: ");
            // System.console().readPassword();
            dbPassword = scanner.nextLine();
        }

        installation.setDbAddress(dbAddress);
        installation.setDbSid(dbSid);
        installation.setDbUserName(dbUserName);
        instance.writeInstallationFile(installation);
        WeblogicConnEntity weblogicEntity = EntityFactory.buildWeblogicConnEntity("weblogic", weblogicAdminPassword,
            "127.0.0.1", "7001");
        DataBaseEntity dbEntity = EntityFactory.buildDataBaseEntity(dbUserName, dbPassword, dbAddress, dbSid, "1521");

        installAndVerifyBPM(weblogicEntity, dbEntity);
    }

    public void installAndVerifyBPM(WeblogicConnEntity weblogicEntity, DataBaseEntity dbEntity) {

        SQLScriptCommand sql = new SQLScriptCommand();
        ShellCommand shellCommand = new ShellCommand();

        DatabaseConnection db = new DatabaseConnection(dbEntity);

        if (!db.isConnectionValid()) {
            log.error("Database Connection is not Valid.");
            throw new RuntimeException("Database Connection is not Valid.");
        }

        SQLScriptCommandArgumentImpl sqlArgument = new SQLScriptCommandArgumentImpl(sqlScriptFile.getAbsolutePath());
        sqlArgument.setDataBaseEntity(dbEntity);
        sqlArgument.setExitOnFailure(true);
        sql.process(sqlArgument);

        PythonCommand python = new PythonCommand();
        PythonCommandArgumentImpl pythonArgument = new PythonCommandArgumentImpl(
                WeblogicScriptPath.updateJvmcontrollers_Py_OSPATH);
        PythonCommandArgumentImpl updateFormsUrlArgument = new PythonCommandArgumentImpl(
                WeblogicScriptPath.UpdateFormsUrl_py_OSPATH);

        python.process(pythonArgument);
        python.process(updateFormsUrlArgument);

        ShellCommand shell = new ShellCommand();
        ShellCommandArgumentImpl shellArgument = new ShellCommandArgumentImpl(
                WeblogicScriptPath.CreateWeblogicUser_sh_OSPATH);
        shellArgument.setScriptArguments(
            "-p " + weblogicEntity.getAdminPassword() + " -s Administrator -w 2000aaaa -g Administrators");
        shell.process(shellArgument);

        ShellCommandArgumentImpl stopBanksJVMshellArgument = new ShellCommandArgumentImpl(
                WeblogicScriptPath.stopBanksJVM_Sh_OSPATH);
        ShellCommandArgumentImpl startBanksJVMshellArgument = new ShellCommandArgumentImpl(
                WeblogicScriptPath.startBanksJVM_Sh_OSPATH);

        shellCommand.process(stopBanksJVMshellArgument);
        shellCommand.process(startBanksJVMshellArgument);

    }

    public InstallerConfiguration readInstallationFile() {
        try {
            File file = FileUtils.getFile(InstallerPath.weblogicInstallation_xml_OSPATH);
            if (!file.exists()) {
                return new InstallerConfiguration();
            }
            JAXBContext jaxbContext = JAXBContext.newInstance(InstallerConfiguration.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

            return (InstallerConfiguration) unmarshaller.unmarshal(file);
        } catch (JAXBException e) {
            log.error("Error creating file " + InstallerPath.weblogicInstallation_xml_OSPATH, e);
        }
        return null;
    }

    public void writeInstallationFile(InstallerConfiguration weblogicInstallation) {
        try {
            File scriptsDir = FileUtils.getFile(WeblogicScriptPath.weblogic_scripts_OS_path);
            if (!scriptsDir.exists()) {
                FileUtils.forceMkdir(scriptsDir);
            }
            File file = FileUtils.getFile(InstallerPath.weblogicInstallation_xml_OSPATH);
            JAXBContext jaxbContext = JAXBContext.newInstance(InstallerConfiguration.class);

            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(weblogicInstallation, file);

        } catch (JAXBException e) {
            log.error("Error creating file " + InstallerPath.weblogicInstallation_xml_OSPATH, e);
        } catch (IOException e) {
            log.error("Error creating file " + InstallerPath.weblogicInstallation_xml_OSPATH, e);
        }
    }

}
