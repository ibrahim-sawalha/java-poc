package ics.installer.cli;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.io.FileUtils;
import org.slf4j.LoggerFactory;

import app.installer.exec.argument.PythonCommandArgumentImpl;
import app.installer.exec.argument.ShellCommandArgumentImpl;
import app.installer.exec.command.PythonCommand;
import app.installer.exec.command.ShellCommand;
import ch.qos.logback.classic.Logger;
import ics.health.bpms.exec.UpdateLimitsConf;
import ics.health.bpms.validate.DatabaseConnection;
import ics.installer.cli.input.InstallerConfiguration;
import ics.installer.common.entity.DataBaseEntity;
import ics.installer.common.entity.EntityFactory;
import ics.installer.generator.InstallerPath;
import ics.installer.generator.JBossScriptGenerator;
import ics.installer.generator.JBossScriptPath;

public class JBossInstallerCli {

    final static Logger log = (Logger) LoggerFactory.getLogger(JBossInstallerCli.class);
    public static String BPM_DS_USER = "BRMS6";
    public static String BANKS_DS_USER = "BANKWS";
    private File jbossCliShell;
    private Scanner scanner;

    public JBossInstallerCli(Scanner scanner) throws IOException {
        this.scanner = scanner;
        validateJBossHome();
    }

    private void validateJBossHome() {
        if (System.getenv().get("JBOSS_HOME") == null) {
            log.warn("$JBOSS_HOME environement variable was not found");
        }
        try {
            JBossScriptGenerator.generateUpdateLimitsConf(true);
            JBossScriptGenerator.generateUpdateEmailAddress(false);
            JBossScriptGenerator.generateInstallJBossServer(false);
            JBossScriptGenerator.generatePrepareJBossServer(false);
        } catch (IOException e) {
            log.error("Error in Generating Script file.", e);
        }
        // jbossCliShell = FileUtils.getFile(
        // System.getenv().get("JBOSS_HOME") + File.separator + "bin" + File.separator + "jboss-cli.sh");
        // if (!jbossCliShell.exists()) {
        // log.error("Could not find " + jbossCliShell.getAbsolutePath() + " environment variable.");
        // throw new RuntimeException(
        // "Could not find " + jbossCliShell.getAbsolutePath() + " environment variable.");
        // }
        // } else {
        // log.error("Could not find $JBOSS_HOME environment variable.");
        // throw new RuntimeException("Could not find $JBOSS_HOME environment variable.");
        // }
    }

    public void prepareJBossServer(String zipFile, String extractionPath) {
        if (zipFile.isEmpty() || zipFile == null) {
            throw new NullPointerException("ZIP File name was NULL.");
        }
        if (FileUtils.getFile(zipFile).exists()) {
            log.error("ZIP File " + zipFile + " already exist.");
            throw new RuntimeException("ZIP File " + zipFile + " already exist.");
        }
        // log.info("Creating Zip File " + zipFile + " from /u01/EAP6/jboss-eap-6.4/");
        log.info("Creating Zip File " + zipFile + " from " + extractionPath);
        // Zipper appZip = new Zipper(new File(extractionPath));
        // appZip.zipIt(zipFile);

        PythonCommandArgumentImpl installJBoss = new PythonCommandArgumentImpl(
                JBossScriptPath.PrepareJBossServer_py_OSPATH);
        PythonCommand python = new PythonCommand();
        installJBoss.setExitOnFailure(true);
        python.process(installJBoss);
        log.info("Zip File " + zipFile + " was successfully created.");
    }

    private void updateEmailAddress() {
        String serverIp = null;
        String serverPort = null;
        String emailAddress = null;
        String emailPassword = null;
        while (serverIp == null || serverIp.isEmpty()) {
            log.info("Enter Email Server's Address/IP: ");
            serverIp = scanner.nextLine();
        }
        while (serverPort == null || serverPort.isEmpty()) {
            log.info("Enter Email Server's port [default 25]: ");
            serverPort = scanner.nextLine();
        }
        while (emailAddress == null || emailAddress.isEmpty()) {
            log.info("Enter Email UserName: ");
            emailAddress = scanner.nextLine();
        }
        while (emailPassword == null || emailPassword.isEmpty()) {
            log.info("Enter Email Password: ");
            emailPassword = scanner.nextLine();
        }

        PythonCommandArgumentImpl updateEmailAddress = new PythonCommandArgumentImpl(
                JBossScriptPath.UpdateEmailAddress_py_OSPATH);
        updateEmailAddress.setScriptArguments("--serverIp", serverIp, "--serverPort", serverPort, "--emailAddress",
            emailAddress, "--emailPassword", emailPassword);
        PythonCommand python = new PythonCommand();
        python.process(updateEmailAddress);
    }

    public void installJBossServer(String zipFile, String extractionPath) {
        if (zipFile.isEmpty() || zipFile == null) {
            throw new NullPointerException("ZIP File name was NULL.");
        }
        if (!FileUtils.getFile(zipFile).exists()) {
            log.error("ZIP File " + zipFile + " was not found.");
            throw new RuntimeException("ZIP File " + zipFile + " was not found.");
        }

        try {
            JBossScriptGenerator.generateJBossMenu(true);
        } catch (IOException e) {
            log.error("Error in Generating Script file.", e);
        }

        UpdateLimitsConf.updateLimitsConf();

        File exportDir = FileUtils.getFile(extractionPath);

        if (exportDir.exists()) {
            updateEmailAddress();
            throw new RuntimeException(exportDir + " was found. It should be deleted first.");
        } else {
            PythonCommandArgumentImpl installJBoss = new PythonCommandArgumentImpl(
                    JBossScriptPath.InstallJBossServer_py_OSPATH);
            PythonCommand python = new PythonCommand();
            installJBoss.setExitOnFailure(true);
            python.process(installJBoss);
            // log.info("Extracting zip File " + zipFile + " to " + extractionPath);
            // Unzipper unZip = new Unzipper();
            // unZip.unZipIt(zipFile, extractionPath);
            // log.info("Extracting zip File " + zipFile + " was finished successfully.");
            updateEmailAddress();
        }
    }

    public void changeDSPasswordCli(Scanner scanner, String dsUser) {
        log.info("Changing DataSource password requires restart JBoss server, are you sure you want to continue?");
        String line = "";
        while (line == null || line.isEmpty() || !line.trim().toUpperCase().startsWith("Y")
                || !line.trim().toUpperCase().startsWith("N")) {
            log.info("Enter your chouce [yes/no]: ");
            line = scanner.nextLine();
            if (line.trim().toUpperCase().startsWith("N")) {
                log.info("Operation was canceled.");
                System.exit(0);
            } else if (line.trim().toUpperCase().startsWith("Y")) {
                break;
            }
        }

        InstallerConfiguration installation = readInstallationFile();
        String dbAddress = null;
        String dbUserName = "bankws";
        String dbSid = "hobank";
        String dbPassword = null;
        String dsPassword = null;
        // String brms6Password = null;
        // String bankwsPassword = null;
        if (installation != null) {
            dbAddress = installation.getDbAddress();
            // dbUserName = installation.getDbUserName();
            // dbSid = installation.getDbSid();
        }

        if (dbAddress != null) {
            log.info("Is the Database Address/IP [" + dbAddress + "] correct? ");
            line = "";
            while (line == null || line.isEmpty() || !line.trim().toUpperCase().startsWith("Y")
                    || !line.trim().toUpperCase().startsWith("N")) {
                log.info("Enter your chouce [yes/no]: ");
                line = scanner.nextLine();
                if (line.trim().toUpperCase().startsWith("N")) {
                    dbAddress = null;
                } else if (line.trim().toUpperCase().startsWith("Y")) {
                    break;
                }
            }

        }
        while (dbAddress == null || dbAddress.isEmpty()) {
            log.info("Enter Database Address/IP: ");
            dbAddress = scanner.nextLine();
        }
        while (dbSid == null || dbSid.isEmpty()) {
            log.info("Enter Database SID: ");
            dbSid = scanner.nextLine();
        }
        while (dbUserName == null || dbUserName.isEmpty()) {
            log.info("Enter Database Username: ");
            dbUserName = scanner.nextLine();
        }
        while (dbPassword == null || dbPassword.isEmpty()) {
            log.info("Enter Database Username <" + dbUserName + "> Password: ");
            // System.console().readPassword();
            dbPassword = scanner.nextLine();
        }
        while (dsPassword == null || dsPassword.isEmpty()) {
            log.info("Enter " + dsUser + " New Password: ");
            // System.console().readPassword();
            dsPassword = scanner.nextLine();
        }

        installation.setDbAddress(dbAddress);
        installation.setDbSid(dbSid);
        installation.setDbUserName(dbUserName);
        writeInstallationFile(installation);
        changeDSPassword(dsUser, dsPassword, dbUserName, dbPassword, dbAddress, dbSid);
    }

    public void changeDSPassword(String dsUser, String dsPassword, String dbUserName, String dbPassword,
            String dbAddress, String dbSid) {

        if (dsUser == null || dsPassword == null || dbUserName == null || dbPassword == null || dbAddress == null
                || dbSid == null || dsUser.isEmpty() || dsPassword.isEmpty() || dbUserName.isEmpty()
                || dbPassword.isEmpty() || dbAddress.isEmpty() || dbSid.isEmpty()) {
            throw new NullPointerException(
                    "dsUser and/or dsPassword and/or dbUserName and/or dbPassword and/or dbAddress and/or dbSid is NULL.");
        }

        DataBaseEntity dbEntity = EntityFactory.buildDataBaseEntity(dbUserName, dbPassword, dbAddress, dbSid, "1521");
        DatabaseConnection db = new DatabaseConnection(dbEntity);

        if (!db.isConnectionValid()) {
            log.error("Database Connection is not Valid.");
            throw new RuntimeException("Database Connection is not Valid.");
        }

        try {
            log.info("Start ALTER " + dsUser + " Password");
            Connection conn = db.getDBConnection();
            Statement s = conn.createStatement();
            s.setEscapeProcessing(false);
            s.executeUpdate("alter user " + dsUser + " identified by " + dsPassword);
            DbUtils.close(conn);
            log.info("Finish ALTER " + dsUser + " Password");
        } catch (SQLException e) {
            log.error("{}", e);
        }

        ShellCommand shellCommand = new ShellCommand();
        ShellCommandArgumentImpl runUpdateDSshellArgument = null;

        if (dsUser.equals(BPM_DS_USER)) {
            try {
                JBossScriptGenerator.generateBpmDSJBoss(dbAddress, dsPassword, null);
            } catch (IOException e) {
                log.error("{}", e);
            }
            runUpdateDSshellArgument = new ShellCommandArgumentImpl(JBossScriptPath.updateBpmDS_Sh_OSPATH);
            shellCommand.process(runUpdateDSshellArgument);
        } else if (dsUser.equals(BANKS_DS_USER)) {
            try {
                JBossScriptGenerator.generateBanksDSJBoss(dbAddress, null, dsPassword);
            } catch (IOException e) {
                log.error("{}", e);
            }
            runUpdateDSshellArgument = new ShellCommandArgumentImpl(JBossScriptPath.updateBanksDS_Sh_OSPATH);
            shellCommand.process(runUpdateDSshellArgument);

        }
    }

    public InstallerConfiguration readInstallationFile() {
        try {
            File file = FileUtils.getFile(InstallerPath.jbossInstallation_xml_OSPATH);
            if (!file.exists()) {
                return new InstallerConfiguration();
            }
            JAXBContext jaxbContext = JAXBContext.newInstance(InstallerConfiguration.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

            return (InstallerConfiguration) unmarshaller.unmarshal(file);
        } catch (JAXBException e) {
            log.error("{}", e);
        }
        return null;
    }

    public void writeInstallationFile(InstallerConfiguration weblogicInstallation) {
        try {
            File scriptsDir = FileUtils.getFile(JBossScriptPath.jboss_scripts_OS_path);
            if (!scriptsDir.exists()) {
                FileUtils.forceMkdir(scriptsDir);
            }
            File file = FileUtils.getFile(InstallerPath.jbossInstallation_xml_OSPATH);
            JAXBContext jaxbContext = JAXBContext.newInstance(InstallerConfiguration.class);

            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(weblogicInstallation, file);

        } catch (JAXBException e) {
            log.error("Error creating file " + InstallerPath.jbossInstallation_xml_OSPATH, e);
        } catch (IOException e) {
            log.error("Error creating file " + InstallerPath.jbossInstallation_xml_OSPATH, e);
        }
    }

}
