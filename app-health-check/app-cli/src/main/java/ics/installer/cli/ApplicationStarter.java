package ics.installer.cli;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Scanner;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import ch.qos.logback.classic.Logger;

public class ApplicationStarter {
    final static Logger log = (Logger) LoggerFactory.getLogger(ApplicationStarter.class);
    // public static Scanner scanner = new Scanner(System.in);

    private Options options = new Options();

    private String[] args;

    public ApplicationStarter(String[] args) throws IOException {
        this.args = args;

        options.addOption("h", "help", false, "Show help.");
        options.addOption(null, "updateBpmDS", false, "Change BPM_DS password on JBoss Server");
        options.addOption(null, "updateBanksDS", false, "Change BANKS_DS password on JBoss Server");
        options.addOption(null, "installWeblogic", false, "Install & Verify BPMS on Weblogic Server");
        // options.addOption(null, "installJBoss", true, "Install JBoss Server & Workspace ");
        options.addOption(null, "prepareJBoss", true, "Zip the JBoss Server's directory.");

        options.addOption(Option.builder().optionalArg(true).longOpt("installJBoss").hasArg(true)
                .desc("Install JBoss Server & Workspace ").build());

        File logFile = new File("logback-debug.xml");
        if (!logFile.exists()) {
            String text = IOUtils.toString(this.getClass().getClassLoader().getResourceAsStream("logback-debug.xml"));
            FileUtils.write(logFile, text, Charset.forName("UTF-8"), false);
        }
    }

    public void parse(Scanner scanner) throws SAXException, IOException {
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = null;
        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (cmd.hasOption("h")) {
            help();
        }
        if (cmd.hasOption("updateBpmDS")) {
            new JBossInstallerCli(scanner).changeDSPasswordCli(scanner, JBossInstallerCli.BPM_DS_USER);
        } else if (cmd.hasOption("updateBanksDS")) {
            new JBossInstallerCli(scanner).changeDSPasswordCli(scanner, JBossInstallerCli.BANKS_DS_USER);
        } else if (cmd.hasOption("installWeblogic")) {
            new WeblogicInstallerCli().installAndVerifyBPMCli(scanner);
        } else if (cmd.hasOption("installJBoss")) {
            String zipPath = "/u01/BPM/ics-bpm6.zip";
            if (cmd.getOptionValue("installJBoss") != null) {
                zipPath = cmd.getOptionValue("installJBoss");
            }
            new JBossInstallerCli(scanner).installJBossServer(zipPath, "/u01/EAP6/jboss-eap-6.4/");
        } else if (cmd.hasOption("prepareJBoss")) {
            String zipPath = cmd.getOptionValue("prepareJBoss");
            new JBossInstallerCli(scanner).prepareJBossServer(zipPath, "/u01/EAP6/jboss-eap-6.4/");
        } else {
            help();
        }
    }

    private void help() {
        HelpFormatter formater = new HelpFormatter();
        StringBuffer header = new StringBuffer("\nICSFS Installer & Verifier \n\n");
        StringBuffer footer = new StringBuffer(
                "\nTo enable or change debug logging settings add the following argument to the execution command:\n"
                        + " -Dlogback.configurationFile=logback-debug.xml");
        formater.printHelp("java -jar [JAR-NAME]", header.toString(), options, footer.toString(), true);
        System.exit(0);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        try {
            new ApplicationStarter(args).parse(scanner);
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            scanner.close();
        }
    }

}
