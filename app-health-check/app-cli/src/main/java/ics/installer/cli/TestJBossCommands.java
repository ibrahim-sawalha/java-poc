package ics.installer.cli;

import org.jboss.as.cli.scriptsupport.CLI;

public class TestJBossCommands {

    public static void main(String[] args) {
        CLI cli = CLI.newInstance();
        cli.connect();
        CLI.Result r = cli.cmd("data-source disable --name=ExampleDS");
        System.out.println(r.getResponse().get("result"));
    }

}
