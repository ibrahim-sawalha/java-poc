package ics.installer.cli;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;
import javax.sql.DataSource;

import app.installer.exec.argument.PythonCommandArgumentImpl;
import app.installer.exec.command.PythonCommand;

public class TestDatasourceConnection {

    public TestDatasourceConnection() {
    }

    public static void main(String args[]) throws Exception {
        PythonCommandArgumentImpl pythonArgument = new PythonCommandArgumentImpl(
                "D:/Developer/POC/java-poc/app-health-check/app-cli/update-jvmcontrollers.py");
        PythonCommand python = new PythonCommand();
        python.process(pythonArgument);
        new TestDatasourceConnection().test();
    }

    public void test() throws NamingException, SQLException {
        Properties env = new Properties();
        String datasourceName = "jdbc/banks";
        env.put("java.naming.factory.initial", "weblogic.jndi.WLInitialContextFactory");
        env.put("java.naming.provider.url", "t3://10.100.102.37:7001");
        InitialContext ctxProxy = new InitialContext(env);
        Object dsObj = ctxProxy.lookup(datasourceName);
        DataSource ds = (DataSource) PortableRemoteObject.narrow(dsObj, javax.sql.DataSource.class);
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet result = null;
        try {
            conn = ds.getConnection();
            String SelectString = "SELECT count(*) from dual";
            ps = conn.prepareStatement(SelectString);
            for (result = ps.executeQuery(); result.next();) {
                String data = result.getString(1) + " : " + result.getString(2);
                System.out.println(data);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (result != null) {
                result.close();
            }
            if (ps != null) {
                ps.close();
            }
            conn.close();
            conn = null;
            ps = null;
            result = null;
        }
    }
}
