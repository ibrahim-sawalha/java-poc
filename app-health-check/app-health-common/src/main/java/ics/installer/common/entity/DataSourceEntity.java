package ics.installer.common.entity;

public class DataSourceEntity {

    private DataBaseEntity dataBaseEntity;
    private String dsJndiName;

    protected DataSourceEntity() {
    }

    public DataBaseEntity getDataBaseEntity() {
        return dataBaseEntity;
    }

    public void setDataBaseEntity(DataBaseEntity dataBaseEntity) {
        this.dataBaseEntity = dataBaseEntity;
    }

    public String getDsJndiName() {
        return dsJndiName;
    }

    public void setDsJndiName(String dsJndiName) {
        this.dsJndiName = dsJndiName;
    }
}
