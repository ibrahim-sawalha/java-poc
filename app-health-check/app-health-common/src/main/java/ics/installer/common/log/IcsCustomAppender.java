package ics.installer.common.log;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.ThrowableProxyUtil;
import ch.qos.logback.core.ConsoleAppender;

public class IcsCustomAppender extends ConsoleAppender<ILoggingEvent> {

    @Override
    protected void append(ILoggingEvent event) {
        super.append(event);
        StringBuffer out = new StringBuffer();
        out.append("[");
        out.append(event.getLevel());
        out.append("]");
        if (event.getLevel().equals(Level.ERROR)) {
            out.append(" ");
            out.append(event.getLoggerName());
            out.append(":");
            out.append(event.getCallerData()[0]);
            out.append(" ");
            out.append(ThrowableProxyUtil.asString(event.getThrowableProxy()));

        }
        out.append(" - ");
        out.append(event.getFormattedMessage());
        IcsLogger.addLog(out.toString());
    }

}
