package ics.installer.common.zip;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Zipper {
    private List<String> fileList;
    private File sourceDir;
    private static final String OUTPUT_ZIP_FILE = "D:\\delete\\jboss-server-backup\\jboss-eap-6.4_6.2.zip";
    private static final String SOURCE_FOLDER = "D:\\delete\\jboss-server-backup\\jboss-eap-6.4_6.2";
    final static Logger log = LoggerFactory.getLogger(Zipper.class.getName());

    public Zipper(File sourceDir) {
        fileList = new ArrayList<String>();
        this.sourceDir = sourceDir;
    }

    /**
     * Zip it
     *
     * @param zipFile output ZIP file location
     */
    public void zipIt(String zipFile) {

        FileUtils.getFile(zipFile).getParentFile().mkdirs();
        generateFileList(sourceDir);

        byte[] buffer = new byte[1024];
        try {
            FileOutputStream fos = new FileOutputStream(zipFile);
            ZipOutputStream zos = new ZipOutputStream(fos);

            log.trace("Output to Zip : " + zipFile);

            for (String file : this.fileList) {

                log.info("File Added : " + file);
                ZipEntry ze = new ZipEntry(file);
                zos.putNextEntry(ze);

                FileInputStream in = new FileInputStream(sourceDir.getAbsolutePath() + File.separator + file);

                int len;
                while ((len = in.read(buffer)) > 0) {
                    zos.write(buffer, 0, len);
                }

                in.close();
            }

            zos.closeEntry();
            // remember close it
            zos.close();

            log.info("Zipping " + zipFile + " was finished successfully.");
        } catch (IOException ex) {
            log.error("{}", ex);
        }
    }

    /**
     * Traverse a directory and get all files,
     * and add the file into fileList
     *
     * @param node file or directory
     */
    private void generateFileList(File node) {

        // add file only
        if (node.isFile()) {
            if (!node.getName().toUpperCase().contains(".log")) {
                fileList.add(generateZipEntry(node.getAbsoluteFile().toString()));
            }
        }

        if (node.isDirectory()) {
            if (!node.getName().equals(".git")) { // we don't need the .git directory
                String[] subNote = node.list();
                for (String filename : subNote) {
                    generateFileList(new File(node, filename));
                }
            }
        }

    }

    /**
     * Format the file path for zip
     *
     * @param file file path
     * @return Formatted file path
     */
    private String generateZipEntry(String file) {
        return file.substring(sourceDir.getAbsolutePath().length() + 1, file.length());
    }
}
