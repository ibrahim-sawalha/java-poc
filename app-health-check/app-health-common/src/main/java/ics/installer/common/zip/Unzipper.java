package ics.installer.common.zip;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Unzipper {
    private static final String INPUT_ZIP_FILE = "D:/delete/jboss-server-backup/jboss-eap-6.4_6.2.zip";
    private static final String OUTPUT_FOLDER = "D:/delete/jboss-server-backup/jboss-eap-6.4_6.2_unzip";
    final static Logger log = LoggerFactory.getLogger(Unzipper.class.getName());

    public static void main(String[] args) {
        Unzipper unZip = new Unzipper();
        unZip.unZipIt(INPUT_ZIP_FILE, OUTPUT_FOLDER);
    }

    /**
     * Unzip it
     *
     * @param zipFile input zip file
     * @param output zip file output folder
     */
    public void unZipIt(String zipFile, String outputFolder) {

        // if (!FileUtils.getFile(zipFile).exists()) {
        // log.error("Zip file [{}] does not exist ", zipFile);
        // return;
        // }

        byte[] buffer = new byte[1024];
        try {
            // create output directory is not exists
            File folder = new File(outputFolder);
            if (!folder.exists()) {
                folder.mkdir();
            }

            // get the zip file content
            ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile));
            // get the zipped file list entry
            ZipEntry ze = zis.getNextEntry();

            while (ze != null) {

                String fileName = ze.getName();
                File newFile = new File(outputFolder + File.separator + fileName);
                if (newFile.getName().endsWith("sh")) {
                    newFile.setExecutable(true);
                }

                log.info("file unzip : " + newFile.getAbsoluteFile());

                // create all non exists folders
                // else you will hit FileNotFoundException for compressed folder
                new File(newFile.getParent()).mkdirs();

                FileOutputStream fos = new FileOutputStream(newFile);

                int len;
                while ((len = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }

                fos.close();
                ze = zis.getNextEntry();
            }

            zis.closeEntry();
            zis.close();

            log.info("Unzipping " + zipFile + " to " + outputFolder + " was finished successfully.");

        } catch (IOException ex) {
            log.error("", ex);
        }
    }
}
