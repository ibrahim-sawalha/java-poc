package ics.installer.common.log;

import java.util.ArrayList;
import java.util.List;

public class IcsLogger {

    private static List<String> holder;

    public static List<String> getLog() {
        // if (holder == null) {
        // holder = new ArrayList<String>();
        // }
        return holder;
    }

    public static void addLog(String item) {
        if (holder != null) {
            holder.add(item);
        }
    }

    public static void startLogging() {
        holder = new ArrayList<String>();
    }

    public static void endLogging() {
        holder = null;
    }

}
