package ics.installer.common.entity;

public final class EntityFactory {

    private EntityFactory() {
    }

    public static DataBaseEntity buildDataBaseEntity(String dbUserName, String dbPassword, String dbAddress,
            String dbSid, String dbPort) {
        if (dbUserName == null || dbPassword == null || dbAddress == null || dbUserName.isEmpty()
                || dbPassword.isEmpty() || dbAddress.isEmpty()) {
            throw new NullPointerException("Database UserName/Password/Address is not valid.");
        }
        if (dbSid == null || dbSid.isEmpty()) {
            dbSid = "HOBANK";
        }
        if (dbPort == null || dbPort.isEmpty()) {
            dbPort = "1521";
        }
        DataBaseEntity e = new DataBaseEntity();
        e.setDbAddress(dbAddress);
        e.setDbPassword(dbPassword);
        e.setDbPort(dbPort);
        e.setDbSid(dbSid);
        e.setDbUserName(dbUserName);
        return e;
    }

    public static DataSourceEntity buildDataSourceEntity(String dsJndiName, String dbUserName, String dbPassword,
            String dbAddress, String dbSid, String dbPort) {
        if (dsJndiName == null || dsJndiName.isEmpty()) {
            throw new NullPointerException("DataSource JNDI Name is not valid.");
        }
        DataSourceEntity e = new DataSourceEntity();
        e.setDsJndiName(dsJndiName);
        e.setDataBaseEntity(buildDataBaseEntity(dbUserName, dbPassword, dbAddress, dbSid, dbPort));
        return e;
    }

    public static WeblogicConnEntity buildWeblogicConnEntity(String adminUser, String adminPassword,
            String weblogicAddress, String consolePort) {
        if (adminPassword == null || adminPassword.isEmpty()) {
            throw new NullPointerException("Weblogic Admin Password is not valid.");
        }
        if (adminUser == null || adminUser.isEmpty()) {
            adminUser = "weblogic";
        }
        if (weblogicAddress == null || weblogicAddress.isEmpty()) {
            weblogicAddress = "localhost";
        }
        if (consolePort == null || consolePort.isEmpty()) {
            consolePort = "7001";
        }
        WeblogicConnEntity e = new WeblogicConnEntity();
        e.setAdminPassword(adminPassword);
        e.setAdminUser(adminUser);
        e.setConsolePort(consolePort);
        e.setWeblogicAddress(weblogicAddress);
        return e;

    }

}
