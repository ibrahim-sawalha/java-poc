package ics.installer.common.entity;

public class WeblogicConnEntity {

    private String adminUser = "weblogic";
    private String adminPassword = "weblogic123";
    private String weblogicAddress = "127.0.0.1";
    private String consolePort = "7001";

    public String getAdminUser() {
        return adminUser;
    }

    public void setAdminUser(String adminUser) {
        this.adminUser = adminUser;
    }

    public String getAdminPassword() {
        return adminPassword;
    }

    public void setAdminPassword(String adminPassword) {
        this.adminPassword = adminPassword;
    }

    public String getWeblogicAddress() {
        return weblogicAddress;
    }

    public void setWeblogicAddress(String weblogicAddress) {
        this.weblogicAddress = weblogicAddress;
    }

    public String getConsolePort() {
        return consolePort;
    }

    public void setConsolePort(String consolePort) {
        this.consolePort = consolePort;
    }
}
