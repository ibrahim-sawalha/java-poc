package ics.installer.common.zip;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTimeUtil {

    private DateTimeUtil() {
    }

    public static DateFormat dateFormat = new SimpleDateFormat("hh.mm-dd.MM.yyyy");

    public static String getCurrentDateTiem() {
        Date date = new Date();
        return dateFormat.format(date);
    }
}
