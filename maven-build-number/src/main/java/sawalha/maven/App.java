package sawalha.maven;

import java.io.IOException;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

public class App {
    public static void main(String[] args) throws IOException {
        // Check Manifest file based on generated build number
        Manifest mf = new Manifest();
        mf.read(Thread.currentThread().getContextClassLoader().getResourceAsStream("META-INF/MANIFEST.MF"));

        Attributes atts = mf.getMainAttributes();

        System.out.println("Implementation-Versio: " + atts.getValue("Implementation-Version"));
        System.out.println("Implementation-Build: " + atts.getValue("Implementation-Build"));
    }
}
