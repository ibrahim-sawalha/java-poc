package sawalha.encryption.aes;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

public class AESCryptoPaddingUtils {

    public synchronized static void encryptFile(String password, String initVector, File inputFile, File outputFile)
            throws Exception {
        FileInputStream inputStream = new FileInputStream(inputFile);
        byte[] inputBytes = new byte[(int) inputFile.length()];
        inputStream.read(inputBytes);

        byte[] outputBytes = encryptBytes(password, initVector, inputBytes);

        FileOutputStream outputStream = new FileOutputStream(outputFile);
        outputStream.write(outputBytes);

        inputStream.close();
        outputStream.close();
    }

    public synchronized static void decryptFile(String password, String initVector, File inputFile, File outputFile)
            throws Exception {
        FileInputStream inputStream = new FileInputStream(inputFile);
        byte[] inputBytes = new byte[(int) inputFile.length()];
        inputStream.read(inputBytes);

        byte[] outputBytes = decryptBytes(password, initVector, inputBytes);

        FileOutputStream outputStream = new FileOutputStream(outputFile);
        outputStream.write(outputBytes);

        inputStream.close();
        outputStream.close();
    }

    ////////////
    public synchronized static byte[] encryptBytes(String key, String initVector, byte[] value) {
        try {
            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
            byte[] encrypted = cipher.doFinal(value);
            return encrypted;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public synchronized static String encryptString(String key, String initVector, String value)
            throws UnsupportedEncodingException {
        return Base64.encodeBase64String(encryptBytes(key, initVector, value.getBytes("UTF-8")));
    }

    public synchronized static byte[] decryptBytes(String key, String initVector, byte[] encrypted) {
        try {
            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
            return cipher.doFinal(encrypted);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public synchronized static String decryptString(String key, String initVector, String encrypted)
            throws UnsupportedEncodingException {
        return new String(decryptBytes(key, initVector, Base64.decodeBase64(encrypted)));
    }

    ///////////////////////////////

    public static void main(String[] args) throws Exception {
        String password = "1234567891234567"; // 16 char == 128 bit
        String initVector = "RandomInitVector"; // 16 bytes IV

        // 1
        String encrypted = AESCryptoPaddingUtils.encryptString(password, initVector, "Ibrahim Adnan Ibrahim Sawalha");
        System.out.println(encrypted);
        String decrypted = AESCryptoPaddingUtils.decryptString(password, initVector, encrypted);
        System.out.println(decrypted);

        // 2
        byte[] encryptedBytes = AESCryptoPaddingUtils.encryptBytes(password, initVector,
            "Ibrahim Adnan Ibrahim Sawalha".getBytes("UTF-8"));
        System.out.println(Base64.encodeBase64String(encryptedBytes));
        byte[] decryptedBytes = AESCryptoPaddingUtils.decryptBytes(password, initVector, encryptedBytes);
        System.out.println(new String(decryptedBytes));

        // 3
        File inputFile = new File("document.mp4");
        File encryptedFile = new File("document.encrypted.mp4");
        File decryptedFile = new File("document.decrypted.mp4");

        try {
            AESCryptoPaddingUtils.encryptFile(password, initVector, inputFile, encryptedFile);
            AESCryptoPaddingUtils.decryptFile(password, initVector, encryptedFile, decryptedFile);
        } catch (AESCryptoException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
    }
}
