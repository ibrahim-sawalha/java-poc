package sawalha.encryption.aes;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Enumeration;

import org.apache.commons.codec.binary.Base64;

public class NewKeyGenerator {

    /**
     * Demonstrates how to generate SHA256 hash in Java
     */
    public static void main(String[] args) {
        try {
            // test();
            // loadKeystore();

            String data = new String(
                    "<MsgBody><AcctInfo><BillingNo>7290</BillingNo><BillerCode>4</BillerCode></AcctInfo><ServiceType>Electricity</ServiceType></MsgBody>"
                            .getBytes(),
                    "UTF-16LE");
            data = "<MsgBody><AcctInfo><BillingNo>7290</BillingNo><BillerCode>4</BillerCode></AcctInfo><ServiceType>Electricity</ServiceType></MsgBody>";
            String dataI = new String(
                    "<MsgBody><RecCount>1</RecCount><BillsRec><BillRec><AcctInfo><BillingNo>54654</BillingNo><BillNo>54654</BillNo></AcctInfo><BillStatus>BillNew</BillStatus><DueAmount>82</DueAmount><IssueDate>2016-11-01T11:54:56</IssueDate><DueDate>2016-11-01T11:54:56</DueDate><ServiceType>ADSL_TEST</ServiceType><PmtConst><AllowPart>true</AllowPart><Lower>1</Lower><Upper>2000</Upper></PmtConst></BillRec></BillsRec></MsgBody>"
                            .getBytes(),
                    "UTF-16LE");

            // byte[] signData = signData(data.getBytes("UTF-16LE"));
            byte[] signData = signData(Base64.decodeBase64(data.getBytes("UTF-16LE")));
            System.out.println("Signed Data:\n" + new String(signData));

            // byte[] signDataI = signData(dataI.getBytes());
            // System.out.println(new String(signDataI));

            // String dataStr = new String(signData, "UTF-8");
            // System.out.println("dataStr: " + dataStr);

            // verifyData(data.getBytes(), Base64.decodeBase64(signData));
            verifyData(Base64.decodeBase64(data), Base64.decodeBase64(signData));

            // verifyData(data.getBytes(),
            // Base64.decodeBase64("qpUFwERaJPGNvYmuA3iiBs76MUJ/H4Gxj4YnU5V67LNT9ySZfvhiqAxRL6NKtAvxraO2xxCUL721Ag0LCyhbJIRUxmFQLGdBy2KKyAHC5WKgPK7o7q+p7ydL5JOGX4yjCEbSkCoi+ISmChI3H5QMFuyDYyquJ2cRiWkyXdG7sOmvEgnWckTGxogCDnRSa4WPTKgINXfk7eUrBWJaycCYqJnngIqkv2owPhp/36rWf3Z79y1E+KkwQ2xT+YNyK+mstKNKtZ2HqbYlL4VvC9fF/5y0Vro58HTyUJF9wlI8OYWXp/bWtjNrye9gMXKA9ApvCSwM1R/Iz17LCDJ1Bwy1SQ==".getBytes()));
            // verifyData(dataI.getBytes(),
            // Base64.decodeBase64("I2ncNdq/Gb/LHstpMeFCF952FLF0SrFyUR7XopRhKggl6bcqoyOHl/NH7ezxLr4+v14QNxwlf2nxmW0uLFqL+Jklz28tfArO6aF7dNn9EFrKLV8bt72cEW6ceMnuCyMe+vq/9KTrVzIFloCkeS6ydKrV5NUKDRIirJg/I/4eO2ja5/RrnxmSu2xcyPF502uSPSa7uXFnd6G3yKtKExDMQjj2jfUmN0uL5hk/S4hA6Hki8NLt/vnvS74nMpNEsKyztIJZIXr526TNrOvudzPv1PQcMQD+9FRcuMW791I4eSoq1QXhBNOw8z+5rZVQMvNm7UfVJN5XyRMKGc5KXp+HeA==".getBytes()));

            // printkey(loadPublicKey());
            // installCert(data.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void loadKeystore() throws Exception {
        KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
        char[] password = "123456".toCharArray();

        keystore.load(ClassLoader.getSystemResourceAsStream("clientkeystore"), password);

        Enumeration<String> aliases = keystore.aliases();
        while (aliases.hasMoreElements()) {
            String alias = aliases.nextElement();
            System.out.println("Alias: " + alias);
            /*
             * if(keystore.getCertificate(alias).getType().equals("X.509")){
             * Key key = keystore.getKey(alias, "123456".toCharArray());
             * System.out.println("Key size: " + key.getFormat());
             * KeyPair keyPair = null;
             * if (key instanceof PrivateKey) {
             * java.security.cert.Certificate cert = keystore.getCertificate(alias);
             * PublicKey publicKey = cert.getPublicKey();
             * keyPair = new KeyPair(publicKey, (PrivateKey) key);
             * }
             * }
             */
        }
    }

    public static void test() throws Exception {
        KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
        kpg.initialize(2048);
        KeyPair keyPair = kpg.genKeyPair();

        byte[] data = "<MsgBody><AcctInfo><BillingNo>7290</BillingNo><BillerCode>4</BillerCode></AcctInfo><ServiceType>Electricity</ServiceType></MsgBody>"
                .getBytes();

        Signature sig = Signature.getInstance("SHA256WithRSA");
        sig.initSign(keyPair.getPrivate());
        sig.update(data);
        byte[] signatureBytes = sig.sign();
        // System.out.println("Singature:" + new BASE64Encoder().encode(signatureBytes));

        /*
         * sig.initVerify(keyPair.getPublic());
         * sig.update(data);
         */

        verifyData(data, signatureBytes);

        // System.out.println(sig.verify(signatureBytes));

    }

    public static KeyPair getKeyPair() throws IOException, GeneralSecurityException {
        try {
            KeyStore keystore = KeyStore.getInstance("JKS", "SUN");
            char[] password = "123456".toCharArray();

            keystore.load(ClassLoader.getSystemResourceAsStream("jdibkeystore.jks"), password);
            // java.security.cert.Certificate certificate = keystore.getCertificate("JDIB");

            /*
             * alias: jdibintrca2
             * alias: jdibintrca1
             * alias: jdibroot
             * alias: jdib
             */

            Enumeration<String> aliases = keystore.aliases();
            while (aliases.hasMoreElements()) {
                String alias = aliases.nextElement();
                // System.out.println("alias: " + alias);
                Key key = keystore.getKey("jdib", "123456".toCharArray());

                KeyPair keyPair = null;
                if (key instanceof PrivateKey) {
                    // System.out.println("hi");
                    java.security.cert.Certificate cert = keystore.getCertificate("jdib");
                    PublicKey publicKey = cert.getPublicKey();
                    keyPair = new KeyPair(publicKey, (PrivateKey) key);
                    return keyPair;
                }

                /*
                 * if(keystore.getCertificate(alias).getType().equals("X.509")){
                 * Key key = keystore.getKey(alias, "123456".toCharArray());
                 * System.out.println("Key size: " + key.getFormat());
                 * KeyPair keyPair = null;
                 * if (key instanceof PrivateKey) {
                 * java.security.cert.Certificate cert = keystore.getCertificate(alias);
                 * PublicKey publicKey = cert.getPublicKey();
                 * keyPair = new KeyPair(publicKey, (PrivateKey) key);
                 * return keyPair;
                 * }
                 * }
                 */
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static PrivateKey loadPrivateKey() throws Exception {
        File f = new File(ClassLoader.getSystemResource("private_key.der").getFile());
        FileInputStream is = new FileInputStream(f);
        DataInputStream dis = new DataInputStream(is);
        byte[] keyBytes = new byte[(int) f.length()];
        dis.readFully(keyBytes);
        dis.close();

        // PKCS12KeyStore spec2 = new PKCS12KeyStore();
        // spec2.engineLoad(is, "123456".toCharArray());

        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePrivate(spec);
    }

    public static PublicKey loadPublicKey() throws Exception {
        CertificateFactory fact = CertificateFactory.getInstance("X.509");
        FileInputStream is = new FileInputStream(new File(ClassLoader.getSystemResource("public_key.pem").getFile()));
        X509Certificate cer = (X509Certificate) fact.generateCertificate(is);
        PublicKey key = cer.getPublicKey();
        return key;
    }

    public static byte[] signData(byte[] data) {
        byte[] encryptedByteValue = null;
        try {
            PrivateKey privateKey = getKeyPair().getPrivate();// loadPrivateKey();
            Signature dsa = Signature.getInstance("SHA256withRSA");
            dsa.initSign(privateKey);
            dsa.update(data);
            byte[] signature = dsa.sign();

            encryptedByteValue = Base64.encodeBase64(signature);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return encryptedByteValue;
    }

    public static boolean verifyData(byte[] data, byte[] sig) throws Exception {
        PublicKey publicKey = getKeyPair().getPublic();// loadPublicKey(); getPK();//
        Signature sign = Signature.getInstance("SHA256withRSA");
        sign.initVerify(publicKey);// loadPublicX509());
        sign.update(data);
        // boolean b = sign.verify(s_base64);
        boolean b = sign.verify(sig);
        System.out.println("verifing: " + b);
        return b;
    }

    public static void printkey(Key key) throws Exception {
        String publicString = Base64.encodeBase64String(key.getEncoded());
        System.out.println("public key is: " + publicString);
        /*
         * byte[] pubBytes = key.getEncoded();
         * // now save pubBytes or prvBytes
         * // to recover the key
         * KeyFactory kf = KeyFactory.getInstance("RSA");
         * PublicKey pub_recovered = kf.generatePublic(new X509EncodedKeySpec(pubBytes));
         * System.out.println("Public Key: \n" + pub_recovered.toString());
         * System.out.println("Public Key: \n" + pub_recovered.getFormat());
         */
    }

    public static PublicKey getPK() throws Exception {
        /*
         * byte[] byteKey = Base64.decodeBase64(publicK.getBytes());
         * X509EncodedKeySpec X509publicKey = new X509EncodedKeySpec(byteKey);
         * KeyFactory keyFactory = KeyFactory.getInstance("RSA");
         * return keyFactory.generatePublic(X509publicKey);
         */
        FileInputStream fis = new FileInputStream(ClassLoader.getSystemResource("MFEPPublic042016.cer").getFile());
        // new FileInputStream("D:\\spr_workspace\\madfooatintg\\resources\\MFEPPublic042016.cer");
        BufferedInputStream bis = new BufferedInputStream(fis);

        CertificateFactory cf = CertificateFactory.getInstance("X.509");

        Certificate cert = null;
        while (bis.available() > 0) {
            cert = cf.generateCertificate(bis);
            System.out.println(cert.toString());
        }

        // CertificateFactory cf = CertificateFactory.getInstance("X.509");
        /*
         * FileInputStream in = new
         * FileInputStream(ClassLoader.getSystemResource("MFEPPublic042016.cer").getFile());
         * Certificate cert = cf.generateCertificate(in);
         */
        return cert.getPublicKey();

        // return loadMFEPPublicX509().getPublicKey();
    }

    public static X509Certificate loadMFEPPublicX509() throws Exception {
        InputStream is = ClassLoader.getSystemResourceAsStream("MFEPPublic042016.cer");
        X509Certificate crt = null;
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        crt = (X509Certificate) cf.generateCertificate(is);
        return crt;
    }

}
