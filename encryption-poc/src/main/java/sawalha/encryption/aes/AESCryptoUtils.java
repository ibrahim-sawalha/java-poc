package sawalha.encryption.aes;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

public class AESCryptoUtils {

    private static final String ALGORITHM = "AES";
    private static final String TRANSFORMATION = "AES";

    public synchronized static void encrypt(String key, File inputFile, File outputFile) throws AESCryptoException {
        doCrypto(Cipher.ENCRYPT_MODE, key, inputFile, outputFile);
    }

    public synchronized static void decrypt(String key, File inputFile, File outputFile) throws AESCryptoException {
        doCrypto(Cipher.DECRYPT_MODE, key, inputFile, outputFile);
    }

    private synchronized static void doCrypto(int cipherMode, String key, File inputFile, File outputFile)
            throws AESCryptoException {
        try {
            Key secretKey = new SecretKeySpec(key.getBytes(), ALGORITHM);
            Cipher cipher = Cipher.getInstance(TRANSFORMATION);
            cipher.init(cipherMode, secretKey);

            FileInputStream inputStream = new FileInputStream(inputFile);
            byte[] inputBytes = new byte[(int) inputFile.length()];
            inputStream.read(inputBytes);

            byte[] outputBytes = cipher.doFinal(inputBytes);

            FileOutputStream outputStream = new FileOutputStream(outputFile);
            outputStream.write(outputBytes);

            inputStream.close();
            outputStream.close();

        } catch (NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException | BadPaddingException
                | IllegalBlockSizeException | IOException ex) {
            throw new AESCryptoException("Error encrypting/decrypting file", ex);
        }
    }

    public static void main(String[] args) {
        // String key = "Bar12345Bar12345"; 128 bit key
        String key = "ibrahimsawalah12";

        System.out.println(key.length());
        if (key.length() != 16) {
            System.err.println("Key must contains 16 character.");
            System.exit(1);
        }
        File inputFile = new File("document.mp4");
        File encryptedFile = new File("document.encrypted.mp4");
        File decryptedFile = new File("document.decrypted.mp4");

        try {
            encrypt(key, inputFile, encryptedFile);
            decrypt(key, encryptedFile, decryptedFile);
        } catch (AESCryptoException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
    }
}
