package sawalha.encryption.aes;

import java.io.File;

import org.apache.commons.codec.binary.Base64;

public class TestAes {

    private static final String password = "1234567891234567"; // 16 char == 128 bit
    private static final String initVector = "RandomInitVector"; // 16 bytes IV

    public static void main(String[] args) throws Exception {
        String encrypted = AESCryptoPaddingUtils.encryptString(password, initVector, "Ibrahim Adnan Ibrahim Sawalha");
        System.out.println(encrypted);
        String decrypted = AESCryptoPaddingUtils.decryptString(password, initVector, encrypted);
        System.out.println(decrypted);

        byte[] encryptedBytes = AESCryptoPaddingUtils.encryptBytes(password, initVector,
            "Ibrahim Adnan Ibrahim Sawalha".getBytes("UTF-8"));
        System.out.println(Base64.encodeBase64String(encryptedBytes));

        byte[] decryptedBytes = AESCryptoPaddingUtils.decryptBytes(password, initVector, encryptedBytes);
        System.out.println(new String(decryptedBytes));

        File inputFile = new File("document.mp4");
        File encryptedFile = new File("document.encrypted.mp4");
        File decryptedFile = new File("document.decrypted.mp4");

        try {
            AESCryptoPaddingUtils.encryptFile(password, initVector, inputFile, encryptedFile);
            AESCryptoPaddingUtils.decryptFile(password, initVector, encryptedFile, decryptedFile);
        } catch (AESCryptoException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
    }
}
