package sawalha.encryption.aes;

public class AESCryptoException extends Exception {

    private static final long serialVersionUID = 8455563779756237115L;

    public AESCryptoException() {
    }

    public AESCryptoException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
