package sawalha.xml.jdom;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

public class JDomDemo {

    public static void main(String[] args) {

        SAXBuilder builder = new SAXBuilder();
        File outXmlFile = new File("jdom-exe-result.xml");

        try {

            // Document document = builder.build(xmlFile);
            Document document = builder.build(ClassLoader.getSystemResourceAsStream("xml/jdom-example.xml"));
            Element rootNode = document.getRootElement();
            List list = rootNode.getChildren("staff");

            for (int i = 0; i < list.size(); i++) {

                Element node = (Element) list.get(i);

                System.out.println("First Name : " + node.getChildText("firstname"));
                System.out.println("Last Name : " + node.getChildText("lastname"));
                System.out.println("Nick Name : " + node.getChildText("nickname"));
                System.out.println("Salary : " + node.getChildText("salary"));
                String salary = node.getChildText("salary");
                if (Integer.parseInt(salary) < 200000) {
                    node.getChild("salary").setText("1200");
                    node.setAttribute("type", "developer");
                } else {
                    node.getChild("salary").setText("3000");
                    node.setAttribute("type", "manager");
                }

            }

            XMLOutputter xmOut = new XMLOutputter();
            xmOut.setFormat(Format.getPrettyFormat());
            System.out.println(xmOut.outputString(document));

            XMLOutputter xmlFileOut = new XMLOutputter();
            xmlFileOut.setFormat(Format.getPrettyFormat());
            xmlFileOut.output(document, new FileOutputStream(outXmlFile));

        } catch (IOException io) {
            System.out.println(io.getMessage());
        } catch (JDOMException jdomex) {
            System.out.println(jdomex.getMessage());
        }
    }

}
