package sawalha.xml.jaxb;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlRootElement;

import sawalha.xml.jaxb.adapter.JaxbParameter;

@XmlRootElement
public class Employees {

    private List<Employee> employee;
    private List<JaxbParameter> parameters = new ArrayList<JaxbParameter>();

    public List<Employee> getEmployee() {
        return employee;
    }

    public void setEmployee(List<Employee> employee) {
        this.employee = employee;
    }

    @XmlAnyElement
    public List<JaxbParameter> getParameters() {
        return parameters;
    }

    public void setParameters(List<JaxbParameter> parameters) {
        this.parameters = parameters;
    }

}
