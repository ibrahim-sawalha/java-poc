package sawalha.xml.jaxb;

import java.lang.reflect.Parameter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import sawalha.xml.jaxb.adapter.JaxbParameter;
import sawalha.xml.jaxb.adapter.JaxbParameterAdapter;

public class JaxbAnyElementParsingDemo {

    public static void main(String[] args) {
        try {

            // File file = new File(ClassLoader.getSystemResourceAsStream("xml/employee.xml"));

            JAXBContext jaxbContext = JAXBContext.newInstance(Employees.class, Parameter.class);

            JaxbParameterAdapter adapter = new JaxbParameterAdapter(jaxbContext);

            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            unmarshaller.setAdapter(adapter);

            Employees empls = (Employees) unmarshaller
                    .unmarshal(ClassLoader.getSystemResourceAsStream("xml/employee-any-element.xml"));
            for (JaxbParameter parameter : empls.getParameters()) {
                System.out.println(parameter.getName());
            }

            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setAdapter(adapter);
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(empls, System.out);

        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

}
