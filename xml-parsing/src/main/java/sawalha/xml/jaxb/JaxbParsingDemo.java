package sawalha.xml.jaxb;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class JaxbParsingDemo {

    public static void main(String[] args) {

        Employee employee1 = new Employee();
        employee1.setId("111");
        employee1.setFirstName("Ibrahim");
        employee1.setLastName("Sawalha");
        employee1.setLocation("Jordan");

        Employee employee2 = new Employee();
        employee2.setId("222");
        employee2.setFirstName("Marcial");
        employee2.setLastName("Rosales");
        employee2.setLocation("Spain");

        List<Employee> employeeList = new ArrayList<>();
        employeeList.add(employee1);
        employeeList.add(employee2);
        Employees emplyees = new Employees();
        emplyees.setEmployee(employeeList);

        try {

            // File file = new File("C:\\file.xml");
            JAXBContext jaxbContext = JAXBContext.newInstance(Employees.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            // jaxbMarshaller.marshal(emplyees, file);
            jaxbMarshaller.marshal(emplyees, System.out);

        } catch (JAXBException e) {
            e.printStackTrace();
        }

    }
}
