package sawalha.xml.stax;

import java.io.IOException;
import java.io.StringWriter;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.Serializer;
import net.sf.saxon.s9api.Serializer.Property;

public class StaxWriterDemo {
    public static void main(String[] args) {
        try {
            StringWriter stringWriter = new StringWriter();

            // XMLOutputFactory xMLOutputFactory = XMLOutputFactory.newInstance();
            // XMLStreamWriter xMLStreamWriter = xMLOutputFactory.createXMLStreamWriter(stringWriter);

            net.sf.saxon.s9api.Processor p = new net.sf.saxon.s9api.Processor(true);
            Serializer s = p.newSerializer();
            s.setOutputProperty(Property.METHOD, "xml");
            s.setOutputProperty(Property.INDENT, "yes");
            s.setOutputStream(System.out);
            XMLStreamWriter xMLStreamWriter = s.getXMLStreamWriter();

            xMLStreamWriter.writeStartDocument();
            xMLStreamWriter.writeStartElement("employees");

            // employ 1
            xMLStreamWriter.writeStartElement("employee");
            xMLStreamWriter.writeAttribute("id", "111");
            xMLStreamWriter.writeStartElement("firstName");
            xMLStreamWriter.writeCharacters("Ibrahim");
            xMLStreamWriter.writeEndElement();
            xMLStreamWriter.writeStartElement("lastName");
            xMLStreamWriter.writeCharacters("Sawalha");
            xMLStreamWriter.writeEndElement();
            xMLStreamWriter.writeStartElement("location");
            xMLStreamWriter.writeCharacters("Jordan");
            xMLStreamWriter.writeEndElement();
            xMLStreamWriter.writeEndElement();// end employee
            // employ 2
            xMLStreamWriter.writeStartElement("employee");
            xMLStreamWriter.writeAttribute("id", "222");
            xMLStreamWriter.writeStartElement("firstName");
            xMLStreamWriter.writeCharacters("John");
            xMLStreamWriter.writeEndElement();
            xMLStreamWriter.writeStartElement("lastName");
            xMLStreamWriter.writeCharacters("Davis");
            xMLStreamWriter.writeEndElement();
            xMLStreamWriter.writeStartElement("location");
            xMLStreamWriter.writeCharacters("UK");
            xMLStreamWriter.writeEndElement();
            xMLStreamWriter.writeEndElement();// end employee
            // employ 3
            xMLStreamWriter.writeStartElement("employee");
            xMLStreamWriter.writeAttribute("id", "333");
            xMLStreamWriter.writeStartElement("firstName");
            xMLStreamWriter.writeCharacters("Marcial");
            xMLStreamWriter.writeEndElement();
            xMLStreamWriter.writeStartElement("lastName");
            xMLStreamWriter.writeCharacters("Rosales");
            xMLStreamWriter.writeEndElement();
            xMLStreamWriter.writeStartElement("location");
            xMLStreamWriter.writeCharacters("Spain");
            xMLStreamWriter.writeEndElement();
            xMLStreamWriter.writeEndElement(); // end employee

            xMLStreamWriter.writeEndDocument(); // end employees

            xMLStreamWriter.flush();
            xMLStreamWriter.close();

            String xmlString = stringWriter.getBuffer().toString();

            stringWriter.close();

            System.out.println(xmlString);

        } catch (XMLStreamException e) {
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SaxonApiException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
